import React, {useState, useEffect} from "react";
import Offcanvas from 'react-bootstrap/Offcanvas';
import { useTranslation } from 'react-i18next';

export default function SideBarOffCanvasMobile({
    sideCanvasShow,
    sideOffCanvasClose,
    updateShowSidebar,
    activeView,
    setActiveView,
    logout
}) {
    const { t } = useTranslation(); // Initialize translation hook

    const handleMenuClick = (view) => {
        setActiveView(view);
    };

    const updateSidebars = () => {
        updateShowSidebar(true);
        sideOffCanvasClose();
    }

    return (
        <Offcanvas show={sideCanvasShow} onHide={() => updateSidebars()} placement={'start'} name='infoIconOffCanvas'
                   id='side_offcanvas'
                   scroll='true'
        >
            <Offcanvas.Body>
                <div>
                    <button className={`custon-mt text-18px font-semibold navbutton ${activeView === "profile" ? "active" : ""}`} onClick={() => handleMenuClick("profile")}>
                        {t("translations.profile")}
                    </button>
                    <hr/>
                    <button className={`text-18px font-semibold navbutton ${activeView === "myorganization" ? "active" : ""}`} onClick={() => handleMenuClick("myorganization")}>
                        {t("translations.myorganization")}
                    </button>
                    <hr/>
                    <button className={`text-18px font-semibold navbutton ${activeView === "mylistings" ? "active" : ""}`} onClick={() => handleMenuClick("mylistings")}>
                        {t("translations.mylistings")}
                    </button>
                    <hr/>
                    <button className={`text-18px font-semibold navbutton`} onClick={() => logout()}>
                        {t("translations.logout")}
                    </button>
                    <hr className="custom-mb"/>
                </div>
            </Offcanvas.Body>
        </Offcanvas>
    );
}
