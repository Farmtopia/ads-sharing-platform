import React, {useState, Fragment} from "react";
import "../../../../public/css/MyListings.css";
import {useTranslation} from "react-i18next";
import RentalCard from "../../components/RentalCard.jsx";
import Modal from 'react-bootstrap/Modal';
import tractor from "../../../../public/img/tractor_image.png";
import drone from "../../../../public/img/drone_image.png";

export default function MyListings({authToken, listingData}) {
    const {t} = useTranslation();
    const [showListingModal, setShowListingModal] = useState(false);

    const handleCloseListingModal = () => setShowListingModal(false);
    const handleShowListingModal = () => setShowListingModal(true);

    const navigateToNewListing = (type) => {
        handleCloseListingModal();
        if (type === "traditional") {
            window.location.href = '/newlisting/traditional_equipment'
        } else {
            window.location.href = '/newlisting/digital_equipment'
        }
    }

    return (
        <div className='my_listings_container'>
            <div className='my_listings_head_btns_container'>
                <div className='my_listings_head_btn_all font-medium text-16px'>{t("translations.all")}</div>
                <div className='my_listings_head_btn_new font-medium text-20px' onClick={handleShowListingModal}><span
                    className='font-medium text-20px me-3'>+</span>{t("translations.addanewlisting")}</div>
            </div>
            <div className='my_listings_cards_container'>
                {listingData?.map((lData, l) => {
                    return (
                        <Fragment key={lData.id}>
                            <RentalCard cardId={lData.id}/>
                        </Fragment>
                    );
                })}
            </div>
            <Modal show={showListingModal} fullscreen={true} onHide={handleCloseListingModal}
                   dialogClassName="newListingModal">
                <div className='new_listing_modal_container'>
                    <div
                        className='new_listing_modal_head font-semibold text-30px text-default'>{t("translations.whattolist")}</div>
                    <div className='new_listing_modal_cards_container'>
                        <div className='new_listing_modal_card_cont'
                             onClick={() => navigateToNewListing('traditional')}>
                            <h3 className='font-semibold text-blue'>{t("translations.traditionalequipment")}</h3>
                            <img className='new_listing_modal_card_image' src={tractor} alt='Tractor Image'/>
                        </div>
                        <div className='new_listing_modal_card_cont'
                             onClick={() => navigateToNewListing('digital')}>
                            <h3 className='font-semibold text-blue'>{t("translations.digitalequipment")}</h3>
                            <img className='new_listing_modal_card_image' src={drone} alt='Drone Image'/>
                        </div>
                    </div>
                </div>
            </Modal>
        </div>
    );
}
