import React from "react";
import menuSidebarIcon from "../../../../public/img/tech_sidebar_menu_mobile.png";

export default function SidebarMenuMobile({updateShowSidebar, sideOffCanvasShow}) {
    const sideButtonClick = () => {
        updateShowSidebar(false);
        sideOffCanvasShow();
    }
    return (
        <button className='pagin_arrow_button' type='button' onClick={(e) => sideButtonClick(e)}>
            <img className='menu_sidebar_icon' src={menuSidebarIcon} alt='Sidebar Menu Icon'/>
        </button>
    );
}
