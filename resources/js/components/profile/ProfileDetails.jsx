import React, { useState, useEffect } from "react";
import "../../../../public/css/MyProfile.css";
import { useTranslation } from 'react-i18next';

export default function ProfileDetails({ userData, countries, authToken, setUserData }) {
    const { t } = useTranslation(); // Initialize translation hook
    const [formData, setFormData] = useState({
        firstname: '',
        lastname: '',
        phone: '',
        profession: '',
        country_id: '',
        region: '',
        zip_code: '',
        address_road: '',
        address_number: '',
        instagram: '',
        facebook: '',
        linkedin: '',
        twitter: '',
        profile_photo: null,
    });

    useEffect(() => {
        if (userData) {
            setFormData({
                firstname: userData.firstname || '',
                lastname: userData.lastname || '',
                phone: userData.phone || '',
                profession: userData.profession || '',
                country_id: userData.country_id || '',
                region: userData.region || '',
                zip_code: userData.zip_code || '',
                address_road: userData.address_road || '',
                address_number: userData.address_number || '',
                instagram: userData.social?.instagram || '',
                facebook: userData.social?.facebook || '',
                linkedin: userData.social?.linkedin || '',
                twitter: userData.social?.twitter || '',
                profile_photo: null,
            });
        }
    }, [userData]);

    const handleChange = (e) => {
        const { name, value, files } = e.target;
        setFormData({
            ...formData,
            [name]: files ? files[0] : value,
        });
    };

    const handleSubmit = async (e) => {
        e.preventDefault();

        // Validation logic for required fields
        const requiredFields = ['firstname', 'lastname', 'country_id', 'region', 'zip_code', 'address_road', 'address_number'];
        for (const field of requiredFields) {
            if (!formData[field]) {
                alert(`${t(`translations.${field}`)} is required`);
                return;
            }
        }

        const body = new FormData();
        body.append('_method', 'PUT');

        // Append only non-empty fields
        Object.keys(formData).forEach((key) => {
            const value = formData[key];

            if (key === 'profile_photo') {
                // Only append if it's a valid file
                if (value instanceof File) {
                    body.append(key, value);
                }
            } else if (value !== null && value !== undefined && value !== "") {
                // Append only non-empty fields
                body.append(key, value);
            }
        });

        console.log("Profile Data:", [...body.entries()]); // Debugging: Check FormData content

        try {
            const response = await fetch('/api/users/profile/user/update', {
                method: 'POST', // Sending as POST with `_method=PUT`
                headers: {
                    'Authorization': `Bearer ${authToken}`,
                    'Accept': 'application/json',
                },
                body,
            });

            const data = await response.json();
            console.log("Profile update response:", data);

            if (data.success) {
                console.log("Profile updated successfully:", data.user);
                setUserData(data.user);
            } else {
                console.error("Error updating profile:", data.message);
            }
        } catch (error) {
            console.error("Error updating profile:", error);
        }
    };



    return (
        <form onSubmit={handleSubmit} className="row profile-details-container mt-5">
            {/* Left Column - Personal Details */}
            <div className="col-12 col-lg-6">
                <div className="text-center">
                    <div className="profile-picture-container">
                        <img src={userData?.profile_photo ? `/storage/profile_photos/${userData.profile_photo}` : "/img/profile-placeholder.webp"} alt="Profile" className="profile-picture w-100" />
                    </div>
                    <input type="file" name="profile_photo" className="form-control change-details-button mt-2" onChange={handleChange} />
                </div>

                <div className="personal-details mt-4">
                    <p className="text-20px font-semibold text-blue">{t('translations.personaldetails')}</p>
                    <input type="text" name="firstname" placeholder={t('translations.firstname')} className="form-control text-ligh-grey mb-3" value={formData.firstname} onChange={handleChange} required />
                    <input type="text" name="lastname" placeholder={t('translations.lastname')} className="form-control text-ligh-grey mb-3" value={formData.lastname} onChange={handleChange} required />
                    <input type="email" name="email" placeholder={t('translations.email')} className="form-control text-ligh-grey mb-3" value={userData?.email || ''} readOnly />
                    <input type="text" name="phone" placeholder={t('translations.phonenumber')} className="form-control text-ligh-grey mb-3" value={formData.phone} onChange={handleChange} />
                    <input type="text" name="profession" placeholder={t('translations.profession')} className="form-control text-ligh-grey mb-3" value={formData.profession} onChange={handleChange} />
                </div>
            </div>

            {/* Right Column - Address & Socials */}
            <div className="col-12 col-lg-6 address-details-container">
                <div className="address-details mb-4">
                    <p className="text-20px font-semibold text-blue">{t('translations.address')}</p>
                    <select name="country_id" className="form-control text-ligh-grey mb-3" value={formData.country_id} onChange={handleChange} required>
                        {countries.map(country => (
                            <option key={country.id} value={country.id}>{country.name}</option>
                        ))}
                    </select>
                    <input type="text" name="region" placeholder={t('translations.region')} className="form-control text-ligh-grey mb-3" value={formData.region} onChange={handleChange} required />
                    <input type="text" name="zip_code" placeholder={t('translations.zipcode')} className="form-control text-ligh-grey mb-3" value={formData.zip_code} onChange={handleChange} required />
                    <input type="text" name="address_road" placeholder={t('translations.streetname')} className="form-control text-ligh-grey mb-3" value={formData.address_road} onChange={handleChange} required />
                    <input type="text" name="address_number" placeholder={t('translations.streetnumber')} className="form-control text-ligh-grey mb-3" value={formData.address_number} onChange={handleChange} required />
                </div>

                <div className="socials-authentication">
                    <p className="text-20px font-semibold text-blue">{t('translations.socialsAndAuthentication')}</p>
                    <div className="input-group mb-3">
                        <span className="input-group-text"><i className="fab fa-instagram"></i></span>
                        <input type="text" name="instagram" value={formData.instagram} className="form-control text-ligh-grey" onChange={handleChange} pattern="https?://.+"/>
                    </div>
                    <div className="input-group mb-3">
                        <span className="input-group-text"><i className="fab fa-facebook"></i></span>
                        <input type="text" name="facebook" value={formData.facebook} className="form-control text-ligh-grey" onChange={handleChange} pattern="https?://.+"/>
                    </div>
                    <div className="input-group mb-3">
                        <span className="input-group-text"><i className="fab fa-linkedin"></i></span>
                        <input type="text" name="linkedin" value={formData.linkedin} className="form-control text-ligh-grey" onChange={handleChange} pattern="https?://.+"/>
                    </div>
                    <div className="input-group mb-3">
                        <span className="input-group-text"><i className="fab fa-x-twitter"></i></span>
                        <input type="text" name="twitter" value={formData.twitter} className="form-control text-ligh-grey" onChange={handleChange} pattern="https?://.+"/>
                    </div>
                </div>
                <button type="submit" className="btn change-details-button mt-3 mb-5">{t('translations.saveChanges')}</button>
            </div>
        </form>
    );
}
