import React, {useEffect, useState} from "react";
import "../../../../public/css/MyOrganizationDetails.css";
import {useTranslation} from 'react-i18next';
import {updateOrganizationProfile} from "../../APIs/Organizations.jsx";
import ReactSelect2Component from "../ReactSelect2Component.jsx";
import Modal from 'react-bootstrap/Modal';
import {inviteOrganizationMember} from "../../APIs/Auth.jsx"
import MyOrganizationMap from "./MyOrganizationMap.jsx";

export default function MyOrganizationDetails({
                                                  authToken,
                                                  organizationData,
                                                  countryById,
                                                  countries,
                                                  setOrganizationData,
                                                  userAccessLevel
                                              }) {
    const {t} = useTranslation();
    const [formData, setFormData] = useState({
        name: '',
        email: '',
        phone: '',
        description: '',
        country_id: '',
        instagram: '',
        facebook: '',
        linkedin: '',
        twitter: '',
        website: '',
        country: '',
        region: '',
        zip_code: '',
        address_road: '',
        address_number: '',
        dial_prefix: '',
        cover_photo: null,
    });
    const [countryOptions, setCountryOptions] = useState([]);
    const [showInvitation, setShowInvitation] = useState(false);
    const [invitationEmail, setInvitationEmail] = useState('');
    const [position, setPosition] = useState({});

    const handleCloseInvitation = () => setShowInvitation(false);
    const handleShowInvitation = () => setShowInvitation(true);

    const handleChange = (e) => {
        const {name, value, files} = e.target;
        setFormData({
            ...formData,
            [name]: files ? files[0] : value,
        });
    };

    const handleUpdateOrganizationProfile = async (id, body) => {
        try {
            const data = await updateOrganizationProfile(authToken, id, body);
            if (data.success) {
                console.log(data.message);
                setOrganizationData(data.organization);
            } else {
                console.log(data.message);
            }
        } catch (error) {
            console.error("Error updating organization profile:", error);
        }
    };

    const formSubmit = (e) => {
        e.preventDefault();

        const requiredFields = ['name', 'description', 'country_id'];
        for (const field of requiredFields) {
            if (!formData[field]) {
                alert(`${t(`translations.${field}`)} is required`);
                return;
            }
        }
        const body = new FormData();
        body.append('_method', 'PUT');
        Object.keys(formData).forEach((key) => {
            const value = formData[key];

            if (key === 'cover_photo') {
                if (value instanceof File) {
                    body.append(key, value);
                }
            } else if (value !== null && value !== undefined && value !== "" && key !== 'country' && key !== 'dial_prefix') {
                body.append(key, value);
            }
        });
        // console.log([...body.entries()]);
        handleUpdateOrganizationProfile(organizationData.id, body);
    }

    const handleSelect = (val) => {
        let newObj = {...formData};
        newObj.country = val.label;
        setFormData(newObj);
    }

    const handleInviteOrganizationMember = async (invitationEmail) => {
        try {
            const data = await inviteOrganizationMember(authToken, invitationEmail);
            if (data.success) {
                console.log(data.message);
            } else {
                console.log(data.message);
            }
        } catch (error) {
            console.error("Error on member invitation: ", error);
        }
    };

    const handleSendInvitation = (e) => {
        e.preventDefault();
        if (invitationEmail !== null && invitationEmail !== undefined && invitationEmail !== "") {
            handleInviteOrganizationMember(invitationEmail);
            handleCloseInvitation();
        } else {
            alert(t('translations.validemail'));
        }
    }

    const calculateAddressDataFromCoords = async (pos) => {
        try {
            const response = await fetch(`https://nominatim.openstreetmap.org/reverse?format=json&accept-language=en&lat=${pos.lat}&lon=${pos.lon}`);
            const data = await response.json();
            if (Object.keys(data).length > 0) {
                const addrData = [data.address.country, data.address.city, data.address.postcode, data.address.road, data.address.house_number];
                if (data.address.country || data.address.city || data.address.postcode || data.address.road || data.address.house_number) {
                    let newObj = {...formData};

                    newObj.country = data.address.country || '';
                    newObj.region = data.address.city || '';
                    newObj.zip_code = data.address.postcode.replace(' ', '') || '';
                    newObj.address_road = data.address.road || '';
                    newObj.address_number = data.address.house_number || '';

                    setFormData(newObj);
                }
            } else {
                console.log('No results found');
            }
        } catch (err) {
            console.log('Error fetching data: ' + err.toString());
        }
    }

    const calculateCoordinates = async (address_number, address_road, zip_code, region, country) => {
        try {
            const response = await fetch(`https://nominatim.openstreetmap.org/search?addressdetails=1&format=json&accept-language=en&street=${encodeURIComponent(address_number + ' ' + address_road)}&city=${encodeURIComponent(region)}&postalcode=${encodeURIComponent(zip_code)}&country=${encodeURIComponent(country)}`);
            const data = await response.json();
            if (data.length > 0) {
                for (let i = 0; i < data.length; i++) {
                    if (data[i].address.postcode.replace(' ', '') === zip_code) {
                        return [data[i].lat, data[i].lon];
                    }
                }
            } else {
                console.log('No results found');
                return [null, null];
            }
        } catch (err) {
            console.log('Error fetching data: ' + err.toString());
            return [null, null];
        }
    }

    useEffect(() => {
        if (Object.keys(position).length > 0) {
            calculateAddressDataFromCoords(position);
        }
    }, [position]);

    useEffect(() => {
        if (countries) {
            let newArray = [...countryOptions];
            for (let i = 0; i < countries.length; i++) {
                newArray.push({value: countries[i].id, label: countries[i].name});
            }
            setCountryOptions(newArray);
        }
    }, [countries]);

    useEffect(() => {
        if (organizationData) {
            let newObj = {...formData};

            newObj.name = organizationData.name || '';
            newObj.email = organizationData.email || '';
            newObj.phone = organizationData.phone || '';
            newObj.description = organizationData.description || '';
            newObj.country_id = organizationData.country_id || '';
            newObj.instagram = organizationData.social?.instagram || '';
            newObj.facebook = organizationData.social?.facebook || '';
            newObj.linkedin = organizationData.social?.linkedin || '';
            newObj.twitter = organizationData.social?.twitter || '';
            newObj.website = organizationData.website || '';
            newObj.region = organizationData.region || '';
            newObj.zip_code = organizationData.zip_code || '';
            newObj.address_road = organizationData.address_road || '';
            newObj.address_number = organizationData.address_number || '';
            newObj.cover_photo = null;
            newObj.country = countryById.name;
            newObj.dial_prefix = countryById.dial_prefix;

            setFormData(newObj);
            const fetchCoordinates = async () => {
                const [latitude, longitude] = await calculateCoordinates(organizationData.address_number, organizationData.address_road, organizationData.zip_code, organizationData.region, countryById.name) || [null, null];
                if (latitude && latitude) {
                    setPosition({lat: latitude, lon: longitude});
                }
            }
            fetchCoordinates();
        }
    }, [organizationData]);

    return (
        <>
            <form onSubmit={formSubmit} className="my_organization_details_container">
                <div className='my_organization_details_cards_row'>
                    <div className='my_organization_details_card_logo'>
                        <div className="my_organization_details_logo_card">
                            <img
                                src={organizationData?.cover_photo ? `/storage/cover_photos/${organizationData.cover_photo}` : "/img/profile-placeholder.webp"}
                                alt="Organization Logo" className="my_organization_details_logo_img"/>
                        </div>
                        <input type="file" name="cover_photo"
                               className="form-control my_organization_details_logo_btn"
                               onChange={handleChange}
                        />
                    </div>
                    <div className='my_organization_details_card_details'>
                        <div
                            className='font-semibold text-20px text-blue mb-2'>{t('translations.organizationdetails')}</div>
                        <input type="text" name="name" placeholder={t('translations.name')}
                               className="form-control text-16px font-normal text-light-grey"
                               value={formData.name} onChange={handleChange}
                               required readOnly={userAccessLevel !== 'org_admin'}/>
                        <input type="email" name="email" placeholder={t('translations.email')}
                               className="form-control text-16px font-normal text-light-grey"
                               value={formData.email} onChange={handleChange}
                               readOnly={userAccessLevel !== 'org_admin'}/>
                        <input type="text" name="phone" placeholder={t('translations.phonenumber')}
                               className="form-control text-16px font-normal text-light-grey"
                               value={formData.phone} onChange={handleChange}
                               readOnly={userAccessLevel !== 'org_admin'}/>
                    </div>
                </div>
                <div className='my_organization_details_cards_row'>
                    <div className='my_organization_details_card_details'>
                        <div
                            className='font-semibold text-20px text-blue mb-2'>{t('translations.organizationdescription')}</div>
                        <div className='my_organization_details_card_description'>
                        <textarea
                            className='form-control text-16px font-normal text-light-grey my_organization_details_card_description_textarea'
                            name="description"
                            rows={13}
                            defaultValue={formData.description} onChange={handleChange} required
                            readOnly={userAccessLevel !== 'org_admin'}/>
                        </div>
                    </div>
                    <div className='my_organization_details_card_details'>
                        <div
                            className='font-semibold text-20px text-blue mb-2'>{t('translations.socialsAndAuthentication')}</div>
                        <div className="input-group">
                            <span className="input-group-text"><i className="fab fa-instagram"></i></span>
                            <input type="text" name="instagram" value={formData.instagram}
                                   className="form-control text-ligh-grey" onChange={handleChange} pattern="https?://.+"
                                   readOnly={userAccessLevel !== 'org_admin'}/>
                        </div>
                        <div className="input-group">
                            <span className="input-group-text"><i className="fab fa-facebook"></i></span>
                            <input type="text" name="facebook" value={formData.facebook}
                                   className="form-control text-ligh-grey" onChange={handleChange} pattern="https?://.+"
                                   readOnly={userAccessLevel !== 'org_admin'}/>
                        </div>
                        <div className="input-group">
                            <span className="input-group-text"><i className="fab fa-linkedin"></i></span>
                            <input type="text" name="linkedin" value={formData.linkedin}
                                   className="form-control text-ligh-grey" onChange={handleChange} pattern="https?://.+"
                                   readOnly={userAccessLevel !== 'org_admin'}/>
                        </div>
                        <div className="input-group">
                            <span className="input-group-text"><i className="fab fa-x-twitter"></i></span>
                            <input type="text" name="twitter" value={formData.twitter}
                                   className="form-control text-ligh-grey" onChange={handleChange} pattern="https?://.+"
                                   readOnly={userAccessLevel !== 'org_admin'}/>
                        </div>
                        <div className='text-16px font-normal text-default mt-5'>{t('translations.website')}</div>
                        <input type="text" name="website" placeholder={t('translations.website')}
                               className="form-control text-16px font-normal text-light-grey"
                               value={formData.website} onChange={handleChange}
                               readOnly={userAccessLevel !== 'org_admin'}/>
                    </div>
                </div>
                <div className='my_organization_details_cards_row'>
                    <div className='my_organization_details_card_details'>
                        <div
                            className='font-semibold text-20px text-blue mb-2'>{t('translations.address')}</div>
                        <ReactSelect2Component
                            options={countryOptions}
                            placeholder={formData.country}
                            value={formData.country}
                            width={null}
                            handleSelect={handleSelect}/>
                        <input type="text" name="region" placeholder={t('translations.region')}
                               className="form-control text-16px font-normal text-light-grey"
                               value={formData.region} onChange={handleChange}
                               readOnly={userAccessLevel !== 'org_admin'}/>
                        <input type="text" name="zip_code" placeholder={t('translations.zipcode')}
                               className="form-control text-16px font-normal text-light-grey"
                               value={formData.zip_code} onChange={handleChange}
                               readOnly={userAccessLevel !== 'org_admin'}/>
                        <input type="text" name="address_road" placeholder={t('translations.streetname')}
                               className="form-control text-16px font-normal text-light-grey"
                               value={formData.address_road} onChange={handleChange}
                               readOnly={userAccessLevel !== 'org_admin'}/>
                        <input type="text" name="address_number" placeholder={t('translations.streetnumber')}
                               className="form-control text-16px font-normal text-light-grey"
                               value={formData.address_number} onChange={handleChange}
                               readOnly={userAccessLevel !== 'org_admin'}/>
                    </div>
                    {Object.keys(position).length !== 0 && (
                        <div className='my_organization_details_card_map'>
                            <MyOrganizationMap position={position} setPosition={setPosition}/>
                        </div>
                    )}
                </div>
                {userAccessLevel === 'org_admin' && (
                    <div className='my_organization_details_cards_row'>
                        <div className='my_organization_details_card_details' style={{width: '100%'}}>
                            <div
                                className='font-semibold text-20px text-blue mb-2'>{t('translations.organizationmembers')}</div>
                            <div
                                className="my_organization_details_invite_btn font-medium text-16px"
                                onClick={handleShowInvitation}>{t('translations.invitemoremembers')}</div>
                        </div>
                    </div>
                )}
                {userAccessLevel === 'org_admin' && (
                    <button type="submit"
                            className="btn my_organization_details_button mt-3 mb-5">{t('translations.saveChanges')}</button>
                )}
            </form>
            <Modal
                show={showInvitation}
                onHide={handleCloseInvitation}
                backdrop="static"
                keyboard={false}
                dialogClassName="invitation_modal"
                centered
            >
                <form onSubmit={handleSendInvitation} className='invitation_modal_container'>
                    <h2
                        className='invitation_modal_header font-medium'>{t('translations.Inviteanewmember')}</h2>
                    <div
                        className='invitation_modal_text font-medium text-16px'>{t('translations.enteremailaddress')}</div>
                    <div className='invitation_modal_label font-medium text-16px'>{t('translations.email')}</div>
                    <input type='email' name='invitationEmail'
                           className='invitation_modal_input_email font-medium text-16px'
                           placeholder={t('translations.writeemailaddress')}
                           onChange={(e) => setInvitationEmail(e.target.value)}/>
                    <div className='invitation_modal_btns_container'>
                        <div className='invitation_modal_cancel_btn font-medium text-23px'
                             onClick={handleCloseInvitation}>{t('translations.cancel')}</div>
                        <button type='submit'
                                className='btn invitation_modal_send_btn font-medium text-23px'>{t('translations.sendinvitation')}</button>
                    </div>
                </form>
            </Modal>
        </>
    );
}
