import React, {useState} from "react";
import {MapContainer, TileLayer, Marker, useMapEvents} from 'react-leaflet';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';

delete L.Icon.Default.prototype._getIconUrl;
L.Icon.Default.mergeOptions({
    iconUrl: "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-icon.png",
    shadowUrl: "https://cdnjs.cloudflare.com/ajax/libs/leaflet/1.7.1/images/marker-shadow.png"
});

const MapMarker = ({position, setPosition}) => {
    const eventHandlers = {
        dragend: (e) => {
            setPosition({lat: e.target.getLatLng().lat, lon: e.target.getLatLng().lng});
        }
    };
    return (
        <Marker
            position={position}
            draggable={true}
            eventHandlers={eventHandlers}
        />
    );
};

export default function MyOrganizationMap({position, setPosition}) {
    return (
        <MapContainer center={position} zoom={18} style={{height: "100%", width: "100%"}}
                      zoomControl={false}>
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            />
            <MapMarker position={position} setPosition={setPosition}/>
        </MapContainer>
    );
}
