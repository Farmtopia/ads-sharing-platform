import React, {useState} from "react";
import arrowDown from "../../../public/img/btn_arrow_down.png";

export default function RegisterDropDownBtn({placeholderText, btnContentList, maxWidth, updateReturnValue, type}) {
    const [roleText, setRoleText] = useState(null);
    const [btnRoleStatus, setBtnRoleStatus] = useState(false);

    const roleClick = (e, value) => {
        e.preventDefault();
        setRoleText(e.target.innerHTML);
        updateReturnValue(value, type);
        setBtnRoleStatus(false);
    }

    const updateBtnRoleStatus = (e) => {
        e.preventDefault();
        setBtnRoleStatus(!btnRoleStatus);
    }

    return (
        <div className='register_dropdown_container'>
            <div className='register_arrow_button_container inter-regular-16'
                 style={{
                     border: btnRoleStatus ? '1px solid #827E74' : '1px solid #E6E2DB',
                     maxWidth: maxWidth,
                     color: roleText ? '#827E74' : '#B7B2A7',
                     opacity: roleText ? '1' : '0.5'
                 }}
                 onClick={(e) => updateBtnRoleStatus(e)}>{roleText ? roleText : placeholderText}
                <img className='register_btn_arrow' src={arrowDown}/>
            </div>
            <div className='register_button_content_container'
                 style={{display: btnRoleStatus ? 'block' : null, maxWidth: maxWidth}}>
                {btnContentList?.map((cont, i) => {
                    return (
                        <div key={`Dropdown-content-${i}`} className='register_button_content inter-regular-16'
                             onClick={(e) => roleClick(e, cont)}>{cont}</div>
                    );
                })}
            </div>
        </div>
    );
}
