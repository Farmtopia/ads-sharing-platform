import React from "react";
import lensIcon from "../../../public/img/lens_icon.png";
import { useTranslation } from 'react-i18next';

export default function SearchComponent() {
    const { t } = useTranslation();
    return (
        <div className='search_container'>
            <input type='text' className='search_input text-20px font-normal' placeholder={t('translations.searchlocation')} />
            <img className='search_lens_icon' src={lensIcon} alt='Lens Icon' />
        </div>
    );
}
