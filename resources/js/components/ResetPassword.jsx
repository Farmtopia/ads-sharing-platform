import React, { useState, useEffect } from "react";
import { useTranslation } from 'react-i18next';
import passEye from "../../../public/img/pass_eye.png";
import passEyeUndo from "../../../public/img/pass_eye_undo.png";
import { resetPassword } from "../APIs/Auth.jsx";
import { resetPasswordVerify } from "../APIs/Auth.jsx";


export default function ResetPassword({ }) {
    const { t } = useTranslation(); // Initialize translation hook
    const token = document.getElementById('resetpassword').getAttribute('data-token');
    const email_encrypted = document.getElementById('resetpassword').getAttribute('data-email');
    const [password, setPassword] = useState('');
    const [password_confirmation, setpassword_confirmation] = useState('');
    const [passType, setPassType] = useState('password');
    const [email, setEmail] = useState('');
    const [isValidLink, setIsValidLink] = useState(false);
    const eyeStyle = {
        height: passType === 'password' ? '0.875rem' : '1.25rem',
        top: passType === 'password' ? '1.094rem' : '0.9rem'
    }

    const eyeClick = (e) => {
        e.preventDefault();
        if (passType === 'password') {
            setPassType('text');
        } else {
            setPassType('password');
        }
    }

    const handleEmailChange = (event) => {
        setEmail(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handlepassword_confirmationChange = (event) => {
        setpassword_confirmation(event.target.value);
    };

    const handleResetPassword = async () => {
        try {
            const response = await resetPassword(token, email, email_encrypted, password, password_confirmation);
            console.log("Response:", response);
            if (response.success) {
                console.log([response.message]);
                window.location.href = '/signup';
            } else {
                console.log([response.message]);
            }
        } catch (error) {
            console.error("Reset password error:", error);
        }
    }

    useEffect(() => {
        console.log("Verifying reset password link...");
        resetPasswordVerify(token, email_encrypted)
            .then((data) => {
                console.log("Verification response:", data);
                if (data.success) {
                    setIsValidLink(true);
                } else {
                    setIsValidLink(false);
                }
            })
            .catch(async (error) => {
                console.error("Verification error:", error);
                setIsValidLink(false);
            });
    }, [token, email_encrypted]);


    return (
        <div>
            { isValidLink ? (
                <div>
                    <div className='password_recovery_welcome_txt mt-3 align-items-center'>
                        <p className='welcome_txt_1 text-26px font-ex-bold'>{t('translations.passwordrecovery')}</p>
                        <p className='welcome_txt_2 text-18px'>{t('translations.passwordrecoverytext')}</p>
                    </div>
                    <div className="password_recovery_container align-items-center">
                    <label className='login_label font-medium'>{t('translations.email')}</label>
                        <div className='login_pass_eye_container mb-3'>
                            <input className='login_password_input inter-regular-16' type='email' required
                                autoComplete='email' value={email} onChange={handleEmailChange} />
                        </div>
                        <label className='login_label font-medium'>{t('translations.newpassword')}</label>
                        <div className='login_pass_eye_container mb-3'>
                            <img className='login_pass_eye' src={passType === 'password' ? passEye : passEyeUndo}
                                    onClick={(e) => eyeClick(e)} style={eyeStyle}/>
                            <input className='login_password_input inter-regular-16' type={passType} required
                                    autoComplete='new-password' value={password} onChange={handlePasswordChange}/>
                        </div>
                        <label className='login_label font-medium'>{t('translations.validatepasswordplaceholder')}</label>
                        <div className='login_pass_eye_container mb-3'>
                            <img className='login_pass_eye' src={passType === 'password' ? passEye : passEyeUndo}
                                    onClick={(e) => eyeClick(e)} style={eyeStyle}/>
                            <input className='login_password_input inter-regular-16' type={passType} required
                                    autoComplete='new-password' value={password_confirmation} onChange={handlepassword_confirmationChange}/>
                        </div>
                        <button className="login_button font-medium text-23px" onClick={handleResetPassword}>
                            {t('translations.changepassword')}
                        </button>
                    </div>
                </div>
            ) : (
                <div className="password_recovery_welcome_txt text-23px mt-3 align-items-center">
                    {t('translations.wrongresetlink')}
                </div>
            )}
        </div>
    );
}