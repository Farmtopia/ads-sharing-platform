import React, {useEffect, useState} from "react";
import "../../../public/css/Marketplace.css";
import FiltersMarketplace from "./marketplace/FiltersMarketplace.jsx";
import MapMarketplace from "./marketplace/MapMarketplace.jsx";
import GalleryMarketplace from "./marketplace/GalleryMarketplace.jsx";
import {getOrganizationListings} from "../APIs/Listings.jsx";

export default function MarketplaceView() {
    const token = sessionStorage.getItem('authToken');
    const [mapSwithcer, setMapSwithcer] = useState('gallery');
    const [listingData, setListingData] = useState(null);

    const updateMapSwithcer = (newVal) => setMapSwithcer(newVal);

    useEffect(() => {
        getOrganizationListings(token).then(data => {
            console.log(data);
            if (Object.keys(data).length > 0 && data.success) {
                let newArray = [...data.listings];
                setListingData(newArray);
            }
        }).catch(err => console.log(err));
    }, []);

    return (
        <div className='marketplace_container'>
            <FiltersMarketplace listingData={listingData}/>
            {mapSwithcer === 'map' ? (
                <MapMarketplace listingData={listingData} mapSwithcer={mapSwithcer}
                                updateMapSwithcer={updateMapSwithcer}/>
            ) : (
                <GalleryMarketplace listingData={listingData} mapSwithcer={mapSwithcer}
                                    updateMapSwithcer={updateMapSwithcer}/>
            )}
        </div>
    );
}
