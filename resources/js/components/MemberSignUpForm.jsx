import React, {useState, useEffect} from "react";
import { useNavigate } from "react-router-dom";
import "../../../public/css/RegisterDropDownBtn.css";
import passEye from "../../../public/img/pass_eye.png";
import passEyeUndo from "../../../public/img/pass_eye_undo.png";
import RegisterDropDownBtn from "./RegisterDropDownBtn.jsx";
import { registermember } from "../APIs/Auth.jsx";
import {allCountries} from "../APIs/Countries.jsx";
import ToastContainer from './toasts/ToastContainer.jsx'
import { useTranslation } from 'react-i18next';


export default function MemberSignupForm({setResp}) {
    const { t } = useTranslation(); // Initialize translation hook
    const [countries, setCountries] = useState();
    const [passType, setPassType] = useState('password');
    const [successMessages, setSuccessMessages] = useState([])
    const [infoMessages, setInfoMessages] = useState([])
    const [formData, setFormData] = useState({
        firstname: '',
        lastname: '',
        email: '',
        password: '',
        confirm_password: '',
        country_id: '',
        region: '',
        zip_code: '',
        address_road: '',
        address_number: '',
    });

    const token = '21e262616115c1b82765fa9816ec64a898c9d735';

    const eyeStyle = {
        height: passType === 'password' ? '0.875rem' : '1.25rem',
        top: passType === 'password' ? '1.094rem' : '0.9rem'
    };

    const eyeClick = (e) => {
        e.preventDefault();
        setPassType(passType === 'password' ? 'text' : 'password');

    };

    useEffect(() => {

        const fetchCoutries = async () => {
            const resp = await allCountries()
            setCountries(resp.countries.map(country => ({
                name: country.name,
                value: country.id
            })))
        }
        fetchCoutries();
    }, [])

    const handleInputChange = (e) => {
        const {name, value} = e.target;
        setFormData((prevData) => ({...prevData, [name]: value}));
    };

    const handleSelect = (value, type) => {
        if (type === 'country') {
            const countryId = countries.find(country => country.name === value)?.value || '';
            setFormData((prevData) => ({...prevData, country_id: countryId}));
        }
    };

    const signUpButtonHandler = async (e) => {
        e.preventDefault();
        try {
            const resp = await registermember(token, formData);
            setResp(resp); // Set the response
            console.log('Registration response:', resp);
        } catch (error) {
            console.error('Error during registration:', error);
        }
    };

    return (
        <div className='login_container'>
            <label className='login_label font-medium mb-1' htmlFor='firstname'>{t('translations.firstname')}</label>
            <input
                className='login_email_input inter-regular-16'
                type='text'
                name='firstname'
                id='firstname'
                placeholder={t('translations.firstnameplaceholder')}
                autoFocus
                required
                autoComplete='new-password'
                value={formData.firstname}
                onChange={handleInputChange}
            />
            <label className='login_label font-medium mb-1' htmlFor='lastname'>{t('translations.lastname')}</label>
            <input
                className='login_email_input inter-regular-16'
                type='text'
                name='lastname'
                id='lastname'
                placeholder={t('translations.lastnameplaceholder')}
                autoFocus
                required
                autoComplete='new-password'
                value={formData.lastname}
                onChange={handleInputChange}
            />
            <label className='login_label font-medium my-1' htmlFor='email'>{t('translations.email')}</label>
            <input
                className='login_email_input inter-regular-16'
                type='email'
                name='email'
                id='email'
                placeholder={t('translations.emailplaceholder')}
                required
                autoComplete='new-password'
                value={formData.email}
                onChange={handleInputChange}
            />
            <label className='login_label font-medium my-1' htmlFor='password'>{t('translations.createpassword')}</label>
            <div className='login_pass_eye_container'>
                <img
                    className='login_pass_eye'
                    src={passType === 'password' ? passEye : passEyeUndo}
                    onClick={eyeClick}
                    style={eyeStyle}
                    alt="Toggle Password Visibility"
                />
                <input
                    className='login_password_input inter-regular-16'
                    type={passType}
                    name='password'
                    id='password'
                    placeholder={t('translations.createpasswordplaceholder')}
                    required
                    autoComplete='new-password'
                    value={formData.password}
                    onChange={handleInputChange}
                />
            </div>
            <label className='login_label font-medium my-1' htmlFor='confirm_password'>{t('translations.validatepassword')}</label>
            <div className='login_pass_eye_container'>
                <img
                    className='login_pass_eye'
                    src={passType === 'password' ? passEye : passEyeUndo}
                    onClick={eyeClick}
                    style={eyeStyle}
                    alt="Toggle Password Visibility"
                />
                <input
                    className='login_password_input inter-regular-16'
                    type={passType}
                    name='confirm_password'
                    id='confirm_password'
                    placeholder={t('translations.validatepasswordplaceholder')}
                    required
                    autoComplete='new-password'
                    value={formData.confirm_password}
                    onChange={handleInputChange}
                />
            </div>
            <label className='login_label font-medium my-1'>{t('translations.address')}</label>
            <RegisterDropDownBtn
                placeholderText={t('translations.country')}
                btnContentList={countries && countries.map(a => a.name)}
                maxWidth={"27.5rem"}
                updateReturnValue={handleSelect}
                type={'country'}
            />
            <input
                className='login_email_input inter-regular-16'
                type='text'
                name='zip_code'
                placeholder={t('translations.zipcode')}
                autoFocus
                required
                autoComplete='new-password'
                value={formData.zip_code}
                onChange={handleInputChange}
            />
            <input
                className='login_email_input inter-regular-16'
                type='text'
                name='region'
                placeholder={t('translations.region')}
                autoFocus
                required
                autoComplete='new-password'
                value={formData.region}
                onChange={handleInputChange}
            />
            <div className="address-container">
                <input
                    className='login_email_input inter-regular-16'
                    type='text'
                    name='address_road'
                    placeholder={t('translations.streetaddress')}
                    autoFocus
                    required
                    autoComplete='new-password'
                    value={formData.address_road}
                    onChange={handleInputChange}
                />
                <input
                    className='login_email_input inter-regular-16'
                    type='text'
                    name='address_number'
                    placeholder={t('translations.streetnumber')}
                    autoFocus
                    required
                    autoComplete='new-password'
                    value={formData.address_number}
                    onChange={handleInputChange}
                />
            </div>
            <button className='login_button text-23px font-medium mt-4' onClick={signUpButtonHandler}>{t('translations.signup')}
            </button>
            <ToastContainer
                successMessages={successMessages}
                setSuccessMessages={setSuccessMessages}
                infoMessages={infoMessages}
                setInfoMessages={setInfoMessages}
            />
        </div>
    );
}
