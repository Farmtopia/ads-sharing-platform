import React, { useState, useEffect } from "react";
import "../../../public/css/RentalCard.css";
import { listingsById } from "../APIs/Listings.jsx";
import { getEquipmentSubCategories } from "../APIs/Equipment.jsx"; // Import the function to fetch subcategory details
import { t } from "i18next";

const RentalCard = ({ cardId }) => {
    const token = sessionStorage.getItem('authToken');

    const [listing, setListing] = useState(null);
    const [subcategory, setSubcategory] = useState(null); // State to store the subcategory name
    const [currentImageIndex, setCurrentImageIndex] = useState(0);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [images, setImages] = useState([]);

    const handleNextImage = () => {
        setCurrentImageIndex((prevIndex) => (prevIndex + 1) % images.length);
    };

    const handlePrevImage = () => {
        setCurrentImageIndex((prevIndex) =>
            (prevIndex - 1 + images.length) % images.length
        );
    };

    useEffect(() => {
        listingsById(token, cardId)
            .then((data) => {
                if (data.success) {
                    setListing(data.listing);
                    if (data.listing.media.length > 0) {
                        setImages(data.listing.media.map(filename => `/storage/material/${filename}`));
                    } else {
                        setImages(["/img/listingplaceholder.png"]);
                    }
                    // Fetch the subcategory name
                    getEquipmentSubCategories(data.listing.equipment_category_id)
                        .then(subcategoryData => {
                            if (subcategoryData.success) {
                                const subcategory = subcategoryData.types.find(type => type.value === data.listing.equipment_subcategory_id);
                                if (subcategory) {
                                    setSubcategory(subcategory.label);
                                } else {
                                    setSubcategory("Unknown Subcategory");
                                }
                            } else {
                                setSubcategory("Unknown Subcategory");
                            }
                        })
                        .catch(() => setSubcategory("Unknown Subcategory"));
                } else {
                    setError("Failed to fetch listing data");
                }
            })
            .catch(() => setError("Error retrieving listing"))
            .finally(() => setLoading(false));
    }, [token, cardId]);

    // Show loading or error message
    if (loading) return <p>Loading...</p>;
    if (error) return <p className="text-danger">{error}</p>;

    return (
        <div className="rental-card">
            <div className="rental-image-container">
                <button className="prev-button" onClick={handlePrevImage}>
                    <i className="text-white text-28px bi bi-caret-left-fill"></i>
                </button>
                <img
                    src={images[currentImageIndex]}
                    className="rental-card-image"
                    onClick={() => window.location.href = `/listingview/${cardId}`}
                    alt={`Slide ${currentImageIndex + 1}`}
                />
                <button className="next-button" onClick={handleNextImage}>
                    <i className="text-white text-28px bi bi-caret-right-fill"></i>
                </button>
                <span className="rental-badge"
                    onClick={() => window.location.href = `/listingview/${cardId}`}>{t("translations.forrent")}</span>
                <div className="image-dots">
                    {images.map((_, index) => (
                        <span
                            key={index}
                            className={`dot ${index === currentImageIndex ? "active" : ""}`}
                        ></span>
                    ))}
                </div>
            </div>
            <div className="rental-card-content">
                <h5 className="rental-card-title">{subcategory} {listing?.equipment_model_name}</h5> {/* Display the subcategory */}
                <p className="rental-card-price">{Math.floor(listing?.min_price)} - {Math.floor(listing?.max_price)} {listing?.delivery_fee_unit}/{t("translations.perday")}</p>
            </div>
            <div className="rental-card-footer">
                <div className="rental-profile-container">
                <img src={listing.user?.profile_photo ? `/storage/profile_photos/${listing.user.profile_photo}` : "/img/profile-placeholder.webp"} alt="Owner" className="rental-profile-image" />
                    <p className="mb-0">{listing?.user?.firstname} {listing?.user?.lastname}</p>
                </div>

                {/* <div className="rental-rating-container">
                    <span className="rental-star">★</span>
                    <p className="rental-rating">4.5 / 5</p>
                </div> */}

            </div>
        </div>
    );
};

export default RentalCard;