import React, {useEffect, useState} from "react";
import Select from 'react-select'

export default function ReactSelect2Component({options, placeholder, value, width, handleSelect, padding}) {
    return (
        <Select options={options} placeholder={placeholder} value={value}
                styles={
                    {
                        valueContainer: (baseStyles, state) => (
                            {
                                ...baseStyles,
                                padding: padding ? padding : '0.438rem 0.938rem',
                            }
                        ),
                        control: (baseStyles, state) => (
                            {
                                ...baseStyles,
                                maxWidth: width + 'rem',
                                width: '100%',
                                borderRadius: '8px',
                                border: '1px solid #E6E2DB',
                                cursor: 'pointer',
                                borderColor: state.isFocused ? '#827E74' : null,
                                boxShadow: state.isFocused && 'none',
                                "&:hover": {
                                    borderColor: '#E6E2DB'
                                },
                            }
                        ),
                        menu: (baseStyles, state) => (
                            {
                                ...baseStyles,
                                maxWidth: width + 'rem',
                                width: '100%',
                                borderRadius: '8px',
                                border: '1px solid #827E74',
                                boxShadow: 'none',
                                "&:focus-visible": {
                                    outlineColor: '#827E74',
                                    outlineWidth: 0,
                                },
                            }
                        ),
                        option: (baseStyles, state) => (
                            {
                                ...baseStyles,
                                backgroundColor: state.isFocused || state.isSelected ? '#F2F0ED' : baseStyles.backgroundColor,
                                color: state.isFocused || state.isSelected ? '#57574F' : baseStyles.color,
                                cursor: state.isFocused || state.isSelected ? 'pointer' : baseStyles.cursor,
                                outline: state.isFocused || state.isSelected ? 'none' : baseStyles.outline,
                                "&:active": {
                                    backgroundColor: '#F2F0ED',
                                    color: '#57574F',
                                    cursor: 'pointer',
                                },
                                "&:focus-visible": {
                                    outline: 'none',
                                    cursor: 'pointer',
                                },
                            }
                        ),
                        dropdownIndicator: (baseStyles, state) => (
                            {
                                ...baseStyles,
                                color: '#E6E2DB',
                            }
                        ),
                        indicatorSeparator: (baseStyles, state) => (
                            {
                                ...baseStyles,
                                display: 'none',
                            }
                        ),
                    }
                }
                onChange={handleSelect}/>
    );
}
