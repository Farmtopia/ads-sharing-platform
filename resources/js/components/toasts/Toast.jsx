import React,{useState , useEffect} from 'react'
import "../../../../public/css/toasts.css"


function Toast({messages , type , setMessages, order }) {

    const [title,setTitle] = useState("")

    useEffect(() => {
        if(type === "error"){
            setTitle("Error")
        }else if(type === "success"){
            setTitle("Success")
        }else if(type === "warning"){
            setTitle("Warning")
        }else{
            setTitle("Update")
        }
    }, [messages])

    const clearMessages = () => {
        setMessages([])
    }


    return (
        <div>
            {messages.length>0 && 
                <div className={`sticky-toast sticky-${type}-toast show`} style={{top:`${5+(5*order)}rem`}}>
                    <div className="d-flex align-items-center justify-content-between h-100">
                        <img src={`/img/Toasts/${type}_icon.svg`} className='toast-icon' />
                        <div className="toast-message-container d-flex align-items-center justify-content-between flex-row my-2">
                            <span className="toast-message text-wrap">
                                <p className='inter-semi-bold-18 text-grey'>{title}</p>
                                {messages.map((message, index) => (
                                    <span className="inter-regular-16 text-grey my-1" key={index}>{message}</span>
                                ))}
                            </span>
                            <button className="toast-message close-toast-icon border-0 p-0" onClick={clearMessages}>
                                <img src={`/img/Toasts/${type}_close.svg`} className='img-contained' />
                            </button>
                        </div>
                    </div>
                </div>
            }
        </div>
        
    )
}

export default Toast