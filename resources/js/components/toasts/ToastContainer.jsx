// ToastContainer.js

import React from 'react';
import Toast from '../toasts/Toast';

function ToastContainer({
    errorMessages,
    setErrorMessages,
    successMessages,
    setSuccessMessages,
    infoMessages,
    setInfoMessages,
    warningMessages,
    setWarningMessages
}) {
    const getToastOrder = () => {
        const toastList = [
            { type: 'error', show: errorMessages && errorMessages.length > 0, setter: setErrorMessages, messages: errorMessages },
            { type: 'success', show: successMessages && successMessages.length > 0, setter: setSuccessMessages, messages: successMessages },
            { type: 'warning', show: warningMessages && warningMessages.length > 0, setter: setWarningMessages, messages: warningMessages },
            { type: 'info', show: infoMessages && infoMessages.length > 0, setter: setInfoMessages, messages: infoMessages }
        ];

        const visibleToasts = toastList.filter(toast => toast.show && toast.setter);

        return visibleToasts.map((toast, index) => ({ ...toast, order: index + 1 }));
    };

    const orderedToasts = getToastOrder();

    return (
        <div>
            {orderedToasts.map((toast) => (
                <Toast
                    key={toast.type}
                    messages={toast.messages}
                    type={toast.type}
                    setMessages={toast.setter}
                    order={toast.order}
                />
            ))}
        </div>
    );
}

export default ToastContainer;