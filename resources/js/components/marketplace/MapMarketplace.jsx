import React, {useEffect, useState} from "react";
import { MapContainer, TileLayer, useMap } from 'react-leaflet';
import L from 'leaflet';
import 'leaflet/dist/leaflet.css';
import CustomMarkerMarketplace from "./CustomMarkerMarketplace.jsx";
import SearchComponent from "../SearchComponent.jsx";
import MapSwitchButtonComponent from "./MapSwitchButtonComponent.jsx";

const FitToBounds = ({ listingData }) => {
    const map = useMap();

    useEffect(() => {
        if (!listingData || !map) return;

        const bounds = L.latLngBounds(listingData.map(loc => [loc.lat, loc.lon]));
        map.fitBounds(bounds, { padding: [50, 50] });
    }, [listingData, map]);580

    return null;
};

export default function MapMarketplace({listingData, mapSwithcer, updateMapSwithcer}) {
    return (
        <MapContainer center={[37.9715, 23.7267]} zoom={7} style={{ height: "260", width: "100%", position: 'relative' }} zoomControl={false}>
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            />
            <FitToBounds listingData={listingData} />
            <SearchComponent/>
            <MapSwitchButtonComponent mapSwithcer={mapSwithcer} updateMapSwithcer={updateMapSwithcer}/>
            {listingData?.map((lData, i) => {
                if (lData.lat && lData.lon) {
                    return (
                        <CustomMarkerMarketplace key={`CustomMarket-${i}`} lData={lData}/>
                    );
                }
            })}
        </MapContainer>
    );
}
