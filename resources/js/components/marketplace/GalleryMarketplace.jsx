import React, {Fragment} from "react";
import SearchComponent from "../SearchComponent.jsx";
import MapSwitchButtonComponent from "./MapSwitchButtonComponent.jsx";
import RentalCard from "../../components/RentalCard.jsx";

export default function GalleryMarketplace({listingData, mapSwithcer, updateMapSwithcer}) {
    return (
        <div className='marketplace_gallery_container'>
            <div className='marketplace_gallery_header_container'>
                <SearchComponent/>
                <MapSwitchButtonComponent mapSwithcer={mapSwithcer} updateMapSwithcer={updateMapSwithcer}/>
            </div>
            <div className='marketplace_gallery_body_container'>
                {listingData?.map((listData, l) => {
                    return (
                        <Fragment key={listData.id}>
                            <RentalCard cardId={listData.id}/>
                        </Fragment>
                    );
                })}
            </div>
        </div>
    );
}
