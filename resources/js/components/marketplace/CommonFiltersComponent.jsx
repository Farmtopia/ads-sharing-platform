import React, {useEffect, useState} from "react";
import {useTranslation} from 'react-i18next';
import ReactSelect2Component from "../ReactSelect2Component.jsx";
import Slider from 'rc-slider';
import 'rc-slider/assets/index.css';

export default function CommonFiltersComponent({listingData}) {
    const {t} = useTranslation();
    const [minValue, setMinValue] = useState(0);
    const [maxValue, setMaxValue] = useState(0);
    const [showMinValue, setShowMinValue] = useState(0);
    const [showMaxValue, setShowMaxValue] = useState(0);

    function log(value) {
        setShowMinValue(value[0]);
        setShowMaxValue(value[1]);
    }

    const updateMinValue = (value) => {
        if (/^\d*\.?\d*$/.test(value)) {
            setShowMinValue(parseFloat(value));
        }
    }
    const updateMaxValue = (value) => {
        if (/^\d*\.?\d*$/.test(value)) {
            setShowMaxValue(parseFloat(value));
        }
    }
    const updateBlurMinValue = (value) => {
        if (/^\d*\.?\d*$/.test(value)) {
            if (value < minValue) {
                setShowMinValue(minValue);
            } else if (value >= showMaxValue) {
                setShowMinValue(showMaxValue - 1);
            } else {
                setShowMinValue(parseFloat(value));
            }
        }
    }
    const updateBlurMaxValue = (value) => {
        if (/^\d*\.?\d*$/.test(value)) {
            if (value <= showMinValue) {
                setShowMaxValue(showMinValue + 1);
            } else if (value > maxValue) {
                setShowMaxValue(maxValue);
            } else {
                setShowMaxValue(parseFloat(value));
            }
        }
    }

    const [options, setOptions] = useState([
        {value: 'equipment1', label: 'Equipment 1'},
        {value: 'equipment2', label: 'Equipment 2'},
        {value: 'equipment3', label: 'Equipment 3'},
        {value: 'equipment4', label: 'Equipment 4'},
        {value: 'equipment5', label: 'Equipment 5'},
        {value: 'equipment6', label: 'Equipment 6'},
    ]);
    const handleSelect = (val) => {
    }

    useEffect(() => {
        if (!listingData || listingData.length === 0) return;

        let minArray = listingData
            .map(item => parseFloat(item.min_price))
            .filter(price => !isNaN(price))
            .sort((a, b) => a - b);

        let maxArray = listingData
            .map(item => parseFloat(item.max_price))
            .filter(price => !isNaN(price))
            .sort((a, b) => a - b);

        if (minArray.length === 0 || maxArray.length === 0) return;
        setMinValue(minArray[0]);
        setShowMinValue(minArray[0]);
        setMaxValue(maxArray[maxArray.length - 1]);
        setShowMaxValue(maxArray[maxArray.length - 1]);
    }, [listingData, t]);

    return (
        <div className='common_filters_container'>
            <div className='common_filters_label font-semibold text-16px'>{t('translations.showavailableonly')}</div>
            <div className='common_filters_checkbox_cont font-normal text-16px'><input type="checkbox"
                                                                                       className='common_filters_checkbox'/>{t('translations.availableonly')}
            </div>
            <div className='common_filters_label font-semibold text-16px'>{t('translations.photoavailable')}</div>
            <div className='common_filters_checkbox_cont font-normal text-16px'><input type="checkbox"
                                                                                       className='common_filters_checkbox'/>{t('translations.withphoto')}
            </div>
            <div className='common_filters_label font-semibold text-16px'>{t('translations.typeofmachinery')}</div>
            <ReactSelect2Component
                options={options}
                placeholder={'Επιλογή...'}
                value={null}
                width={null}
                handleSelect={handleSelect}/>
            <div
                className='common_filters_label font-semibold text-16px mt-4'>{t('translations.categoryofmachinery')}</div>
            <ReactSelect2Component
                options={options}
                placeholder={'Επιλογή...'}
                value={null}
                width={null}
                handleSelect={handleSelect}/>
            <div
                className='common_filters_label font-semibold text-16px mt-4'>{t('translations.manufacturermodel')}</div>
            <ReactSelect2Component
                options={options}
                placeholder={'Επιλογή...'}
                value={null}
                width={null}
                handleSelect={handleSelect}/>
            <div className='common_filters_label font-semibold text-16px mt-4'>{t('translations.costofequipment')}</div>
            <div className='common_filters_bar_container'>
                <div className='common_filters_bar'>
                    <Slider range min={minValue} max={maxValue}
                            step={1} onChange={log}
                            defaultValue={[showMinValue, showMaxValue]} value={[showMinValue, showMaxValue]}
                            className='common_filters_slider'/>
                </div>
                <div className='common_filters_bar_minmax_inputs_cont'>
                    <input type='number' className='common_filters_bar_input' value={showMinValue}
                           onChange={(e) => updateMinValue(e.target.value)}
                           onBlur={(e) => updateBlurMinValue(e.target.value)}/>
                    <input type='number' className='common_filters_bar_input' value={showMaxValue}
                           onChange={(e) => updateMaxValue(e.target.value)}
                           onBlur={(e) => updateBlurMaxValue(e.target.value)}/>
                </div>
            </div>
            <div className='common_filters_label font-semibold text-16px mt-4'>{t('translations.rentaltype')}</div>
            <div className='common_filters_checkbox_cont font-normal text-16px mb-0'><input type="checkbox"
                                                                                            className='common_filters_checkbox'/>{t('translations.standalone')}
            </div>
            <div className='common_filters_checkbox_cont font-normal text-16px mb-0'><input type="checkbox"
                                                                                            className='common_filters_checkbox'/>{t('translations.compinewithlabor')}
            </div>
        </div>
    );
}
