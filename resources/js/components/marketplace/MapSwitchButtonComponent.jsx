import React from 'react';
import { useTranslation } from 'react-i18next';

export default function MapSwitchButtonComponent({mapSwithcer, updateMapSwithcer}) {
    const { t } = useTranslation();
    return (
        <div className='map_switch_container'>
            <div className={`map_switch_default font-normal text-20px ${mapSwithcer === 'map' && 'map_switch_default_selected'}`} onClick={() => updateMapSwithcer('map')}>{t('translations.map')}</div>
            <div className={`map_switch_default font-normal text-20px ${mapSwithcer === 'gallery' && 'map_switch_default_selected'}`} onClick={() => updateMapSwithcer('gallery')}>{t('translations.gallery')}</div>
        </div>
    );
}
