import React from "react";
import {useTranslation} from 'react-i18next';
import MarketPlaceCalendar from "./MarketPlaceCalendar.jsx";

export default function MoreFiltersComponent({listingData}) {
    const {t} = useTranslation();
    return (
        <div className='more_filters_container'>
            <MarketPlaceCalendar/>
            <div className='more_filters_btns_container'>
                <div className='more_filters_btn_clear font-medium text-16px'>{t('translations.clearallfilters')}</div>
                {/*<div className='more_filters_btn_more text-16px'>{t('translations.morefilters')}</div>*/}
            </div>
        </div>
    );
}
