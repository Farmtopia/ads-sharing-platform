import React, {useState, useEffect} from "react";
import {useMap} from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import RentalCard from "../RentalCard.jsx";

export default function CustomMarkerMarketplace({lData, handleMarkerClick}) {
    const map = useMap();
    const [position, setPosition] = useState({x: 0, y: 0});
    const [showCard, setShowCard] = useState(false);

    useEffect(() => {
        if (!map) return;

        const updatePosition = () => {
            const point = map.latLngToContainerPoint([lData.lat, lData.lon]);
            setPosition({x: point.x, y: point.y});
        };

        updatePosition();
        map.on("move", updatePosition);

        return () => {
            map.off("move", updatePosition);
        };
    }, [map, lData]);

    return (
        <div className='custom_div_icon_container' style={{
            left: position.x,
            top: position.y,
            transform: showCard ? 'translate(-50%, -6%)' : 'translate(-50%, -50%)'
        }}>
            <div className='custom_div_icon_content' onClick={() => setShowCard(!showCard)}>Kubota Tractor</div>
            <div className='custom_div_icon_arrow' onClick={() => setShowCard(!showCard)}></div>
            {showCard && (
                <RentalCard cardId={lData.id}/>
            )}
        </div>
    );
}
