import React, {useState, useEffect} from "react";
import {useTranslation} from 'react-i18next';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import arrowLeft from "../../../../public/img/calendar_arrow_left.png";
import arrowRight from "../../../../public/img/calendar_arrow_right.png";
import ReactSelect2Component from "../ReactSelect2Component.jsx";
import {addDays, subDays, addMonths, subMonths} from "date-fns";

const customHeader = (t, startDate, endDate, monthOptions, yearOptions, monthHandleSelect, yearHandleSelect, monthValue, yearValue, decreaseMonth, increaseMonth, changeMonth, changeYear, calWidth) => {
    const formatter = new Intl.DateTimeFormat("en-US", {
        year: "numeric",
        month: "long",
        day: "numeric",
    });

    const decrMonth = () => {
        let currentMonth = -1;
        const firstYear = Number(yearOptions[0].value);
        monthOptions.forEach((option) => {
            if (option.label === monthValue) {
                currentMonth = Number(option.value);
            }
        });

        if (!(currentMonth === 0 && yearValue === firstYear)) {
            decreaseMonth();
        }
    }
    const selectYear = (e) => {
        const firstYear = Number(yearOptions[0].value);
        const lastYear = Number(yearOptions[yearOptions.length - 1].value);

        if (parseInt(e.value) >= firstYear && parseInt(e.value) <= lastYear) {
            changeYear(parseInt(e.value));
        }
    }
    const incrMonth = () => {
        let currentMonth = -1;
        const lastYear = Number(yearOptions[yearOptions.length - 1].value);
        monthOptions.forEach((option) => {
            if (option.label === monthValue) {
                currentMonth = Number(option.value);
            }
        });

        if (!(currentMonth === 11 && yearValue === lastYear)) {
            increaseMonth();
        }
    }

    return (
        <div className='picker_custom_header_container'>
            <div className='picker_custom_header_fromto_container'>
                <div className='picker_custom_header_fromto_in_container'>
                    <div className='picker_custom_header_label text-16px font-normal'>{t('translations.from')}</div>
                    <div
                        className='picker_custom_header_input text-16px font-normal'
                        style={{border: '1px solid #518CB7'}}>{formatter.format(startDate)}</div>
                </div>
                <div className='picker_custom_header_fromto_in_container'>
                    <div className='picker_custom_header_label text-16px font-normal'>{t('translations.to')}</div>
                    <div className='picker_custom_header_input text-16px font-normal'>{formatter.format(endDate)}</div>
                </div>
            </div>
            <div className='picker_custom_header_dt_container'
                 style={{
                     flexDirection: calWidth < 410 && 'column',
                     alignItems: calWidth < 410 && 'start',
                     width: calWidth < 410 && '100%'
                 }}>
                {calWidth >= 410 ? (
                    <>
                        <div className='picker_custom_header_dt_arrow_cont' onClick={() => decrMonth()}>
                            <img className='picker_custom_header_dt_arrow' src={arrowLeft} alt='Arrow Left'/>
                        </div>
                        <ReactSelect2Component
                            options={monthOptions}
                            placeholder={monthValue}
                            value={monthValue}
                            width={null}
                            handleSelect={(e) => changeMonth(parseInt(e.value))}
                            padding={'0.7rem'}
                        />
                        <ReactSelect2Component
                            options={yearOptions}
                            placeholder={yearValue}
                            value={yearValue}
                            width={null}
                            handleSelect={(e) => selectYear(e)}
                            padding={'0.7rem'}
                        />
                        <div className='picker_custom_header_dt_arrow_cont'
                             onClick={() => incrMonth()}>
                            <img className='picker_custom_header_dt_arrow' src={arrowRight} alt='Arrow Right'/>
                        </div>
                    </>
                ) : (
                    <>
                        <div className='picker_custom_header_dt_small_size'>
                            <div className='picker_custom_header_dt_arrow_cont' onClick={() => decrMonth()}>
                                <img className='picker_custom_header_dt_arrow' src={arrowLeft} alt='Arrow Left'/>
                            </div>
                            <div className='picker_custom_header_dt_arrow_cont'
                                 onClick={() => incrMonth()}>
                                <img className='picker_custom_header_dt_arrow' src={arrowRight} alt='Arrow Right'/>
                            </div>
                        </div>
                        <div className='picker_custom_header_dt_small_size'>
                            <ReactSelect2Component
                                options={monthOptions}
                                placeholder={monthValue}
                                value={monthValue}
                                width={null}
                                handleSelect={(e) => changeMonth(parseInt(e.value))}
                                padding={'0.7rem'}
                            />
                            <ReactSelect2Component
                                options={yearOptions}
                                placeholder={yearValue}
                                value={yearValue}
                                width={null}
                                handleSelect={(e) => selectYear(e)}
                                padding={'0.7rem'}
                            />
                        </div>
                    </>
                )}
            </div>
        </div>
    );
}

export default function MarketPlaceCalendar(props, ref) {
    const [winWidth, setWinWidth] = useState(-1);
    const [calWidth, setCalWidth] = useState(-1);
    const {t} = useTranslation();
    const [calendarOpen, setCalendarOpen] = useState(false);
    const sdt = new Date();
    const edt = new Date();
    const [dateRange, setDateRange] = useState([sdt, edt.setDate(edt.getDate() + 1)]);
    const [startDate, endDate] = dateRange;
    const [monthValue, setMonthValue] = useState(sdt.toLocaleString('en-us', {month: 'long'}));
    const [yearValue, setYearValue] = useState(sdt.getFullYear());
    const [monthOptions, setMonthOptions] = useState([
        {value: '0', label: 'January'},
        {value: '1', label: 'February'},
        {value: '2', label: 'March'},
        {value: '3', label: 'April'},
        {value: '4', label: 'May'},
        {value: '5', label: 'June'},
        {value: '6', label: 'July'},
        {value: '7', label: 'August'},
        {value: '8', label: 'September'},
        {value: '9', label: 'October'},
        {value: '10', label: 'November'},
        {value: '11', label: 'December'},
    ]);
    const [yearOptions, setYearOptions] = useState(Array.from({length: (sdt.getFullYear() + 1) - sdt.getFullYear() + 1}, (_, i) => ({
        value: `${sdt.getFullYear() + i}`,
        label: `${sdt.getFullYear() + i}`
    })));
    const [excludedDays, setExcludedDays] = useState([
        {start: subDays(new Date(), 365), end: subDays(new Date(), 1)},
        {
            start: new Date(`${Number(yearOptions[yearOptions.length - 1].value)}-12-31`),
            end: addDays(new Date(`${Number(yearOptions[yearOptions.length - 1].value)}-12-31`), 365)
        },
    ]);

    const yearHandleSelect = (val) => {
        setYearValue(val.getFullYear());
    }
    const monthHandleSelect = (val) => {
        setMonthValue(val.toLocaleString('en-us', {month: 'long'}));
        yearHandleSelect(val);
    }

    useEffect(() => {
        const calendarWidth = document.getElementById('datePicker').offsetWidth;
        setCalWidth(calendarWidth);
        const popperWidth = document.getElementsByClassName("calendar_popper")[0];
        if (popperWidth) {
            const popperDatePicker = popperWidth.getElementsByClassName('react-datepicker')[0];
            const popperDaysDatePicker = popperWidth.getElementsByClassName('react-datepicker__day');
            popperDatePicker.style.width = calendarWidth + 'px';
            for (let i = 0; i < popperDaysDatePicker.length; i++) {
                popperDaysDatePicker[i].style.height = popperDaysDatePicker[i].offsetWidth + 'px';
            }
        }
    }, [calendarOpen, winWidth, monthValue, yearValue]);

    useEffect(() => {
        const hasWin = typeof window !== 'undefined';

        if (hasWin) {
            function handleResize() {
                setWinWidth(window.innerWidth);
            }

            setWinWidth(window.innerWidth);

            window.addEventListener('resize', handleResize);
            return () => window.removeEventListener('resize', handleResize);
        }
    }, []);
    return (
        <DatePicker
            id='datePicker'
            renderCustomHeader={({
                                     date,
                                     decreaseMonth,
                                     increaseMonth, decreaseYear, increaseYear, changeMonth, changeYear
                                 }) => customHeader(t, startDate, endDate, monthOptions, yearOptions, monthHandleSelect, yearHandleSelect, monthValue, yearValue, decreaseMonth, increaseMonth, changeMonth, changeYear, calWidth)}
            showIcon
            toggleCalendarOnIconClick
            selected={startDate}
            startDate={startDate}
            endDate={endDate}
            onChange={(update) => {
                setDateRange(update);
            }}
            onCalendarOpen={() => setCalendarOpen(true)}
            onCalendarClose={() => setCalendarOpen(false)}
            className='more_filters_calendar'
            popperClassName="calendar_popper"
            dateFormat="MMMM d yyyy"
            selectsRange={true}
            shouldCloseOnSelect={false}
            showPopperArrow={false}
            excludeDateIntervals={excludedDays}
            onMonthChange={(date) => monthHandleSelect(date)}
            onYearChange={(date) => yearHandleSelect(date)}
            icon={
                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="22" viewBox="0 0 20 22">
                    <path id="Path_329" data-name="Path 329"
                          d="M12,19a1,1,0,1,0-1-1A1,1,0,0,0,12,19Zm5,0a1,1,0,1,0-1-1A1,1,0,0,0,17,19Zm0-4a1,1,0,1,0-1-1A1,1,0,0,0,17,15Zm-5,0a1,1,0,1,0-1-1A1,1,0,0,0,12,15ZM19,3H18V2a1,1,0,0,0-2,0V3H8V2A1,1,0,0,0,6,2V3H5A3,3,0,0,0,2,6V20a3,3,0,0,0,3,3H19a3,3,0,0,0,3-3V6A3,3,0,0,0,19,3Zm1,17a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V11H20ZM20,9H4V6A1,1,0,0,1,5,5H6V6A1,1,0,0,0,8,6V5h8V6a1,1,0,0,0,2,0V5h1a1,1,0,0,1,1,1ZM7,15a1,1,0,1,0-1-1A1,1,0,0,0,7,15Zm0,4a1,1,0,1,0-1-1A1,1,0,0,0,7,19Z"
                          transform="translate(-2 -1)" fill="#518cb7"/>
                </svg>
            }
        />
    );
}
