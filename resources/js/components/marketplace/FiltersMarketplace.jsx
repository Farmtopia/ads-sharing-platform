import React from "react";
import MoreFiltersComponent from "./MoreFiltersComponent.jsx";
import CommonFiltersComponent from "./CommonFiltersComponent.jsx";

export default function FiltersMarketplace({listingData}) {
    return (
        <div className='marketplace_filters_container'>
            <MoreFiltersComponent listingData={listingData}/>
            <CommonFiltersComponent listingData={listingData}/>
        </div>
    );
}
