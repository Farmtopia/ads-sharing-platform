import React, {useState, useEffect} from "react";
import "../../../public/css/LogInView.css";
import passEye from "../../../public/img/pass_eye.png";
import passEyeUndo from "../../../public/img/pass_eye_undo.png";
import {login} from "../APIs/Auth";
import ToastContainer from "../components/toasts/ToastContainer";
import { useTranslation } from 'react-i18next';

export default function LogInView({updateButtonSelect, csrf}) {
    const { t } = useTranslation(); // Initialize translation hook
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errorMessages, setErrorMessages] = useState([]);
    const [passType, setPassType] = useState('password');
    const eyeStyle = {
        height: passType === 'password' ? '0.875rem' : '1.25rem',
        top: passType === 'password' ? '1.094rem' : '0.9rem'
    };

    const eyeClick = (e) => {
        e.preventDefault();
        setPassType(passType === 'password' ? 'text' : 'password');
    };

    useEffect(() => {
        // Check for errors passed from the backend
        if (window.Settings.errors && window.Settings.errors.length > 0) {
            setErrorMessages(window.Settings.errors);
        }
    }, []);
    
    return (
        <div>
            <form className='login_container' method = 'POST' action = '/login'>
            <input type="hidden" name="_token" value={csrf} />
                <label className='login_label font-medium' htmlFor='email'>{t('translations.email')}</label>
                <input
                    className="login_email_input inter-regular-16 mb-3"
                    type="email"
                    name="email"
                    id='email'
                    placeholder={t('translations.emailplaceholder')}
                    autoFocus
                    required
                    autoComplete="new-password"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <label className='login_label font-medium' htmlFor='password'>{t('translations.enterpassword')}</label>
                <div className='login_pass_eye_container mb-3'>
                    <img className='login_pass_eye' src={passType === 'password' ? passEye : passEyeUndo}
                         onClick={(e) => eyeClick(e)} style={eyeStyle}/>
                    <input
                        className="login_password_input inter-regular-16"
                        type={passType}
                        placeholder={t('translations.enterpasswordplaceholder')}
                        id='password'
                        name='password'
                        required
                        autoComplete="new-password"
                        value={password}
                        onChange={(e) => setPassword(e.target.value)}
                    />
                </div>
                {/* {errorMessage && <p className="error_message">{errorMessage}</p>} */}
                <label className='login_text text-14px'>{t('translations.forgotpassword')} <span
                    className='text-14px' onClick={(e) => updateButtonSelect(e, 'resetPassword')}>{t('translations.here')}</span></label>
                <button type="submit" className="login_button font-medium text-23px">
                    {t('translations.login')}
                </button>
            </form>
            <label className='login_text text-center text-14px'>{t('translations.noaccount')}&nbsp;
                <span className='text-14px' onClick={(e) => updateButtonSelect(e, 'signup')}>{t('translations.register')}</span>
            </label>
            <ToastContainer
                errorMessages={errorMessages}
                setErrorMessages={setErrorMessages}
            />
        </div>
    );
}
