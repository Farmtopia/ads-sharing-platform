import React, { useState } from "react";
import { useTranslation } from 'react-i18next';
import "../../../public/css/LogInView.css";
import { sendPasswordResetEmail } from "../APIs/Auth.jsx";
import passEye from "../../../public/img/pass_eye.png";
import passEyeUndo from "../../../public/img/pass_eye_undo.png";
import ToastContainer from "../components/toasts/ToastContainer";

export default function ResetPasswordView({ updateButtonSelect }) {
    const { t } = useTranslation(); // Initialize translation hook
    const [email, setEmail] = useState('');
    const [successMessages, setSuccessMessages] = useState([]);
    const [errorMessages, setErrorMessages] = useState([]);


    // const goBack = () => {
    //     navigate('/login');
    // };

    const handleEmailChange = (event) => {
        setEmail(event.target.value); 
    };

    const handlePasswordReset = (e) => {
        e.preventDefault();
        if (email) {
            sendPasswordResetEmail(email)
                .then(() => {
                    updateButtonSelect(e, 'login');
                })
                .catch((error) => {
                    setErrorMessages((prevMessages) => [
                        ...prevMessages,
                        "Email sending failed. Please try again."
                    ]);
                });
        } else {
            setErrorMessages((prevMessages) => [
                ...prevMessages,
                "Please enter a valid email."
            ]);
        }
    };

    return (
        <div>
            <div className='password_recovery_welcome_txt mt-3'>
                <p className='welcome_txt_1 text-26px font-ex-bold'>{t('translations.passwordrecovery')}</p>
                <p className='welcome_txt_2 text-18px'>{t('translations.passwordrecoverytext')}</p>
            </div>
            <div className='password_recovery_container'>
                    <label className='login_label font-medium'>{t('translations.email')}</label>
                    <input className='login_email_input mb-3' type='email'
                           placeholder={t('translations.emailplaceholder')}
                           autoFocus
                           required
                           onChange={handleEmailChange} 
                           autoComplete='new-password'/>
                    <div className='password_recovery_not_recieve' dangerouslySetInnerHTML={{ __html: t('translations.noemailinfotext') }}/>
                    <button
                        onClick={handlePasswordReset}
                        className="login_button font-medium text-23px"
                    >
                        {t('translations.sendInstructions')}
                    </button>
                </div>
            <ToastContainer
                errorMessages={errorMessages}
                setErrorMessages={setErrorMessages}
            />
        </div>
    );
}