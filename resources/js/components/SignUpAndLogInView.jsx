import React, { useEffect, useState } from "react";
// import { useLocation } from "react-router-dom";
import "../../../public/css/SignUpAndLogInView.css";
import SignUpView from "./SignUpView.jsx";
import LogInView from "./LogInView.jsx";
import ResetPasswordView from "./ResetPasswordView.jsx";
import ToastContainer from '../components/toasts/ToastContainer.jsx';
import { useTranslation } from 'react-i18next';

export default function SignUpAndLogInView({ action, csrf }) {
    const { t } = useTranslation(); // Initialize translation hook
    // const location = useLocation();
    const [buttonSelect, setButtonSelect] = useState(action === 'signup' ? 'signup' : 'login');
    const [successMessages, setSuccessMessages] = useState([]);
    const [infoMessages, setInfoMessages] = useState([]);
    const [errorMessages, setErrorMessages] = useState([]);
    const [registerResp, setRegisterResp] = useState();

    const updateButtonSelect = (e, newVal) => {
        e.preventDefault();
        setButtonSelect(newVal);
    };

    useEffect(() => {
        if (registerResp) {
            console.log(registerResp);
            console.log(registerResp.status);
            if (registerResp.success) {
                setSuccessMessages(["Your email was successfully registered."]);
                setButtonSelect('login');
            } else {
                setErrorMessages((prev) => ([...prev, registerResp.message]));
            }
        }
    }, [registerResp]);

    return (
        <div className='signuplogin_container'>
            <div className='signuplogin_form_container'>
                <div className='signuplogin_buttons_container'>
                    <button
                        className={`text-23px font-medium ${buttonSelect === 'signup' ? 'signuplogin_button_enabled' : 'signuplogin_button_disabled'}`}
                        onClick={(e) => updateButtonSelect(e, 'signup')}>{t('translations.signup')}
                    </button>
                    <button
                        className={`text-23px font-medium ${buttonSelect === 'login' ? 'signuplogin_button_enabled' : 'signuplogin_button_disabled'}`}
                        onClick={(e) => updateButtonSelect(e, 'login')}>{t('translations.login')}
                    </button>
                </div>
                <div className='signup_or_login_container'>
                    {buttonSelect === 'signup' && <SignUpView updateButtonSelect={updateButtonSelect} setResp={setRegisterResp} />}
                    {buttonSelect === 'login' && <LogInView updateButtonSelect={updateButtonSelect} csrf={csrf} />}
                    {buttonSelect === 'resetPassword' && <ResetPasswordView updateButtonSelect={updateButtonSelect} />}
                </div>
                {/* <div className='signuplogin_terms text-14px'>Με την είσοδό σας, συμφωνείτε με τους 
                    <a href="https://google.com" target='_blank' className='text-14px'>Όρους Χρήσης</a> και την 
                    <a href="https://google.com" target='_blank' className='text-14px'>Πολιτική Απορρήτου</a>.
                </div> */}
            </div>
            <ToastContainer
                successMessages={successMessages}
                setSuccessMessages={setSuccessMessages}
                errorMessages={errorMessages}
                setErrorMessages={setErrorMessages}
                infoMessages={infoMessages}
                setInfoMessages={setInfoMessages}
            />
        </div>
    );
}