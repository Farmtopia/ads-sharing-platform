import React, {useState, useEffect, Fragment} from "react";
import "../../../../public/css/TechnicalCharacteristics.css";
import {useTranslation} from 'react-i18next';
import {getEquipmentSubCategories} from "../../APIs/Equipment.jsx";
import {getFormFields} from "../../APIs/FormFields.jsx";
import ReactSelect2Component from "../ReactSelect2Component.jsx";

export default function TechnicalCharacteristics({equipmentType, equipmentCategories, formData, setFormData}) {
    const {t} = useTranslation();
    const [equipmentCategory, setEquipmentCategory] = useState(null);
    const [equipmentSubCategory, setEquipmentSubCategory] = useState(null);
    const [equipmentSubCategories, setEquipmentSubCategories] = useState(null);
    const [fields, setFields] = useState(null);
    const [collections, setCollections] = useState(null);
    const [itemDescription, setItemDescription] = useState('');
    const [specificModel, setSpecificModel] = useState('');
    const formDataFields = ['power', 'displacement', 'fuel_type', 'number_of_cylinders', 'number_of_furrows', 'weight', 'working_width', 'power_supply'];
    // console.log(formData);

    const handleSelectEquipmentCategory = (selection) => {
        setEquipmentSubCategory(null);
        let newObj = {...formData};
        newObj['equipment_subcategory_id'] = null;
        for (let f of formDataFields) {
            newObj[f] = null;
        }
        setFormData(newObj);
        setEquipmentCategory(selection);
    }

    const handleSelectEquipmentSubCategory = (selection) => {
        setEquipmentSubCategory(selection);
        setFormData((formData) => ({
            ...formData,
            equipment_subcategory_id: String(selection.value),
        }));
    }

    const handleSelectTechnicalCharacteristics = (selection, name) => {
        setFormData((formData) => ({
            ...formData,
            [name]: selection.label
        }));
    }

    const handleInputTechnicalCharacteristics = (value, name) => {
        setFormData((formData) => ({
            ...formData,
            [name]: value
        }));
    }

    const handleItemDescription = (value) => {
        setItemDescription(value);
        setFormData((formData) => ({
                ...formData, description: value
            }
        ));
    }

    const handleSpecificModel = (value) => {
        setSpecificModel(value);
        setFormData((formData) => ({
                ...formData, equipment_model_name: value
            }
        ));
    }

    const formSubmit = (e) => {
        e.preventDefault();
    }

    const fetchFormFields = async (id) => {
        getFormFields(id).then(data => {
            if (Object.keys(data).length > 0 && data.success) {
                setFields(data.fields);
                setCollections(data.collections);
            }
        }).catch(err => console.log(err));
    }

    const fetchEquipmentSubCategories = async (id) => {
        getEquipmentSubCategories(id).then(data => {
            if (Object.keys(data).length > 0 && data.success) {
                setEquipmentSubCategories(data.types);
            }
        }).catch(err => console.log(err));
    }

    useEffect(() => {
        if (equipmentSubCategory) {
            fetchFormFields(Number(equipmentSubCategory.value));
        }
    }, [equipmentSubCategory]);

    useEffect(() => {
        if (equipmentCategory) {
            fetchEquipmentSubCategories(Number(equipmentCategory.value));
        }
    }, [equipmentCategory]);

    return (
        <form onSubmit={formSubmit} className='technical_char_container'>
            <div className='technical_char_column_container'>
                <div
                    className='technical_char_column_label font-semibold text-16px text-blue'>{t("translations.itemdescription")}</div>
                <textarea
                    className='technical_char_column_label_description form-control text-16px font-normal text-light-grey'
                    name="description"
                    rows={6}
                    defaultValue={itemDescription} onChange={(e) => handleItemDescription(e.target.value)}/>
                <div
                    className='technical_char_column_label font-semibold text-16px text-blue mt-3'>{t("translations.categoryofequipment")}</div>
                <ReactSelect2Component
                    options={equipmentCategories}
                    placeholder={equipmentCategory && equipmentCategory.label}
                    value={equipmentCategory && equipmentCategory.label}
                    width={null}
                    handleSelect={handleSelectEquipmentCategory}/>
                {equipmentCategory && (
                    <>
                        <div
                            className='technical_char_column_label font-semibold text-16px text-blue mt-3'>{t("translations.subcategoryofequipment")}</div>
                        <ReactSelect2Component
                            options={equipmentSubCategories}
                            placeholder={equipmentSubCategory && equipmentSubCategory.label}
                            value={equipmentSubCategory && equipmentSubCategory.label}
                            width={null}
                            handleSelect={handleSelectEquipmentSubCategory}/>
                        <div
                            className='technical_char_column_label font-semibold text-16px text-blue mt-3'>{t("translations.specificModel")}</div>
                        <input type="text" name='specificModel' value={specificModel}
                               className="form-control text-16px font-normal text-light-grey"
                               onChange={(e) => handleSpecificModel(e.target.value)}
                        />
                    </>
                )}
            </div>
            <div className='technical_char_column_container'>
                {equipmentSubCategory && fields?.map((field, f) => {
                    return (
                        <Fragment key={`TechCharFields-${f}`}>
                            <div
                                className={`technical_char_column_label_char font-medium text-16px ${f !== 0 && 'mt-3'}`}>{field.label} {field.unit && `(${field.unit})`}</div>
                            {field.type === 'select' ? (
                                <ReactSelect2Component
                                    options={collections[field.options]}
                                    placeholder={formData.name && formData.name}
                                    value={formData.name && formData.name}
                                    width={null}
                                    handleSelect={(selection) => handleSelectTechnicalCharacteristics(selection, field.name)}/>
                            ) : (
                                <input type="text" name={field.name}
                                       className="form-control text-16px font-normal text-light-grey"
                                       onChange={(e) => handleInputTechnicalCharacteristics(e.target.value, field.name)}/>
                            )}
                        </Fragment>
                    );
                })}
            </div>
        </form>
    );
}
