import React from "react";

export default function BlockSelectedDays({t, startDate, endDate}) {
    const formatter = new Intl.DateTimeFormat("en-US", {
        year: "numeric",
        month: "long",
        day: "numeric",
    });
    return (
        <div className='block_selected_days_container'>
            <div
                className='text-16px font-semibold text-default mb-2'>{t('translations.blockselecteddates')}</div>
            <div className='text-14px font-normal text-grey-brown'>{t('translations.from')}</div>
            <div
                className='block_selected_days_input text-16px font-normal mb-2'>{formatter.format(startDate)}</div>
            <div className='text-14px font-normal text-grey-brown'>{t('translations.to')}</div>
            <div
                className='block_selected_days_input text-16px font-normal'>{formatter.format(endDate)}</div>
            <div className='block_selected_button font-medium text-16px'>{t('translations.block')}</div>
        </div>
    );
}
