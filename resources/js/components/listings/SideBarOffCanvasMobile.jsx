import React, {useState, useEffect} from "react";
import Offcanvas from 'react-bootstrap/Offcanvas';
import { useTranslation } from 'react-i18next';

export default function SideBarOffCanvasMobile({
    sideCanvasShow,
    sideOffCanvasClose,
    updateShowSidebar,
    activeView,
    setActiveView
}) {
    const { t } = useTranslation(); // Initialize translation hook

    const handleMenuClick = (view) => {
        setActiveView(view);
    };

    const updateSidebars = () => {
        updateShowSidebar(true);
        sideOffCanvasClose();
    }

    return (
        <Offcanvas show={sideCanvasShow} onHide={() => updateSidebars()} placement={'start'} name='infoIconOffCanvas'
                   id='side_offcanvas'
                   scroll='true'
        >
            <Offcanvas.Body>
                <div>
                    <button className={`custon-mt text-18px font-semibold navbutton ${activeView === "techicalchar" ? "active" : ""}`} onClick={() => handleMenuClick("techicalchar")}>
                        {t("translations.technicalCharacteristics")}
                    </button>
                    <hr/>
                    <button className={`text-18px font-semibold navbutton ${activeView === "photos" ? "active" : ""}`} onClick={() => handleMenuClick("photos")}>
                        {t("translations.photos")}
                    </button>
                    <hr/>
                    <button className={`text-18px font-semibold navbutton ${activeView === "pricing" ? "active" : ""}`} onClick={() => handleMenuClick("pricing")}>
                        {t("translations.pricing")}
                    </button>
                    <hr/>
                    <button className={`text-18px font-semibold navbutton ${activeView === "drywetlease" ? "active" : ""}`} onClick={() => handleMenuClick("drywetlease")}>
                        {t("translations.drywetlease")}
                    </button>
                    <hr/>
                    <button className={`text-18px font-semibold navbutton ${activeView === "availability" ? "active" : ""}`} onClick={() => handleMenuClick("availability")}>
                        {t("translations.availability2")}
                    </button>
                    <hr/>
                    <button className={`text-18px font-semibold navbutton ${activeView === "deliveryoptions" ? "active" : ""}`} onClick={() => handleMenuClick("deliveryoptions")}>
                        {t("translations.deliveryoptions")}
                    </button>
                    <hr/>
                    <button className={`text-18px font-semibold navbutton ${activeView === "supportmaterials" ? "active" : ""}`} onClick={() => handleMenuClick("supportmaterials")}>
                        {t("translations.supportmaterials2")}
                    </button>
                    <hr/>
                    <button className={`text-18px font-semibold navbutton ${activeView === "rentingprerequisites" ? "active" : ""}`} onClick={() => handleMenuClick("rentingprerequisites")}>
                        {t("translations.rentingprerequisites")}
                    </button>
                    <hr className="custom-mb"/>
                    <div className="w-100 custom-mb text-center">
                        <button className={`text-18px font-semibold text-white exit-btn`} onClick={() => window.location.href = '/profile'}>
                            {t("translations.exit")}
                        </button>
                    </div>
                    
                </div>
            </Offcanvas.Body>
        </Offcanvas>
    );
}
