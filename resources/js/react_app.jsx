import ReactDOM from 'react-dom/client';
import SignUpAndLogInView from './components/SignUpAndLogInView';
import ListingView from './views/ListingView';
import MarketplaceView from './components/MarketplaceView';
import ResetPassword from './components/ResetPassword';
import NewListing from './views/NewListingContainer';
import Profile from './views/Profile';
import MemberSignupView from './views/MemberSignupView';
import './i18n';


const signuplogin = document.getElementById('signuplogin');
const listingview = document.getElementById('listingview');
const marketplace = document.getElementById('marketplace');
const resetpassword = document.getElementById('resetpassword');
const profile = document.getElementById('myprofile');
const newlisting = document.getElementById('newlisting');
const membersignupview = document.getElementById('membersignup');

const authToken = sessionStorage.getItem('authToken');

if (signuplogin) {
    const action = window.action; // Read the action variable from the global window object
    ReactDOM.createRoot(signuplogin).render(
        <>
            <SignUpAndLogInView action={action} csrf={window.Settings.csrf}/>
        </>
    );
}

if (membersignupview) {
    ReactDOM.createRoot(membersignupview).render(
        <>
            <MemberSignupView/>
        </>
    );
}

if (resetpassword) {
    ReactDOM.createRoot(resetpassword).render(
        <>
            <ResetPassword />
        </>
    );
}

if (listingview) {
    const listingId = listingview.getAttribute('data-listing-id');
    ReactDOM.createRoot(listingview).render(
        <>
            <ListingView listingId={listingId} />
        </>
    );
}

if (marketplace) {
    ReactDOM.createRoot(marketplace).render(
        <>
            <MarketplaceView csrf={window.Settings.csrf}/>
        </>
    );
}

if (profile) {
    ReactDOM.createRoot(profile).render(
        <>
            <Profile csrf={window.Settings.csrf} authToken={authToken} />
        </>
    );
}

if (newlisting) {
    ReactDOM.createRoot(newlisting).render(
        <>
            <NewListing csrf={window.Settings.csrf} authToken={authToken} />
        </>
    );
}
