export const allCountries = async () => {
    const response = await fetch(`/api/countries`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    });
    const data = await response.json();
    return data;
}

export const getCountryById = async (id) => {
    const response = await fetch(`/api/countries/${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    });
    const data = await response.json();
    return data;
}
