export const getUsersOrganization = async (token) => {
    const response = await fetch(`/api/organizations/organizations/organization/by-member`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    });
    const data = await response.json();
    return data;
}

export const updateOrganizationProfile = async (token, id, body) => {
    const response = await fetch(`/api/organizations/organizations/${id}/update`, {
        method: 'POST',
        headers: {
            "Accept": "application/json",
            'Authorization': `Bearer ${token}`,
        },
        body,
    });
    const data = await response.json();
    return data;
}
