export const getEquipmentTypes = async () => {
    const response = await fetch(`/api/equipment/types`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
        },
    });
    const data = await response.json();
    return data;
}

export const getEquipmentCategories = async (id) => {
    const response = await fetch(`/api/equipment/categories/type/${id}`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
        },
    });
    const data = await response.json();
    return data;
}

export const getEquipmentSubCategories = async (id) => {
    const response = await fetch(`/api/equipment/subcategories/category/${id}`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
        },
    });
    const data = await response.json();
    return data;
}
