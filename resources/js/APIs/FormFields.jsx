export const getFormFields = async (id) => {
    const response = await fetch(`/api/form_fields/${id}`, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
        },
    });
    const data = await response.json();
    return data;
}
