export const register = async (body) => {
    const response = await fetch(`/api/auth/register`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    });
    const data = await response.json();
    return data;
}

export const registermember = async (token, body) => {
    const response = await fetch(`/api/auth/register/organization/member/${token}`, {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(body),
    });
    const data = await response.json();
    return data;
}

export const login = async (body) => {
    const response = await fetch(`/api/auth/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
            body
        ),
    });
    const data = await response.json();

    return data;
}


export const logout = async (token) => {
    const response = await fetch(`/api/auth/logout`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`,
        },
    });
    const data = await response.json();
    return data;
}

export const sendVerificationEmail = async (id) => {
    const response = await fetch(`/api/auth/verify/${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
        },
    });
    const data = await response.json();
    return data;
}

export const verifyEmail = async (hash, id) => {
    const response = await fetch(`/api/auth/verify`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
            {
                "hash": hash,
                "id": id
            }
        ),
    });
    const data = await response.json();
    console.log(data)
    return data;
}

export const sendPasswordResetEmail = async (email) => {
    const response = await fetch(`/api/auth/forgot-password`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
            {
                "email": email
            }
        ),
    });
    const data = await response.json();
    console.log(data)
    return data;
}

export const resetPassword = async (token, email, email_encrypted, password, password_confirmation) => {
    const response = await fetch(`/api/auth/reset-password`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
            {
                "token": token,
                "email_encrypted": email_encrypted,
                "email": email,
                "password": password,
                "password_confirmation": password_confirmation
            }
        ),
    });
    const data = await response.json();
    return data;
}

export const resetPasswordVerify = async (token, email_encrypted) => {
    const response = await fetch(`/api/auth/reset-password/verify`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(
            {
                "token": token,
                "email_encrypted": email_encrypted
            }
        ),
    });
    const data = await response.json();
    return data;
}

export const inviteOrganizationMember = async (token, email) => {
    const response = await fetch(`/api/auth/invite/user`, {
        method: 'POST',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Authorization': `Bearer ${token}`,
        },
        body: JSON.stringify(
            {
                "email": email,
            }
        ),
    });
    const data = await response.json();
    return data;
}
