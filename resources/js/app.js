import './bootstrap';
import './i18n';


import Alpine from 'alpinejs';

window.Alpine = Alpine;

Alpine.start();
