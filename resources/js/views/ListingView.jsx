import { React, useRef, useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import "../../../public/css/ListingView.css";
import RentalCard from "../components/RentalCard";
import { listingsById, getUserListings } from "../APIs/Listings.jsx";
import { getEquipmentSubCategories } from "../APIs/Equipment.jsx"; 
import ListingMap from "../components/listings/ListingMap.jsx";
import MarketPlaceCalendar from "../components/marketplace/MarketPlaceCalendar.jsx";

const ListingView = ({ listingId }) => {
    const token = sessionStorage.getItem('authToken');
    const { t } = useTranslation();
    const scrollContainerRef = useRef(null);

    const [listing, setListing] = useState(null);
    const [subcategory, setSubcategory] = useState(null); // State to store the subcategory name
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [mainImage, setMainImage] = useState("/img/profile-placeholder.webp");
    const [thumbnails, setThumbnails] = useState([]);
    const [cardWidth, setCardWidth] = useState(0);
    const [canScrollLeft, setCanScrollLeft] = useState(false);
    const [canScrollRight, setCanScrollRight] = useState(true);
    const [countries, setCountries] = useState([]);
    const [countryName, setCountryName] = useState("");
    const [listingData, setListingData] = useState([]);

    const fetchUserListings = async (userId) => {
        try {
            const data = await getUserListings(token, userId);
            if (data.success) {
                setListingData(data.listings);
            } else {
                console.error("Failed to fetch user listings");
            }
        } catch (error) {
            console.error("Error fetching user listings:", error);
        }
    };

    const fetchCountries = async () => {
        try {
            const response = await fetch('/api/countries', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                },
            });
            const data = await response.json();
            setCountries(data.countries);
        } catch (error) {
            console.error("Error fetching countries:", error);
        }
    };

    useEffect(() => {
        fetchCountries();
    }, []);

    useEffect(() => {
        listingsById(token, listingId)
            .then((data) => {
                if (data.success) {
                    setListing(data.listing);
                    if (data.listing.media.length > 0) {
                        const mediaUrls = data.listing.media.map(filename => `/storage/material/${filename}`);
                        setMainImage(mediaUrls[0]);
                        setThumbnails(mediaUrls.slice(1));
                    } else {
                        setThumbnails(["/img/profile-placeholder.webp"]);
                        setMainImage("/img/profile-placeholder.webp");
                    }
                    // Fetch the subcategory name
                    getEquipmentSubCategories(data.listing.equipment_category_id)
                        .then(subcategoryData => {
                            if (subcategoryData.success) {
                                const subcategory = subcategoryData.types.find(type => type.value === data.listing.equipment_subcategory_id);
                                if (subcategory) {
                                    setSubcategory(subcategory.label);
                                } else {
                                    setSubcategory("Unknown Subcategory");
                                }
                            } else {
                                setSubcategory("Unknown Subcategory");
                            }
                        })
                        .catch(() => setSubcategory("Unknown Subcategory"));
                    
                    // Find the country name
                    const country = countries.find(country => country.id === data.listing.user.country_id);
                    if (country) {
                        setCountryName(country.name);
                    } else {
                        setCountryName("Unknown Country");
                    }
    
                    // Fetch user listings
                    fetchUserListings(data.listing.user.id);
    
                    console.log(data.listing);
                } else {
                    setError("Failed to fetch listing data");
                }
            })
            .catch(() => setError("Error retrieving listing"))
            .finally(() => setLoading(false));
    }, [token, listingId, countries]);

    useEffect(() => {
        const updateCardWidth = () => {
            if (scrollContainerRef.current) {
                setCardWidth(scrollContainerRef.current.clientWidth / 3); // 3 cards visible at a time
            }
        };

        updateCardWidth();
        window.addEventListener("resize", updateCardWidth);
        return () => window.removeEventListener("resize", updateCardWidth);
    }, []);

    const checkScroll = () => {
        if (scrollContainerRef.current) {
            setCanScrollLeft(scrollContainerRef.current.scrollLeft > 0);
            setCanScrollRight(
                scrollContainerRef.current.scrollLeft + scrollContainerRef.current.clientWidth <
                scrollContainerRef.current.scrollWidth
            );
        }
    };

    const scroll = (direction) => {
        if (scrollContainerRef.current) {
            scrollContainerRef.current.scrollBy({ left: direction * cardWidth, behavior: "smooth" });
            setTimeout(checkScroll, 300);
        }
    };

    // Show loading or error message
    if (loading) return <p>Loading listing details...</p>;
    if (error) return <p className="text-danger">{error}</p>;

    const technicalCharacteristics = listing.technical_characteristics;

    return (
        <div className="listing-container p-5">
            <a href="#" className="text-center text-blue text-decoration-none pe-auto"
                onClick={() => window.location.href = `/marketplace`}>
                <i className="bi bi-arrow-left text-blue me-2 text-24px align-items-center"></i>
                {t('translations.backToSearch')}
            </a>
            <h3 className="font-bold mt-5">{listing.manufacturer} {subcategory} {listing.equipment_model_name}</h3>

            {/* Gallery */}
            <div className="gallery-container">
                <div className="gallery-grid">
                    <div className="main-image-container">
                        <img
                            className="main-image rounded"
                            src={mainImage}
                            alt="Main display"
                        />
                    </div>

                    <div className="thumbnails-container">
                        {thumbnails.map((thumb, index) => (
                            <div
                                key={index}
                                className="thumbnail"
                                onClick={() => setMainImage(thumb)}
                            >
                                <img
                                    className={`thumbnail rounded ${mainImage === thumb ? "active-thumbnail" : ""}`}
                                    src={thumb}
                                    alt={`Thumbnail ${index + 1}`}
                                />
                            </div>
                        ))}
                    </div>
                </div>
            </div>

            {/* Content Grid */}
            <div className="row">
                {/* Left Column: Tractor Details */}
                <div className="col-lg-8">
                    <div className="my-4">
                        <h3 className="font-bold">{subcategory} {listing.manufacturer} {listing.equipment_model_name}</h3>
                        <h5 className="font-light text-muted">{listing.user.region}, {countryName}</h5>
                        <p className="text-18px">{listing.listing_description}
                        </p>
                    </div>
                    <h4 className="text-blue font-bold">{t('translations.techSpecs')}
                    </h4>
                    <table className="listing-table">
                        <tbody>
                            {technicalCharacteristics.power && (
                                <tr>
                                    <th>{t('translations.enginePowerHP')}</th>
                                    <td>{technicalCharacteristics.power}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.displacement && (
                                <tr>
                                    <th>{t('translations.displacement')}</th>
                                    <td>{technicalCharacteristics.displacement}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.number_of_cylinders && (
                                <tr>
                                    <th>{t('translations.noOfCylinders')}</th>
                                    <td>{technicalCharacteristics.number_of_cylinders}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.fuel_type && (
                                <tr>
                                    <th>{t('translations.fuelType')}</th>
                                    <td>{technicalCharacteristics.fuel_type.label}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.fuel_consumption && (
                                <tr>
                                    <th>{t('translations.fuelConsumption')}</th>
                                    <td>{technicalCharacteristics.fuel_consumption}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.drivetrain && (
                                <tr>
                                    <th>{t('translations.drivetrain')}</th>
                                    <td>{technicalCharacteristics.drivetrain}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.weight && (
                                <tr>
                                    <th>{t('translations.weight')}</th>
                                    <td>{technicalCharacteristics.weight}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.number_of_furrows && (
                                <tr>
                                    <th>{t('translations.numoffurrows')}</th>
                                    <td>{technicalCharacteristics.number_of_furrows}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.working_width && (
                                <tr>
                                    <th>{t('translations.workingwidth')}</th>
                                    <td>{technicalCharacteristics.working_width}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.accuracy && (
                                <tr>
                                    <th>{t('translations.accuracy')}</th>
                                    <td>{technicalCharacteristics.accuracy}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.author && (
                                <tr>
                                    <th>{t('translations.author')}</th>
                                    <td>{technicalCharacteristics.author}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.correction_services && (
                                <tr>
                                    <th>{t('translations.correctionservices')}</th>
                                    <td>{technicalCharacteristics.correction_services}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.description && (
                                <tr>
                                    <th>{t('translations.description')}</th>
                                    <td>{technicalCharacteristics.description}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.external_data_source && (
                                <tr>
                                    <th>{t('translations.externaldatasource')}</th>
                                    <td>{technicalCharacteristics.external_data_source}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.flight_time && (
                                <tr>
                                    <th>{t('translations.flighttime')}</th>
                                    <td>{technicalCharacteristics.flight_time}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.is_fixed_or_mounted && (
                                <tr>
                                    <th>{t('translations.fixedormounted')}</th>
                                    <td>{technicalCharacteristics.is_fixed_or_mounted}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.license && (
                                <tr>
                                    <th>{t('translations.license')}</th>
                                    <td>{technicalCharacteristics.license}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.max_altitude && (
                                <tr>
                                    <th>{t('translations.maxaltitude')}</th>
                                    <td>{technicalCharacteristics.max_altitude}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.max_range && (
                                <tr>
                                    <th>{t('translations.maxrange')}</th>
                                    <td>{technicalCharacteristics.max_range}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.measurement_parameters && (
                                <tr>
                                    <th>{t('translations.measurementarameters')}</th>
                                    <td>{technicalCharacteristics.measurement_parameters}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.power_supply && (
                                <tr>
                                    <th>{t('translations.powersupply')}</th>
                                    <td>{technicalCharacteristics.power_supply.label}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.publication_date && (
                                <tr>
                                    <th>{t('translations.publicationdate')}</th>
                                    <td>{technicalCharacteristics.publication_date}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.sensors_accompanied && (
                                <tr>
                                    <th>{t('translations.sensorsaccompanied')}</th>
                                    <td>{technicalCharacteristics.sensors_accompanied}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.title && (
                                <tr>
                                    <th>{t('translations.title')}</th>
                                    <td>{technicalCharacteristics.title}</td>
                                </tr>
                            )}
                            {technicalCharacteristics.type && (
                                <tr>
                                    <th>{t('translations.type')}</th>
                                    <td>{technicalCharacteristics.type}</td>
                                </tr>
                            )}
                        </tbody>
                    </table>

                    {/* Offered Labor Section */}
                    {listing.has_labour && (
                        <div className="labor-offer mt-4">
                            <h4 className="text-blue font-bold">{t("translations.offeredLaborDisc")}</h4>
                            <div className="labor-card">
                                <p className="text-default font-bold text-18px">{t("translations.forthisitem")} {listing.user.firstname} {t("translations.offerslaborservices")}</p>
                                <h2 className="text-blue text-end">{Math.floor(listing?.labour_min_price)} - {Math.floor(listing?.labour_max_price)} {listing?.delivery_fee_unit} {t("translations.perday")}</h2>
                                <img src={listing.user?.profile_photo ? `/storage/profile_photos/${listing.user.profile_photo}` : "/img/profile-placeholder.webp"} alt="Owner" className="profile-pic rounded-circle me-3" />
                                <div className="d-flex align-items-center">
                                    <div>
                                        <h6 className="text-default font-medium">{t("translations.operatorsExperience")}</h6>
                                        <p className="text-16px font-normal text-default">{listing.labour_experience}</p>
                                        <h6 className="text-default font-medium">{t("translations.laborScope")}</h6>
                                        <p className="text-16px font-normal text-default">{listing.labour_scope}</p>
                                        <p className="text-16px font-normal text-default">{listing.labour_special_requirements}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )}

                    {/* Support Materials Section */}
                    {listing.support_materials && listing.support_materials.length > 0 && (
                        <div className="support-materials mt-4">
                            <h4 className="text-blue">{t("translations.supportMaterials")}</h4>
                            <div className="mt-3">
                                {listing.support_materials.map((material, index) => (
                                    <div key={index} className="pdf-item d-flex align-items-center justify-content-between">
                                        <div className="d-flex align-items-center">
                                            <i className="text-24px me-3 bi bi-filetype-pdf"></i>
                                            <span>{material}</span>
                                        </div>
                                        <a className="text-default text-decoration-none" href={`/storage/materials/${material}`} download>
                                            <i className="text-24px text-blue bi bi-download"></i>
                                        </a>
                                    </div>
                                ))}
                            </div>
                        </div>
                    )}

                    {/* Location and Delivery Section */}
                    <div className="location-delivery mt-4">
                        <h4 className="text-blue">{t("translations.locationAndDelivery")}</h4>
                        <div className="map-container mb-3">
                            <ListingMap position={{ lat: listing.lat, lon: listing.lng }} />
                        </div>
                        <div className="row delivery-option-container p-3">
                            {listing.has_pick_up_point && (
                                <div className="delivery-option col-lg-5">
                                    <h5 className="font-medium">{t("translations.pickUp")}</h5>
                                    <h6 className="font-medium">{t("translations.pickUpLoc")}</h6>
                                    <p>{listing.pick_up_address_number} {listing.pick_up_address_road} {listing.pick_up_region}</p>
                                    <h6 className="font-medium">{t("translations.availPickUpTimes")}</h6>
                                    <ul className="list-unstyled">
                                        <li>{listing.pickup_timeframe_description}</li>
                                    </ul>
                                </div>
                            )}
                            {listing.has_delivery_service && (
                                <div className="delivery-option col-lg-5">
                                    <h5 className="font-medium">{t("translations.delivery")}</h5>
                                    <h6 className="font-medium">{t("translations.deliveryService")}</h6>
                                    <p>{listing.has_delivery_service}</p>
                                    <h6 className="font-medium">{t("translations.deliveryCharge")}</h6>
                                    <p>{Math.floor(listing.delivery_fee)}{listing.delivery_fee_unit} {t("translations.fordeliverywithin")} {Math.floor(listing.max_delivery_distance)}{listing.max_delivery_distance_unit}</p>
                                    <p>{listing.delivery_timeframe_description}</p>
                                </div>
                            )}
                        </div>
                        <div className="renter-reqs mb-3">
                            <h6>{t("translations.renterReqs")}</h6>
                            <p> {listing.special_requirements}</p>
                        </div>
                        <div className="special-reqs">
                            <h6>{t("translations.specialReqs")}</h6>
                            <p> {listing.special_requirements}</p>
                        </div>
                    </div>

                    {/* Other items from User */}
                    {listingData.filter((lData) => lData.id !== listing.id).length > 0 && (
                        <div className="rental-gallery-wrapper">
                            <h4 className="text-blue">{t("translations.otherItemsFrom")} {listing.user.firstname}</h4>
                            {canScrollLeft && (
                                <button className="scroll-btn left" onClick={() => scroll(-1)}>
                                    <i className="text-white text-30px bi bi-caret-left-fill"></i>
                                </button>
                            )}

                            <div className="rental-card-scroll-container" ref={scrollContainerRef} onScroll={checkScroll}>
                                <div className="rental-card-track">
                                    {listingData
                                        .filter((lData) => lData.id !== listing.id)
                                        .map((lData) => (
                                            <RentalCard key={lData.id} cardId={lData.id} />
                                        ))}
                                </div>
                            </div>

                            {canScrollRight && (
                                <button className="scroll-btn right" onClick={() => scroll(1)}>
                                    <i className="text-white text-30px bi bi-caret-right-fill"></i>
                                </button>
                            )}
                        </div>
                    )}

                    
                </div>

                {/* Right Column: Sticky Price & Contact Info */}
                <div className="col-lg-4">
                    <div className="sticky-column">
                        <div className="card p-3">
                            <MarketPlaceCalendar/>
                            <h2 className=" my-4">{Math.floor(listing.min_price)} {listing.price_currency} to {Math.floor(listing.max_price)} {listing.price_currency} /{t("translations.perday")}</h2>
                            {listing.has_labour && (
                                <h6 className="font-light">{t("translations.forthisitem")} {listing.user.firstname} {t("translations.offerslaborservices")}</h6>
                            )}
                            {listing.has_labour && (
                                <p className="text-end">+ {Math.floor(listing?.labour_min_price)} - {Math.floor(listing?.labour_max_price)} {listing?.delivery_fee_unit} {t("translations.perday")}</p>
                            )}             
                            <label>
                                <input className="font-light" type="checkbox" /> {t("translations.includelabour")}
                            </label>
                            {/* Todo add send request */}
                            <button className="send-request-btn mt-3">{t("translations.sendRequest")}</button>
                        </div>

                        <div className="profile-card p-3 mt-3">

                            <p className="text-21px">
                                <img src={listing.user?.profile_photo ? `/storage/profile_photos/${listing.user.profile_photo}` : "/img/profile-placeholder.webp"} alt="Owner" className="profile-pic-sm rounded-circle me-3" />
                                {listing.user.firstname} {listing.user.lastname}</p>
                                <a className="d-flex align-items-center text-default"
                                    href={`mailto:${listing.user.email}`}>
                                    <i className="text-21px me-3 text-blue bi bi-envelope"></i>{listing.user.email}
                                </a>
                            <p className="d-flex align-items-center"><i
                                className="text-21px me-3 text-blue bi bi-geo-alt"></i>{listing.user.address_road} {listing.user.address_number} {listing.user.region}, {countryName}</p>
                            
                            {/* Not in MVP */}
                            {/* <div className="d-flex justify-content-end">
                                <button className="view-profile-btn-sm text-16px font-normal">{t("translations.viewProfile")}</button>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default ListingView;