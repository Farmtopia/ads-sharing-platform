import React, {useState, useEffect} from "react";
import "../../../public/css/MyProfile.css";
import {useTranslation} from 'react-i18next';
import ProfileDetails from "../components/profile/ProfileDetails";
import MyOrganizationDetails from "../components/profile/MyOrganizationDetails.jsx";
import MyListings from "../components/profile/MyListings.jsx";
import SidebarMenuMobile from "../components/profile/SidebarMenuMobile";
import SideBarOffCanvasMobile from "../components/profile/SideBarOffCanvasMobile";
import axios from "axios";
import {motion} from "framer-motion";
import {getUsersOrganization} from "../APIs/Organizations.jsx";
import {getCountryById} from "../APIs/Countries.jsx";
import { getUserListings } from "../APIs/Listings.jsx";

export default function Profile({csrf, authToken}) {
    const {t} = useTranslation(); // Initialize translation hook
    const [activeView, setActiveView] = useState("profile");
    const [userData, setUserData] = useState(null);
    const [organizationData, setOrganizationData] = useState(null);
    const [userAccessLevel, setUserAccessLevel] = useState(null);
    const [listingData, setListingData] = useState(null);
    const [countryById, setCountryById] = useState(null);
    const [countries, setCountries] = useState([]);
    const [showSidebar, setShowSidebar] = useState(true);
    const [sideCanvasShow, setSideCanvasShow] = useState(false);

    const fetchUserData = async () => {
        try {
            const response = await fetch('/api/users/authenticated/user', {
                method: 'GET',
                headers: {
                    'Authorization': `Bearer ${authToken}`,
                    'Accept': 'application/json',
                },
            });
            const data = await response.json();
            setUserData(data);
            console.log("User Data:", data);
        } catch (error) {
            console.error("Error fetching user data:", error);
        }
    };

    const fetchCountries = async () => {
        try {
            const response = await fetch('/api/countries', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                },
            });
            const data = await response.json();
            setCountries(data.countries);
        } catch (error) {
            console.error("Error fetching countries:", error);
        }
    };

    const fetchOrganizationData = async () => {
        getUsersOrganization(authToken).then(data => {
            if (Object.keys(data).length > 0 && data.success) {
                setUserAccessLevel(data.access_level);
                setOrganizationData(data.organization);
                getCountryById(data.organization.country_id).then(data => {
                    if (Object.keys(data).length > 0 && data.success) {
                        setCountryById(data.country);
                    }
                }).catch(err => console.log(err));
            }
        }).catch(err => console.log(err));
    }

    const fetchUserListings = async () => {
        getUserListings(authToken).then(data => {
            if (Object.keys(data).length > 0 && data.success) {
                let newArray = [...data.listings];
                setListingData(newArray);
            }
        }).catch(err => console.log(err));
    }

    const updateShowSidebar = (newVal) => {
        setShowSidebar(newVal);

    }

    const sideOffCanvasClose = () => {
        setSideCanvasShow(false);

    }
    const sideOffCanvasShow = () => {
        setSideCanvasShow(true);

    }

    useEffect(() => {
        console.log("Auth Token:", authToken);
        fetchUserData();
        fetchCountries();
        fetchOrganizationData();
        fetchUserListings();
    }, [authToken]);

    const renderView = () => {
        switch (activeView) {
            case "profile":
                return <ProfileDetails userData={userData} countries={countries} authToken={authToken}
                                       setUserData={setUserData}/>;
            case "myorganization":
                return <MyOrganizationDetails authToken={authToken} organizationData={organizationData}
                                              countryById={countryById} countries={countries}
                                              setOrganizationData={setOrganizationData}
                                              userAccessLevel={userAccessLevel}/>;
            case "mylistings":
                return <MyListings authToken={authToken} listingData={listingData}/>;
            default:
                return <ProfileDetails userData={userData} countries={countries} authToken={authToken}
                                       setUserData={setUserData}/>;
        }
    };
    const logout = async () => {
        try {
            const csrfToken = csrf;

            await axios.post('/logout', {
                _token: csrfToken // Include CSRF token in request body
            }, {
                headers: {
                    'X-CSRF-TOKEN': csrfToken,
                    'Content-Type': 'application/json'
                },
                withCredentials: true // Ensure cookies are sent
            });
            // Remove authToken from sessionStorage
            sessionStorage.removeItem('authToken');
            // Redirect to login or homepage after logout
            window.location.href = '/';
        } catch (error) {
            console.error("Logout failed", error);
        }
    };

    return (
        <div className="profile-container">
            <SideBarOffCanvasMobile sideCanvasShow={sideCanvasShow} sideOffCanvasClose={sideOffCanvasClose}
                                    updateShowSidebar={updateShowSidebar}
                                    activeView={activeView} setActiveView={setActiveView} logout={logout}/>
            <motion.div className='card tech_side_menu_card d-block d-lg-none mt-3 mb-3'
                        animate={showSidebar ? {x: 0} : {x: -100}}
                        transition={{duration: 1}}>
                <SidebarMenuMobile updateShowSidebar={updateShowSidebar} sideOffCanvasShow={sideOffCanvasShow}/>
            </motion.div>
            <div className=" d-flex flex-row">
                <div className="profile-nav-container d-none d-lg-block col-lg-2">
                    <button
                        className={`custon-mt text-20px font-semibold navbutton ${activeView === "profile" ? "active" : ""}`}
                        onClick={() => setActiveView("profile")}>
                        {t("translations.profile")}
                    </button>
                    <hr/>
                    <button
                        className={`text-20px font-semibold navbutton ${activeView === "myorganization" ? "active" : ""}`}
                        onClick={() => setActiveView("myorganization")}>
                        {t("translations.myorganization")}
                    </button>
                    <hr/>
                    <button
                        className={`text-20px font-semibold navbutton ${activeView === "mylistings" ? "active" : ""}`}
                        onClick={() => setActiveView("mylistings")}>
                        {t("translations.mylistings")}
                    </button>
                    <hr/>
                    <button className={`text-20px font-semibold navbutton`} onClick={() => logout()}>
                        {t("translations.logout")}
                    </button>
                    <hr className="custom-mb"/>
                </div>
                <div className="profile-view-content col-12 col-lg-10 ">
                    {renderView()}
                </div>
            </div>

        </div>
    );
}
