import React, {useState, useEffect} from "react";
import "../../../public/css/MyProfile.css";
import "../../../public/css/NewListing.css";
import {useTranslation} from 'react-i18next';
import SidebarMenuMobile from "../components/listings/SidebarMenuMobile";
import SideBarOffCanvasMobile from "../components/listings/SideBarOffCanvasMobile";
import TechnicalCharacteristics from "../components/listings/TechnicalCharacteristics.jsx";
import Availability from "../components/listings/Availability.jsx";
import {getEquipmentTypes, getEquipmentCategories} from "../APIs/Equipment.jsx";
import {motion} from "framer-motion";

export default function NewListing({csrf, authToken}) {
    const {t} = useTranslation(); // Initialize translation hook
    const [activeView, setActiveView] = useState("techicalchar");
    const [countries, setCountries] = useState([]);
    const [showSidebar, setShowSidebar] = useState(true);
    const [sideCanvasShow, setSideCanvasShow] = useState(false);
    const [listingAction, setListingAction] = useState(window.location.pathname.split('/')[1] === 'newlisting' ? 'add' : 'edit');
    const [typeOrId, setTypeOrId] = useState(window.location.pathname.split('/')[2]);
    const [equipmentType, setEquipmentType] = useState(null);
    const [equipmentCategories, setEquipmentCategories] = useState(null);
    const [equipmentSubCategory, setEquipmentSubCategory] = useState(null);
    const [formData, setFormData] = useState(
        {
            equipment_subcategory_id: null,
            manufacturer_id: null,
            equipment_model_id: null,
            equipment_model_name: '',
            listing_description: '',
            min_price: null,
            max_price: null,
            has_labour: null,
            labour_min_price: null,
            labour_max_price: null,
            labour_scope: '',
            labour_experience: '',
            labour_special_requirements: '',
            has_delivery_service: null,
            max_delivery_distance: null,
            max_delivery_distance_unit: '',
            delivery_fee: null,
            delivery_fee_unit: '',
            delivery_timeframe_description: '',
            include_setup: null,
            has_pick_up_point: null,
            pick_up_country_id: null,
            pick_up_region: '',
            pick_up_address_road: '',
            pick_up_address_number: '',
            pick_up_zip_code: '',
            pickup_timeframe_description: '',
            max_distance_booking: null,
            max_distance_booking_unit: '',
            is_license_required: null,
            license_type: '',
            renter_responsibilities: '',
            special_requirements: '',
            weight: null,
            power: null,
            working_width: null,
            number_of_cylinders: null,
            displacement: null,
            fuel_type: null,
            power_supply: null,
            number_of_furrows: null,
            is_fixed_or_mounted: null,
            measurement_parameters: null,
            flight_time: null,
            max_range: null,
            max_altitude: null,
            sensors_accompanied: '',
            accuracy: null,
            correction_services: [],
            title: '',
            description: '',
            author: '',
            license: null,
            publication_date: '',
            external_data_source: '',
            media: [],
            support_materials: [],
        }
    );

    const steps = [
        "techicalchar",
        "photos",
        "pricing",
        "drywetlease",
        "availability",
        "deliveryoptions",
        "supportmaterials",
        "rentingprerequisites"
    ];

    const fetchCountries = async () => {
        try {
            const response = await fetch('/api/countries', {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                },
            });
            const data = await response.json();
            setCountries(data.countries);
        } catch (error) {
            console.error("Error fetching countries:", error);
        }
    };

    const fetchEquipmentCategories = async (id) => {
        getEquipmentCategories(id).then(data => {
            if (Object.keys(data).length > 0 && data.success) {
                setEquipmentCategories(data.types);
            }
        }).catch(err => console.log(err));
    }

    const fetchEquipmentTypes = async () => {
        getEquipmentTypes().then(data => {
            if (Object.keys(data).length > 0 && data.success) {
                let type = '';
                if (typeOrId === 'traditional_equipment') {
                    type = "Traditional Equipment";
                } else {
                    type = "Digital Equipment";
                }
                for (const equipment of data.types) {
                    if (equipment.label === type) {
                        setEquipmentType(equipment);
                    }
                }
            }
        }).catch(err => console.log(err));
    }

    const updateShowSidebar = (newVal) => {
        setShowSidebar(newVal);

    }

    const sideOffCanvasClose = () => {
        setSideCanvasShow(false);

    }
    const sideOffCanvasShow = () => {
        setSideCanvasShow(true);

    }

    useEffect(() => {
        if (equipmentType) {
            fetchEquipmentCategories(Number(equipmentType.value));
        }
    }, [equipmentType]);

    useEffect(() => {
        console.log("Auth Token:", authToken);
        fetchCountries();
        if (listingAction === 'add') {
            fetchEquipmentTypes();
        }
    }, [authToken]);

    const renderView = () => {
        switch (activeView) {
            case "techicalchar":
                return equipmentType ?
                    <TechnicalCharacteristics equipmentType={equipmentType}
                                              equipmentCategories={equipmentCategories} formData={formData}
                                              setFormData={setFormData}/> : null;
            case "photos":
                return <div>{t("translations.photos")}</div>;
            case "pricing":
                return <div>{t("translations.pricing")}</div>;
            case "drywetlease":
                return <div>{t("translations.drywetlease")}</div>;
            case "availability":
                return <Availability formData={formData} setFormData={setFormData}/>;
            case "deliveryoptions":
                return <div>{t("translations.deliveryoptions")}</div>;
            case "supportmaterials":
                return <div>{t("translations.supportmaterials2")}</div>;
            case "rentingprerequisites":
                return <div>{t("translations.rentingprerequisites")}</div>;
            default:
                return <div>{t("translations.technicalCharacteristics")}</div>;
        }
    };

    const handleNext = () => {
        const currentIndex = steps.indexOf(activeView);
        if (currentIndex < steps.length - 1) {
            setActiveView(steps[currentIndex + 1]);
        }
    };

    const handlePrevious = () => {
        const currentIndex = steps.indexOf(activeView);
        if (currentIndex > 0) {
            setActiveView(steps[currentIndex - 1]);
        }
    };

    const currentStepIndex = steps.indexOf(activeView) + 1;
    const totalSteps = steps.length;

    return (
        <div className="profile-container">
            <SideBarOffCanvasMobile sideCanvasShow={sideCanvasShow} sideOffCanvasClose={sideOffCanvasClose}
                                    updateShowSidebar={updateShowSidebar}
                                    activeView={activeView} setActiveView={setActiveView}/>
            <motion.div className='card tech_side_menu_card d-block d-lg-none mt-3 mb-3'
                        animate={showSidebar ? {x: 0} : {x: -100}}
                        transition={{duration: 1}}>
                <SidebarMenuMobile updateShowSidebar={updateShowSidebar} sideOffCanvasShow={sideOffCanvasShow}/>
            </motion.div>
            <div className=" d-flex flex-row">
                <div className="profile-nav-container d-none d-lg-block col-lg-2">
                    <button
                        className={`custon-mt text-20px font-semibold navbutton ${activeView === "techicalchar" ? "active" : ""}`}
                        onClick={() => setActiveView("techicalchar")}>
                        {t("translations.technicalCharacteristics")}
                    </button>
                    <hr/>
                    <button className={`text-20px font-semibold navbutton ${activeView === "photos" ? "active" : ""}`}
                            onClick={() => setActiveView("photos")}>
                        {t("translations.photos")}
                    </button>
                    <hr/>
                    <button className={`text-20px font-semibold navbutton ${activeView === "pricing" ? "active" : ""}`}
                            onClick={() => setActiveView("pricing")}>
                        {t("translations.pricing")}
                    </button>
                    <hr/>
                    <button
                        className={`text-20px font-semibold navbutton ${activeView === "drywetlease" ? "active" : ""}`}
                        onClick={() => setActiveView("drywetlease")}>
                        {t("translations.drywetlease")}
                    </button>
                    <hr/>
                    <button
                        className={`text-20px font-semibold navbutton ${activeView === "availability" ? "active" : ""}`}
                        onClick={() => setActiveView("availability")}>
                        {t("translations.availability2")}
                    </button>
                    <hr/>
                    <button
                        className={`text-20px font-semibold navbutton ${activeView === "deliveryoptions" ? "active" : ""}`}
                        onClick={() => setActiveView("deliveryoptions")}>
                        {t("translations.deliveryoptions")}
                    </button>
                    <hr/>
                    <button
                        className={`text-20px font-semibold navbutton ${activeView === "supportmaterials" ? "active" : ""}`}
                        onClick={() => setActiveView("supportmaterials")}>
                        {t("translations.supportmaterials2")}
                    </button>
                    <hr/>
                    <button
                        className={`text-20px font-semibold navbutton ${activeView === "rentingprerequisites" ? "active" : ""}`}
                        onClick={() => setActiveView("rentingprerequisites")}>
                        {t("translations.rentingprerequisites")}
                    </button>
                    <hr className="custom-mb"/>
                    <div className="w-100 custom-mb text-center">
                        <button className={`text-20px font-semibold text-white exit-btn`}
                                onClick={() => window.location.href = '/profile'}>
                            {t("translations.exit")}
                        </button>
                    </div>

                </div>
                <div className="profile-view-content col-12 col-lg-10 ">
                    <div className="content"
                         style={{backgroundColor: activeView === 'availability' ? '#FBFBFB' : '#FFFFFF'}}>
                        {renderView()}
                    </div>
                    <div className="navigation-buttons-container">
                        <button className="btn navigation-buttons text-20px custom-ml-60" onClick={handlePrevious}
                                disabled={steps.indexOf(activeView) === 0}>
                            {t("translations.previous")}
                        </button>
                        <span
                            className="step-indicator text-20px">{t("translations.step")} {currentStepIndex}/{totalSteps}</span>
                        <button className="btn navigation-buttons text-20px custom-mr-60" onClick={handleNext}
                                disabled={steps.indexOf(activeView) === steps.length - 1}>
                            {t("translations.next")}
                        </button>
                    </div>
                </div>
            </div>

        </div>
    );
}
