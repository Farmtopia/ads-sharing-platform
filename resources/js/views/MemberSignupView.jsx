import React, { useEffect, useState } from "react";
import "../../../public/css/SignUpAndLogInView.css";
import MemberSignupForm from "../components/MemberSignUpForm.jsx";
import ToastContainer from '../components/toasts/ToastContainer.jsx';
import { useTranslation } from 'react-i18next';

export default function MemberSignupView({ action, csrf }) {
    const { t } = useTranslation(); // Initialize translation hook
    const [successMessages, setSuccessMessages] = useState([]);
    const [infoMessages, setInfoMessages] = useState([]);
    const [errorMessages, setErrorMessages] = useState([]);
    const [registerResp, setRegisterResp] = useState();

    useEffect(() => {
        if (registerResp) {
            console.log(registerResp);
            console.log(registerResp.status);
            if (registerResp.success) {
                setSuccessMessages(["Your email was successfully registered."]);
                window.location.href = '/';
            } else {
                setErrorMessages((prev) => ([...prev, registerResp.message]));
            }
        }
    }, [registerResp]);

    return (
        <div className='signuplogin_container'>
            <div className='signuplogin_form_container'>
                <div className='signup_or_login_container'>
                    <MemberSignupForm setResp={setRegisterResp} />
                </div>
                {/* <div className='signuplogin_terms text-14px'>Με την είσοδό σας, συμφωνείτε με τους 
                    <a href="https://google.com" target='_blank' className='text-14px'>Όρους Χρήσης</a> και την 
                    <a href="https://google.com" target='_blank' className='text-14px'>Πολιτική Απορρήτου</a>.
                </div> */}
            </div>
            <ToastContainer
                successMessages={successMessages}
                setSuccessMessages={setSuccessMessages}
                errorMessages={errorMessages}
                setErrorMessages={setErrorMessages}
                infoMessages={infoMessages}
                setInfoMessages={setInfoMessages}
            />
        </div>
    );
}