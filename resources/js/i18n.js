import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import HttpApi from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';


const defaultLanguage = window.defaultLanguage || 'en'; // Fallback to 'en' if not defined

i18n
  .use(HttpApi)
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    supportedLngs: ['en'], // Add your supported languages
    fallbackLng: defaultLanguage,
    detection: {
      order: ['querystring', 'cookie', 'localStorage', 'navigator'],
      caches: ['cookie'],
    },
    backend: {
      loadPath: '/api/translations/{{lng}}', // Laravel API endpoint
    },
  });

export default i18n;