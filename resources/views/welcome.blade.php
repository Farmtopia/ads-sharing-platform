<x-layout.layout>

    <x-slot:main_body>
        <div class="wrapper p-0">

            {{-- Example for implementing html in translated strings:
            {!! __('translations.whyChooseDescTitle3') !!} 
            --}}

            {{-- Hero Section --}}

            <section class="hero position-relative">
                <!-- Background Video -->
                <video class="hero-video" autoplay muted loop>
                    <source src="{{ asset('videos/hero-section-comp.mp4') }}" type="video/mp4">
                </video>
                <img class="hero-overlay-image" src="{{ asset('img/hero-overlay.svg') }}" alt="Description of the image">

                <!-- Overlay Content -->
                <div class="hero-overlay p-5 d-flex flex-column justify-content-between">
                    <div class="logo-container align-self-start">
                        <img class="logo" src="{{ asset('img/logos/white_logo.png') }}" alt="Farmtopia Logo"
                            style="max-width: 500px">
                    </div>

                    <div class="text-content text-start mt-auto align-self-end margin-bottom-10">
                        <h1 class="font-bold text-white">{!! __('translations.welcome') !!}</h1>
                        <h2 class="font-medium text-lime-green">{!! __('translations.welcomeSubtitle') !!}</h2>
                        <p class="font-normal text-white">{!! __('translations.welcomeDesc') !!}</p>
                    </div>
                </div>
            </section>

            {{-- Our Vision --}}

            <section class=" py-5 position-relative">
                <!-- Title Banner -->
                <div class="row gutter-x-0">
                    <div class="col-12 position-relative">
                        <div class="our-vision-title bg-green py-2">
                            <h1 class="text-white font-bold m-0">{!! __('translations.ourVision') !!}</h1>
                        </div>
                    </div>
                </div>

                <!-- Content -->
                <div class="row our-vision align-items-stretch mt-4 p-5 d-flex">
                    <!-- Image Column -->
                    <div class="col-12 col-lg-6 d-flex align-items-stretch">
                        <img class="p-0 our-vision-image img-fluid w-100" src="{{ asset('img/combine.png') }}"
                            alt="Description of the image">
                    </div>

                    <!-- Text Content Column -->
                    <div class="col-12 col-lg-6 d-flex flex-column">
                        <h2 class="text-blue fw-bold mt-3 mb-3">{!! __('translations.ourVisionSubtitle') !!}</h2>
                        <p>{!! __('translations.ourVisionDesc') !!}</p>
                    </div>
                </div>
            </section>

            {{-- About Us --}}

            <section class="about-us">
                <div class="row justify-center mb-4 gutter-x-0">
                    <div class="col-12 about-us-title bg-green">
                        <h1 class="text-white font-bold p-3">{!! __('translations.aboutUs') !!}</h1>
                    </div>
                </div>
                <div class="row align-items-center">
                    <!-- Text Content Column -->
                    <div class="col-12 col-lg-6 text-black p-5">
                        <h2 class="text-blue fw-bold mb-3">{!! __('translations.aboutUsSubtitle') !!}</h2>
                        <p>{!! __('translations.aboutUsDesc') !!}</p>
                        <a href="https://farmtopia.eu/" target="_blank" class="text-center text-blue text-decoration-none ">{!! __('translations.learnMore') !!}
                            <i class="bi bi-arrow-right text-blue"></i></a>
                    </div>
                    <!-- Image Column -->
                    <div class="col-12 col-lg-6 text-white p-3 d-flex flex-column justify-content-center">
                        <img class="about-us-image img-fluid"
                            src="{{ asset('img/logos/color_logo.png') }}" alt="Farmtopia Logo">
                        <img class="eu-logo-image img-fluid mt-3"
                            src="{{ asset('img/logos/EN_Co-fundedbytheEU_RGB_POS.png') }}" alt="EU Logo">
                    </div>
                </div>
            </section>

            {{-- Why Choose the ADS Platform? --}}

            <section class=" p-0 position-relative">
                <!-- Background Image -->
                <img class="wave-img position-absolute top-0 start-0 w-100 " src="{{ asset('img/green-wave-1.png') }}"
                    alt="Background Image">
                <!-- Title Banner -->
                <div class="row position-relative gutter-x-0">
                    <div class="col-12">
                        <!-- Title -->
                        <div class="why-choose-title bg-green py-2 position-relative" style="z-index: 2;">
                            <h1 class="text-white font-bold m-0">{!! __('translations.whyChoose') !!}</h1>
                        </div>
                    </div>
                </div>



                <div class="row why-choose align-items-stretch mt-4 p-5 d-flex">
                    <!-- Text Column -->
                    <div class="col-12 col-lg-6 d-flex flex-column h-100">
                        <h2 class="text-blue fw-bold mb-3">{!! __('translations.whyChooseSubtitle') !!}</h2>
                        <h3 class="text-green fw-bold mb-3">{!! __('translations.whyChooseDescTitle1') !!}</h3>
                        <p>{!! __('translations.whyChooseDesc1') !!}</p>
                        <h3 class="text-green fw-bold mb-3">{!! __('translations.whyChooseDescTitle2') !!}</h3>
                        <p>{!! __('translations.whyChooseDesc2') !!}</p>
                        <h3 class="text-green fw-bold mb-3">{!! __('translations.whyChooseDescTitle3') !!}</h3>
                        <p>{!! __('translations.whyChooseDesc3') !!}</p>
                        <h3 class="text-green fw-bold mb-3">{!! __('translations.whyChooseDescTitle4') !!}</h3>
                        <p>{!! __('translations.whyChooseDesc4') !!}</p>
                    </div>

                    <!-- Image Column -->
                    <div class="col-12 col-lg-6 d-flex align-items-stretch">
                        <img class="why-choose-image img-fluid" src="{{ asset('img/driver.png') }}"
                            alt="Description of the image" style="z-index: 2;">
                    </div>
                </div>

            </section>


            {{-- Why sign up to the platform? --}}
            <div class="row gutter-x-0">
                <div class="col-12 position-relative">
                    <div class="why-signup-title bg-green py-2">
                        <h1 class="text-white font-bold m-0">{!! __('translations.whySignUp') !!}</h1>
                    </div>
                </div>
            </div>
            <section class="why-signup">
                
                <div class="row align-items-stretch why-signup m-5">
                    <!-- Text Content Column Left-->
                    <div class="col-12 col-lg-6 text-black why-signup-box p-5 ">
                        <h2 class="text-blue fw-bold mb-3">{!! __('translations.whySignUpSubtitle1') !!}</h2>
                        <p>{!! __('translations.whySignUpDesc1') !!}</p>
                        <ul class="list-unstyled">
                            <li class="text-green"><i
                                    class="bi me-3 text-green bi-check-circle-fill"></i>{!! __('translations.whySignUpLi1') !!}
                            </li>
                            <li class="text-green"><i
                                    class="bi me-3 text-green bi-check-circle-fill"></i>{!! __('translations.whySignUpLi2') !!}
                            </li>
                        </ul>
                    </div>
                    <!-- Text Content Column Right-->
                    <div class="col-12 col-lg-6 text-black why-signup-box p-5">
                        <h2 class="text-blue fw-bold mb-3">{!! __('translations.whySignUpSubtitle2') !!}</h2>
                        <p>{!! __('translations.whySignUpDesc2') !!}</p>
                        <ul class="list-unstyled">
                            <li class="text-green "><i
                                    class="bi me-3 text-green bi-check-circle-fill"></i>{!! __('translations.whySignUpLi3') !!}
                            </li>
                            <li class="text-green"><i
                                    class="bi me-3 text-green bi-check-circle-fill"></i>{!! __('translations.whySignUpLi4') !!}
                            </li>
                        </ul>
                        <i>{!! __('translations.whySignUpDesc3') !!}</i>
                    </div>
                </div>

            </section>

            {{-- Join Us --}}

            <section class="bg-blue p-5">
                <div class="row justify-center align-items-center join-us">
                    <div class="col-12 col-lg-8 ">
                        <h1 class="text-white text-start font-normal">{!! __('translations.joinUs') !!}</h1>
                    </div>
                    <div class="col-12 col-lg-4 text-end bg-blue">
                        <a href="{{ route('signuplogin', ['action' => 'login']) }}" class="join-us-btn text-decoration-none">{!! __('translations.joinNow') !!}</a>
                    </div>
                </div>
            </section>

            {{-- Project Partners --}}

            <section class="partners p-5">
                <div class="m-4">
                    <div class="partners-title bg-green">
                        <h1 class="text-white font-bold p-4">{!! __('translations.projectPartners') !!}</h1>
                    </div>
                </div>

                <div class="angry-grid">
                    <div id="item-0"><img class="grid-logo" src="{{ asset('img/logos/sata.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-1"><img class="grid-logo" src="{{ asset('img/logos/agricolus.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-2"><img class="grid-logo" src="{{ asset('img/logos/neuropublic.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-3"><img class="grid-logo" src="{{ asset('img/logos/gaia.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-4"><img class="grid-logo" src="{{ asset('img/logos/kuLeuven.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-5"><img class="grid-logo" src="{{ asset('img/logos/wimGovaerts.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-6"><img class="grid-logo" src="{{ asset('img/logos/politecnico.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-7"><img class="grid-logo" src="{{ asset('img/logos/anamob.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-8"><img class="grid-logo" src="{{ asset('img/logos/smartRdi.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-9"><img class="grid-logo" src="{{ asset('img/logos/ITC.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-10"><img class="grid-logo" src="{{ asset('img/logos/arvalis.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-11"><img class="grid-logo" src="{{ asset('img/logos/afentiko.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-12"><img class="grid-logo" src="{{ asset('img/logos/art21.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-13"><img class="grid-logo" src="{{ asset('img/logos/cuma.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-14"><img class="grid-logo" src="{{ asset('img/logos/UCD.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-15"><img class="grid-logo" src="{{ asset('img/logos/foodscale.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-16"><img class="grid-logo" src="{{ asset('img/logos/greenSupplyChain.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-17"><img class="grid-logo" src="{{ asset('img/logos/pilzeNagy.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-18"><img class="grid-logo" src="{{ asset('img/logos/AUA.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-19"><img class="grid-logo" src="{{ asset('img/logos/agrifood.png') }}"
                            alt="Description of the image"></div>
                    <div id="item-20"><img class="grid-logo" src="{{ asset('img/logos/substratproduktion.png') }}"
                            alt="Description of the image"></div>
                </div>
                <div class="d-flex mx-5 justify-content-center">
                    <a href="https://farmtopia.eu/our-team/" target="_blank" class="text-center text-blue text-decoration-none ">{!! __('translations.learnMorePartners') !!}
                        <i class="bi bi-arrow-right text-blue"></i></a>
                </div>
            </section>

            {{-- Contact us --}}

            <section class=" py-5 position-relative">
                <!-- Background Image -->
                <img class="wave-img position-absolute start-0 w-100 " src="{{ asset('img/green-wave-1.png') }}"
                    alt="Background Image">
                <!-- Title Banner -->
                <div class="row position-relative gutter-x-0">
                    <div class="col-12">
                        <!-- Title -->
                        <div class="contact-us-title bg-green py-2 position-relative" style="z-index: 2;">
                            <h1 class="text-white font-bold p-4">{!! __('translations.contactUs') !!}</h1>
                        </div>
                    </div>
                </div>
                <div class="row contact-us mt-5 align-items-center">

                    <!-- Text Content Column Left-->
                    <div class="col-12 col-lg-12 text-black bg-light-blue p-5">
                        <h2 class="text-blue fw-bold mb-3">{!! __('translations.contactUsTitle') !!}</h2>
                        <p>{!! __('translations.contactUsDesc') !!}</p>
                        <h4 class="font-medium text-green">{!! __('translations.contactUsSubtitle1') !!}</h4>
                        <p>{!! __('translations.contactUsDesc1') !!}</p>
                        <h4 class="font-medium text-green">{!! __('translations.contactUsSubtitle2') !!}</h4>
                        <p>{!! __('translations.contactUsDesc2') !!}</p>
                        <h4 class="font-medium text-green">{!! __('translations.contactUsSubtitle3') !!}</h4>
                        <p>{!! __('translations.contactUsDesc3') !!}</p>
                        <h4 class="font-medium text-green">{!! __('translations.contactUsSubtitle4') !!}</h4>
                        <p>{!! __('translations.contactUsDesc4') !!}</p>
                        <div class="d-flex justify-content-center mt-4">
                            <a href="mailto:contact@example.com" class="mt-4 contact-submit-btn">
                                {!! __('translations.contactUs') !!}
                            </a>
                        </div>
                    </div>

                    <!-- Form Column Right-->

                    {{-- <div class="col-12 col-lg-6 text-black  align-content-center justify-content-center p-5">
                        <form action="#" method="POST">
                            <div class="mb-3">
                                <label for="firstName" class="form-label">{!! __('translations.firstName') !!}</label>
                                <input type="text" class="form-control" id="firstName" name="firstName" placeholder="Write your first name" required>
                            </div>
                            <div class="mb-3">
                                <label for="lastName" class="form-label">{!! __('translations.lastName') !!}</label>
                                <input type="text" class="form-control" id="lastName" name="lastName" placeholder="Write your last name" required>
                            </div>
                            <div class="mb-3">
                                <label for="email" class="form-label">{!! __('translations.email') !!}</label>
                                <input type="email" class="form-control" id="email" name="email" placeholder="Write your email" required>
                            </div>
                            <div class="mb-3">
                                <label for="subject" class="form-label">{!! __('translations.subject') !!}</label>
                                <input type="text" class="form-control" id="subject" name="subject" placeholder="Write your message" required>
                            </div>
                            <button type="submit" class="contact-submit-btn">{!! __('translations.submit') !!}</button>
                        </form>
                    </div> --}}

                </div>
            </section>

        </div>

    </x-slot:main_body>

</x-layout.layout>
