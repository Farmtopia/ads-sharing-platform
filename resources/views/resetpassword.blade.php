<x-layout.layout>   
    <x-slot:title>
        Farmtopia | Reset password
    </x-slot:title>

    <x-slot:description>
        Reset password
    </x-slot:description>

    <x-slot:main_body>
        <div class="wrapper p-0">
            <div class="row m-0 min-vh-100">
                <div class="col-12 bg-white text-white p-0">
                    <div id="resetpassword" data-token="{{ $token }}" data-email="{{ $email }}">
                    </div>
                </div>
            </div>
        </div>
    </x-slot:main_body>

</x-layout.layout>
