{{-- This file is used for menu items by any Backpack v6 theme --}}
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="la la-home nav-icon"></i> {{ trans('backpack::base.dashboard') }}</a></li>

<x-backpack::menu-dropdown title="Authorization-Management" icon="la la-user-circle">
    <x-backpack::menu-dropdown-header title="Authentication" />
    <x-backpack::menu-dropdown-item title="Users" icon="la la-user" :link="backpack_url('user')" />
    <x-backpack::menu-dropdown-item title="Roles" icon="la la-group" :link="backpack_url('role')" />
    <x-backpack::menu-dropdown-item title="Permissions" icon="la la-key" :link="backpack_url('permission')" />
</x-backpack::menu-dropdown>

<x-backpack::menu-item title="Pages" icon="la la-file" :link="backpack_url('page')" />

<x-backpack::menu-item title="Settings" icon="la la-cog" :link="backpack_url('setting')" />

<x-backpack::menu-item title="Equipment types" icon="la la-question" :link="backpack_url('equipment-type')" />
<x-backpack::menu-item title="Equipment categories" icon="la la-question" :link="backpack_url('equipment-category')" />
<x-backpack::menu-item title="Equipment subcategories" icon="la la-question" :link="backpack_url('equipment-subcategory')" />
<x-backpack::menu-item title="Fields" icon="la la-question" :link="backpack_url('field')" />
<x-backpack::menu-item title="Manufacturers" icon="la la-question" :link="backpack_url('manufacturer')" />
<x-backpack::menu-item title="Listings" icon="la la-question" :link="backpack_url('listing')" />
<x-backpack::menu-item title="Listing bookings" icon="la la-question" :link="backpack_url('listing-booking')" />
<x-backpack::menu-item title="Technical characteristics" icon="la la-question" :link="backpack_url('technical-characteristics')" />
<x-backpack::menu-item title="Materials" icon="la la-question" :link="backpack_url('material')" />
<x-backpack::menu-item title="Invitations" icon="la la-question" :link="backpack_url('invitation')" />
<x-backpack::menu-item title="Equipment models" icon="la la-question" :link="backpack_url('equipment-model')" />