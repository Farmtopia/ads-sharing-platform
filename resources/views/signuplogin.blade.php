<x-layout.layout>   
    <x-slot:title>
        Farmtopia | Sign up
    </x-slot:title>

    <x-slot:description>
        By signing up, you become part of a community that believes in shared resources, knowledge, and a sustainable future.
    </x-slot:description>

    <x-slot:main_body>
        <div class="wrapper p-0">
            <div class="row m-0 min-vh-100">
                <div class="col-12 col-lg-7 bg-very-light-blue p-0">
                    <div class="d-flex flex-column signup-text-container p-3">
                        <div class="pt-103 signup-logo-image-container">
                            <img class="w-100" src="{{ asset('img/logos/color_logo.png') }}" alt="Farmtopia Logo">
                        </div>
                        <div class="pt-4">
                            <h1 class="text-green mb-2">{{ __('translations.signuppagetitle') }}</h1>
                            <h2 class="text-blue mb-3">{{ __('translations.signuppagesubtitle') }}</h2>
                            <p class="text-18px text-black">{{ __('translations.signuppagetext') }}</p>
                            <div class="d-flex flex-row">
                                <p class="text-42px text-green line-height-initial me-custom">1.</p>
                                <div>
                                    <p class="text-green text-20px mb-1 text-black">{{ __('translations.signuppagefirstpoint') }}</p>
                                    <p class="text-18px text-black">{{ __('translations.signuppagefirspointtext') }}</p>
                                </div>
                            </div>
                            <div class="d-flex flex-row">
                                <p class="text-42px text-green line-height-initial me-custom">2.</p>
                                <div>
                                    <p class="text-green text-20px mb-1 text-black">{{ __('translations.signuppagesecondpoint') }}</p>
                                    <p class="text-18px text-black">{{ __('translations.signuppagesecondpointtext') }}</p>
                                </div>
                            </div>
                            <div class="d-flex flex-row">
                                <p class="text-42px text-green line-height-initial me-custom">3.</p>
                                <div>
                                    <p class="text-green text-20px mb-1 text-black">{{ __('translations.signuppagethirdpoint') }}</p>
                                    <p class="text-18px text-black">{{ __('translations.signuppagethirdpointtext') }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    
                </div>
                <div class="col-12 col-lg-5 bg-white text-white p-0">
                    <div id="signuplogin">
                    </div>
                </div>
            </div>
        </div>
    </x-slot:main_body>

</x-layout.layout>

<script>
    window.action = "{{ $action }}"; // Pass the action variable to React
</script>