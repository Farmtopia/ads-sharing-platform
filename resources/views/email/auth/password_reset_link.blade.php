<h1>Password Reset Link</h1>

<a href="{{\App\Helpers\AuthHelper::getUserPasswordResetURL($user)}}">Reset Password</a>
