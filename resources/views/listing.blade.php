<x-layout.layout>
    <x-slot:main_body>
        <div class="wrapper listing-container p-0">
            <div id="listingview" data-listing-id="{{ request()->route('listing_id') }}">
            </div>
        </div>
    </x-slot:main_body>
</x-layout.layout>