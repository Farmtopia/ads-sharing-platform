<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ $title ?? 'Farmtopia' }}</title>

    <meta name="keywords" content="{{ $keywords ?? 'Farmtopia, collaboration, equipment, marketplace, agriculture' }}" />
    <meta name="description" content="{{ $description ?? 'Welcome to FARMTOPIA, where innovation meets collaboration in agriculture. Our platform is designed to make farming smarter, more affordable, and efficient.' }}">
    <meta name="author" content="{{ $author ?? 'AUA Sftg' }}">

    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content='Farmtopia' />
    <meta property="og:description" content="'Welcome to FARMTOPIA, where innovation meets collaboration in agriculture. Our platform is designed to make farming smarter, more affordable, and efficient." />
    {{-- <meta property="og:url" content="https://digital-agriculture.horizoncodecs.eu/" /> --}}
    <meta property="og:site_name" content="FARMTOPIA" />
    <meta property="og:image" content="{{asset('img/logos/color_logo.png')}}" />
    <meta name="twitter:card" content="summary_large_image" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="{{ asset('img/logos/favicon.svg') }}">
    <link rel="apple-touch-icon" href="{{ asset('img/logos/favicon.svg') }}">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons/font/bootstrap-icons.css">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

    <!-- Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500&display=swap">

    @viteReactRefresh
    @vite(['resources/js/react_app.jsx', 'resources/css/app.css'])
    {{$head_scripts??''}}

    <script>
        window.Settings = {!! json_encode([
                    'csrf'=>csrf_token(),
                    'appUrl'=>url('/'),
                    'errors' => $errors->all(),
                ]) !!}
    </script>
    <script>
        window.defaultLanguage = "{{ app()->getLocale() ?? 'en' }}";
    </script>

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
</head>
<body>
    <header id="header">
        <div class="header-body w-100 p-0 d-flex justify-content-between align-items-center">
            <div class="wrapper header-container ">
                <div class="header-row">
                    <div class="header-column flex-grow-0">
                        <div class="header-row">
                            <div class="header-logo">
                                <a href="{{ route('home') }}">
                                    <img alt="FARMTOPIA" style="max-width: 219px" src="{{ asset('img/logos/color_logo.png') }}">
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="header-column justify-content-end">
                        <div class="header-row">
                            <div class="header-nav justify-content-start ml-100">
                                <div class="header-nav-main flex-grow-1">
                                    <nav class="navbar navbar-expand-lg navbar-light flex-grow-1">
                                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                                            <span class="navbar-toggler-icon"></span>
                                        </button>
                                        <div class="collapse navbar-collapse p-3" id="navbarNav">
                                            <ul class="navbar-nav mobile-nav">
                                                @auth
                                                    <li class="nav-item">
                                                        <a class="nav-link text-20px text-blue {{ (request()->routeIs('marketplace')) ? ' active' : '' }}" href="{{ route('marketplace') }}">{{ __('translations.market_place') }}</a>
                                                    </li>
                                                @endauth
                                                {{-- <li class="nav-item">
                                                    <a class="nav-link text-20px text-blue" href="{{ route('home') }}">{{ __('translations.knowledge_base') }}</a>
                                                </li>--}}
                                                {{-- <li class="nav-item">
                                                    <a class="nav-link text-20px text-blue" href="{{ route('listingview') }}">{{ __('translations.faqs') }}</a>
                                                </li>  --}}
                                                @guest
                                                    <li class="nav-item d-lg-none"> <!-- Hidden on larger screens -->
                                                        <a class="nav-link text-20px text-blue" href="{{ route('signuplogin', ['action' => 'signup']) }}">{{ __('translations.signup') }}</a>
                                                    </li>
                                                    <li class="nav-item d-lg-none"> <!-- Hidden on larger screens -->
                                                        <a class="nav-link text-20px text-blue" href="{{ route('signuplogin', ['action' => 'login']) }}">{{ __('translations.login') }}</a>
                                                    </li>
                                                @endguest
                                                @auth
                                                    <li class="nav-item d-lg-none"> <!-- Hidden on larger screens -->
                                                        <a class="nav-link text-20px text-blue" href="{{ route('profile') }}">{{ __('translations.profile') }}</a>
                                                    </li>
                                                @endauth
                                            </ul>
                                            <div class="d-none d-lg-flex ms-auto"> <!-- Hidden on smaller screens -->
                                                @guest
                                                    <a class="btn signup-button me-2" href="{{ route('signuplogin', ['action' => 'signup']) }}">{{ __('translations.signup') }}</a>
                                                    <a class="btn login-button" href="{{ route('signuplogin', ['action' => 'login']) }}">{{ __('translations.login') }}</a>
                                                @endguest
                                                @auth
                                                    <a class="btn profile-button" href="{{ route('profile') }}">
                                                        <img src="{{ asset('img/icons/account_circle.svg') }}" alt="Profile" />
                                                    </a>
                                                @endauth
                                            </div>
                                        </div>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div role="main" class="main">
        @isset($main_body)
            {{ $main_body }}
        @endisset
    </div>

    <footer class="bg-blue">
        <div class="footer-body wrapper">
            <div class="footer-link d-flex flex-column">
                <img class="footer-logo" src="{{ asset('img/logos/white_logo.png') }}" alt="Farmtopia Logo"/>
                <div>
                    <p class="text-12px text-white">© {{ date('Y') }} Farmtopia Rights Reserved</p>
                </div>

            </div>
            <div class="footer-menu">
                <a class="nav-link text-white mb-3" href="{{ route('home') }}">{{ __('translations.home') }}</a>
                @auth
                    <a class="nav-link text-white mb-3" href="{{ route('marketplace') }}">{{ __('translations.market_place') }}</a>
                @endauth
                
            </div>
            <div class="socials text-center">
                <p class="text-white mb-3">{{ __('translations.oursocialmedia') }}</p>
                <a href="https://x.com/i/flow/login?redirect_after_login=%2FFarmtopia_EU" class="text-decoration-none me-3" target="_blank">
                    <img src="{{asset('img/logos/x.png')}}" alt="X">
                </a>
                <a href="https://www.linkedin.com/company/farmtopiaproject/about/" class="text-decoration-none" target="_blank">
                    <img src="{{asset('img/logos/linkedin.png')}}" alt="Linkedin">
                </a>
            </div>
            <div class="horizon_logo_container">
                <img class="european_flag" src="{{ asset('img/logos/csa-funded-by-eu-logo-768x171.png') }}" alt="European Flag"/>
                <p class="horizon_text text-12px text-white">This project has received funding from the Horizon Europe research and innovation programme under Grant Agreement No 101084179</p>
                
            </div>
        </div>
    </footer>

    {{$body_scripts??''}}

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
    <!-- Bootstrap JS and Popper.js -->
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.min.js"></script>

    @auth
        <script>
            if (!sessionStorage.getItem('authToken')) {
                sessionStorage.setItem('authToken', '{{ Auth::user()->createToken('api-token')->plainTextToken }}');
            }
            console.log(sessionStorage.getItem('authToken'));
        </script>
    @endauth

</body>
</html>
