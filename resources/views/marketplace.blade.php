<x-layout.layout>
    <x-slot:title>
        Farmtopia | Marketplace
    </x-slot:title>

    <x-slot:description>
        Marketplace
    </x-slot:description>

    <x-slot:main_body>
        <div id='marketplace' class="wrapper p-0 m-0">
        </div>
    </x-slot:main_body>

</x-layout.layout>
