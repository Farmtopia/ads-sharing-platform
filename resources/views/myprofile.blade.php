<x-layout.layout>   
    <x-slot:title>
        Farmtopia | My profile
    </x-slot:title>

    <x-slot:description>
        By signing up, you become part of a community that believes in shared resources, knowledge, and a sustainable future.
    </x-slot:description>

    <x-slot:main_body>
        <div class="wrapper p-0">
            <div id="myprofile" class="m-0 min-vh-100">
                
            </div>
        </div>
    </x-slot:main_body>

</x-layout.layout>
