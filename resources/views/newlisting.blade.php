<x-layout.layout>   
    <x-slot:title>
        Farmtopia | New Listing
    </x-slot:title>

    <x-slot:description>
        Upload new listing
    </x-slot:description>

    <x-slot:main_body>
        <div class="wrapper p-0">
            <div id="newlisting" class="m-0">
                
            </div>
        </div>
    </x-slot:main_body>

</x-layout.layout>
