<x-layout.layout>   
    <x-slot:title>
        Farmtopia | Member Sign up
    </x-slot:title>

    <x-slot:description>
        By signing up, you become part of a community that believes in shared resources, knowledge, and a sustainable future.
    </x-slot:description>

    <x-slot:main_body>
        <div class="wrapper p-0">
            <div class="m-0 min-vh-100">
                <div class="col-12 bg-white text-white p-0">
                    <div id="membersignup">
                    </div>
                </div>
            </div>
        </div>
    </x-slot:main_body>

</x-layout.layout>
