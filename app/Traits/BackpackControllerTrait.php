<?php
namespace App\Traits;

use App\Fields\UserFields;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

trait BackpackControllerTrait
{

    private $fields = [];
    public function __construct()
    {
        if(defined('self::FIELDS_CLASS') && self::FIELDS_CLASS!==null)
            $this->fields = new (self::FIELDS_CLASS);
        else
            throw new \Exception('FIELDS_CLASS constant not defined in '.get_class($this));

        parent::__construct();
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    function setupListOperation():void
    {
        if(method_exists($this->fields,'filters') ){
            $this->fields->filters($this->crud);
        }
        $this->crud->addColumns($this->fields->list_fields());
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    function setupCreateOperation():void
    {
        if(method_exists($this,'createViewConfig'))
        {
            $this->createViewConfig();
        }

        CRUD::setValidation($this->fields->VALIDATION_RULES);
        $this->crud->addFields($this->fields->create_fields());
    }


    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    function setupUpdateOperation():void
    {
        if(method_exists($this,'updateViewConfig'))
        {
            $this->updateViewConfig();
        }

        CRUD::setValidation($this->fields->VALIDATION_RULES);
        $this->crud->addFields($this->fields->create_fields());
    }
}
