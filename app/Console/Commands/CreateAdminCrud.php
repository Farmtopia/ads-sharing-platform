<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateAdminCrud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:admin-crud {model}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** make sure we have a name for our helper class */
        $model_name = $this->argument('model');
        if($model_name==='') throw new \Exception('Class name cannot be empty');

        $this->call('backpack:crud', ['name' => $model_name]);
        $this->call('make:bp-fields-helper', ['className' => $model_name.'Fields']);
    }
}
