<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Contracts\Console\PromptsForMissingInput;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class BackpackFieldsHelper extends Command implements PromptsForMissingInput
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:bp-fields-helper {className}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        /** make sure we have a name for our helper class */
        $input = $this->argument('className');
        if($input==='') throw new \Exception('Class name cannot be empty');

        /** create a pascal case class name */
        $className = \App\Helpers\Str::pascal(($input));

        /** check if the file already exists and stop the procedure if that's the case*/
        if(file_exists(base_path('app/Fields/'.$className.'.php')))
        {
            $this->error('File already exists');
            return;
        }

        /** create the file form the stub by replacing the ClassName (default in Stub) with the user input class name */
        $stubCode = file_get_contents(base_path('stubs/custom/backpack-fields-helper-class.stub'));
        $code = Str::replace('ClassName',$className,$stubCode);

        file_put_contents(base_path('app/Fields/'.$className.'.php'),$code);
        $this->alert("$className helper class created successfully");
    }

    /**
     * Prompt for missing input arguments using the returned questions.
     *
     * @return array
     */
    protected function promptForMissingArgumentsUsing()
    {
        return [
            'className' => 'Name of the class to create',
        ];
    }
}
