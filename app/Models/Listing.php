<?php

namespace App\Models;

use App\Enums\MaterialGroupEnum;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Listing extends Model
{
    use CrudTrait;
    use HasFactory;

    /*
    |--------------------------------------------------------------------------
    | GLOBAL VARIABLES
    |--------------------------------------------------------------------------
    */

    protected $table = 'listings';
    // protected $primaryKey = 'id';
    // public $timestamps = false;
    protected $guarded = ['id'];
    // protected $fillable = [];
    // protected $hidden = [];

    /*
    |--------------------------------------------------------------------------
    | FUNCTIONS
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | RELATIONS
    |--------------------------------------------------------------------------
    */
        public function user(): BelongsTo
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function country(): BelongsTo
        {
            return $this->belongsTo(Country::class, 'pick_up_country_id');
        }

        public function manufacturer(): BelongsTo
        {
            return $this->belongsTo(Manufacturer::class, 'manufacturer_id');
        }

        public function equipment_subcategory(): BelongsTo
        {
            return $this->belongsTo(EquipmentSubcategory::class, 'equipment_subcategory_id');
        }

        public function technical_characteristics(): HasOne
        {
            return $this->hasOne(TechnicalCharacteristics::class, 'listing_id');
        }

        public function listing_bookings(): HasMany
        {
            return $this->hasMany(ListingBooking::class, 'listing_id');
        }

        public function materials(): HasMany
        {
            return $this->hasMany(Material::class, 'listing_id');
        }

    /*
    |--------------------------------------------------------------------------
    | SCOPES
    |--------------------------------------------------------------------------
    */

    /*
    |--------------------------------------------------------------------------
    | ACCESSORS
    |--------------------------------------------------------------------------
    */
    public function getMediaAttribute()
    {
        return $this->materials()->where('group', MaterialGroupEnum::MEDIA->value)->get();
    }

    public function getSupportMaterialAttribute()
    {
        return $this->materials()->where('group', MaterialGroupEnum::SUPPORT_MATERIAL->value)->get();
    }

    /*
    |--------------------------------------------------------------------------
    | MUTATORS
    |--------------------------------------------------------------------------
    */
}
