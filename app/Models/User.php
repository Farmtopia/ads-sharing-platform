<?php

namespace App\Models;

use App\Enums\UserRegistrationStatus;
use App\Mail\UserVerificationEmail;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use CrudTrait;
    use HasApiTokens, HasFactory, Notifiable;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $guarded = ['id'];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
        'social' => 'array'
    ];

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Relationships
     * -----------------------------------------------------------------------------------------------------------------
     */

    public function organizations(): BelongsToMany
    {
        return $this->belongsToMany(Organization::class, 'user_organization_access', 'user_id', 'organization_id')
            ->withPivot('access_level');
    }

//    public function region(): BelongsTo
//    {
//        return $this->belongsTo(Region::class, 'region_id');
//    }

    public function listings(): HasMany
    {
        return $this->hasMany(Listing::class, 'user_id');
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function listing_bookings(): HasMany
    {
        return $this->hasMany(ListingBooking::class, 'booker_id');
    }



    public static function booted()
    {
        static::updated(function (self $user) {
            if ($user->isDirty('status') && $user->status === UserRegistrationStatus::ACCEPTED->value)
            {
                Mail::to($user)->send(new UserVerificationEmail($user));
            }
        });
    }
}
