<?php

namespace App\Models;

use App\Enums\OrganizationAccessEnum;
use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;

class Organization extends Model
{
    use CrudTrait;
    use HasFactory;

    protected $guarded = ['id'];

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Relationships
     * -----------------------------------------------------------------------------------------------------------------
     */

    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'user_organization_access', 'organization_id', 'user_id')
            ->withPivot('access_level');
    }

    public function country(): BelongsTo
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Accessors
     * -----------------------------------------------------------------------------------------------------------------
     */
    public function members()
    {
        return $this->users()->where('access_level', OrganizationAccessEnum::ORG_MEMBER)->get();
    }

    public function admins()
    {
        return $this->users()->where('access_level', OrganizationAccessEnum::ORG_ADMIN)->get();
    }

    public function listings()
    {
        return Listing::whereIn('user_id', $this->users()->pluck('id'))->get();
    }
}
