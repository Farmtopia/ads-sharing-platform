<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Country extends Model
{
    use CrudTrait;
    use HasFactory;

    /**
     * -----------------------------------------------------------------------------------------------------------------
     * Relationships
     * -----------------------------------------------------------------------------------------------------------------
     */

//    public function regions(): HasMany
//    {
//        return $this->hasMany(Region::class, 'country_id');
//    }

    public function users(): HasMany
    {
        return $this->hasMany(User::class, 'country_id');
    }

    public function listings(): HasMany
    {
        return $this->hasMany(Listing::class, 'pick_up_country_id');
    }

    public function organizations(): HasMany
    {
        return $this->hasMany(Organization::class, 'country_id');
    }
}
