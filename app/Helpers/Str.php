<?php

namespace App\Helpers;

class Str
{
    /**
     *
     * Sanitize a string to PascalCase
     *
     * @param string $string
     * @return string
     */
    public static function pascal(string $string):string
    {
        $pascalCaseWords = array_map(function($word) {
            return ucfirst($word);
        }, explode(' ', $string));

        $pascalCaseWords = implode('',$pascalCaseWords);
        if(is_numeric(substr($pascalCaseWords,0,1)))
        {
            $pascalCaseWords = '_'.$pascalCaseWords;
        }

        return $pascalCaseWords;
    }
}
