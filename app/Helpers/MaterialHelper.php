<?php

namespace App\Helpers;

use App\Enums\MaterialGroupEnum;
use App\Enums\MaterialTypeEnum;
use Illuminate\Http\UploadedFile;
use Illuminate\Database\Eloquent\Model;

class MaterialHelper
{
    public static function handleMaterialUpload(UploadedFile $file, Model $listing, MaterialGroupEnum $group): void
    {
        $originalName = $file->getClientOriginalName();
        $extension = $file->getClientOriginalExtension();
        $filename = uniqid() . '.' . $extension;


        // Determine material type based on extension
        $type = match(strtolower($extension)) {
            'jpg', 'jpeg', 'png' => MaterialTypeEnum::IMAGE,
            'mp4' => MaterialTypeEnum::VIDEO,
            default => MaterialTypeEnum::DOCUMENT
        };

        // For media materials, ensure only images and videos are processed
        if ($group === MaterialGroupEnum::MEDIA && $type === MaterialTypeEnum::DOCUMENT) {
            throw new \InvalidArgumentException('Media materials can only be images or videos');
        }

        $listing->materials()->create([
            'original_filename' => $originalName,
            'filename' => $filename,
            'type' => $type,
            'group' => $group,
        ]);
    }
}
