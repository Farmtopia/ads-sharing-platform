<?php

namespace App\Helpers;

use App\Models\User;

class UserHelper
{
    public static function userResponse(User $user)
    {
        return [
            'id' => $user->id,
            'firstname' => $user->firstname,
            'lastname' => $user->lastname,
            'email' => $user->email,
            'phone' => $user->phone,
            'profession' => $user->profession,
            'country_id' => $user->country_id,
            'region' => $user->region,
            'zip_code' => $user->zip_code,
            'address_road' => $user->address_road,
            'address_number' => $user->address_number,
            'social' => json_decode($user->social, true),
            'profile_photo' => $user->profile_photo,
        ];
    }

}
