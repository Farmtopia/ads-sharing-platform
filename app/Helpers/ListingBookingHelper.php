<?php

namespace App\Helpers;

class ListingBookingHelper
{
    public static function listingBookingsCollectionResponse($listingBookings)
    {
        return $listingBookings->map(fn($booking) => self::singleListingBooking($booking));
    }

    public static function listingBookingResponse($listingBooking)
    {
        return [
            ...self::singleListingBooking($listingBooking),
            'listing_id' => $listingBooking->listing_id,
        ];
    }

    public static function singleListingBooking($listingBooking)
    {
        return [
            'id' => $listingBooking->id,
            'start_date' => $listingBooking->start_date,
            'end_date' => $listingBooking->end_date,
            'booker' => UserHelper::userResponse($listingBooking->booker),
        ];
    }
}
