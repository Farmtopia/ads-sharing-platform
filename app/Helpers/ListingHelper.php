<?php

namespace App\Helpers;

use App\Enums\FieldTypeEnum;
use App\Enums\MaterialGroupEnum;

class ListingHelper
{

    public static function listingCollectionResponse($listings)
    {
        return $listings->map(function ($listing) {
            return self::listingResponse($listing);
        });
    }

    public static function listingResponse($listing)
    {
        $location = json_decode($listing->location, true);
        return [
            'id' =>                             $listing->id,
            'equipment_subcategory_id' =>       $listing->equipment_subcategory_id,
            'equipment_category_id' =>          $listing->equipment_subcategory->equipment_category_id,
            'listing_description' =>            $listing->listing_description,
            'manufacturer_id' =>                $listing->manufacturer_id,
            'equipment_model_id' =>             $listing->equipment_model_id,
            'equipment_model_name' =>           $listing->equipment_model_name,
            'min_price' =>                      $listing->min_price,
            'max_price'=>                       $listing->max_price,
            'has_labour' =>                     !($listing->has_labour === 0),
            'labour_min_price'=>                $listing->labour_min_price,
            'labour_max_price' =>               $listing->labour_max_price,
            'labour_scope' =>                   $listing->labour_scope,
            'labour_experience' =>              $listing->labour_experience,
            'labour_special_requirements' =>    $listing->labour_special_requirements,
            'has_delivery_service' =>           !($listing->has_delivery_service === 0),
            'max_delivery_distance' =>          $listing->max_delivery_distance,
            'max_delivery_distance_unit' =>     $listing->max_delivery_distance_unit,
            'delivery_fee' =>                   $listing->delivery_fee,
            'delivery_fee_unit' =>              $listing->delivery_fee_unit,
            'delivery_timeframe_description' => $listing->delivery_timeframe_description,
            'include_setup' =>                  !($listing->include_setup === 0),
            'has_pick_up_point' =>              !($listing->has_pick_up_point === 0),
            'pick_up_country_id' =>             $listing->pick_up_country_id,
            'pick_up_region' =>                 $listing->pick_up_region,
            'pick_up_address_road' =>           $listing->pick_up_address_road,
            'pick_up_address_number' =>         $listing->pick_up_address_number,
            'pick_up_zip_code' =>               $listing->pick_up_zip_code,
            'pickup_timeframe_description' =>   $listing->pickup_timeframe_description,
            'lat' =>                            $location['lat'],
            'lng' =>                            $location['lon'],
            'max_distance_booking' =>           $listing->max_distance_booking,
            'max_distance_booking_unit' =>      $listing->max_distance_booking_unit,
            'is_license_required' =>            !($listing->is_license_required === 0),
            'license_type' =>                   $listing->license_type,
            'renter_responsibilities' =>        $listing->renter_responsibilities,
            'special_requirements' =>           $listing->special_requirements,
            'technical_characteristics' =>      self::technicalCharacteristicsToArray($listing->technical_characteristics),
            'media' =>                          $listing->media,
            'support_materials' =>              $listing->support_material,
            'user' =>                           UserHelper::userResponse($listing->user),
            'bookings' =>                       ListingBookingHelper::listingBookingsCollectionResponse($listing->listing_bookings),
        ];
    }

    private static function technicalCharacteristicsToArray($technical_characteristics)
    {
        return [
            'weight' =>                 $technical_characteristics['weight'],
            'power' =>                  $technical_characteristics['power'],
            'working_width' =>          $technical_characteristics['working_width'],
            'number_of_cylinders' =>    FieldHelper::findByValue(FieldHelper::number_of_cylinders(), $technical_characteristics['number_of_cylinders']),
            'displacement' =>           $technical_characteristics['displacement'],
            'fuel_type' =>              FieldHelper::findByValue(FieldHelper::fuel_types(), $technical_characteristics['fuel_type']),
            'power_supply' =>           FieldHelper::findByValue(FieldHelper::power_supply(), $technical_characteristics['power_supply']),
            'number_of_furrows' =>      $technical_characteristics['number_of_furrows'],
            'is_fixed_or_mounted' =>    FieldHelper::findByValue(FieldHelper::attachment_types(), $technical_characteristics['is_fixed_or_mounting']),
            'measurement_parameters' => FieldHelper::findByValues(FieldHelper::measurement_parameters(), json_decode($technical_characteristics['measurement_parameters'])),
            'flight_time' =>            $technical_characteristics['flight_time'],
            'max_range' =>              $technical_characteristics['max_range'],
            'max_altitude' =>           $technical_characteristics['max_altitude'],
            'sensors_accompanied' =>    $technical_characteristics['sensors_accompanied'],
            'accuracy' =>               $technical_characteristics['accuracy'],
            'correction_services' =>    FieldHelper::findByValues(FieldHelper::correction_services(), json_decode($technical_characteristics['correction_services'])),
            'title' =>                  $technical_characteristics['title'],
            'description' =>            $technical_characteristics['description'],
            'author' =>                 $technical_characteristics['author'],
            'license' =>                FieldHelper::findByValue(FieldHelper::licenses(), $technical_characteristics['license']),
            'publication_date' =>       $technical_characteristics['publication_date'],
            'external_data_source' =>   $technical_characteristics['external_data_source'],
        ];
    }

}
