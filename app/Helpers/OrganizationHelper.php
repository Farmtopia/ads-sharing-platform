<?php

namespace App\Helpers;

use App\Models\Organization;

class OrganizationHelper
{
    public static function organizationResponse(Organization $organization)
    {
        return [
            'id' => $organization->id,
            'name' => $organization->name,
            'description' => $organization->description,
            'email' => $organization->email,
            'phone' => $organization->phone,
            'country_id' => $organization->country_id,
            'region' => $organization->region,
            'zip_code' => $organization->zip_code,
            'address_road' => $organization->address_road,
            'address_number' => $organization->address_number,
            'social' => json_decode($organization->social, true),
            'website' => $organization->website,
            'cover_photo' => $organization->cover_photo
        ];
    }
}
