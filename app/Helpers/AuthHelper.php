<?php

namespace App\Helpers;

use App\Config\AppConfig;
use App\Models\PasswordResetToken;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Symfony\Component\HttpFoundation\Response;

class AuthHelper
{
    public static function storeUser($data)
    {
        $user = User::where('email', $data['email'])->first();
        if ($user) {
            return response()->json([
                'message' => 'User already exists',
            ], Response::HTTP_BAD_REQUEST);
        }
        return User::create($data);
    }

    public static function authenticateUser($data)
    {
        return Auth::attempt($data) ? Auth::user() : null;
    }

    public static function getUserVerificationURL(User $user):string
    {
        return config('app.frontend.url') . '/verify-email/' . $user->id . '/' . self::createUserVerificationHash($user);
    }

    public static function createUserVerificationHash(User $user):string
    {
        return sha1($user->getEmailForVerification());
    }

    public static function getUserPasswordResetURL(User $user): string
    {
        PasswordResetToken::where('email', $user->email)->delete();

        $passwordResetToken = PasswordResetToken::create([
            'email' => $user->email,
            'token' => self::createUserPasswordResetToken($user),
            'created_at' => now(),
        ]);

        return AppConfig::frontURL() . '/reset-password/' . '?token=' . $passwordResetToken->token . '&email='. Crypt::encryptString($user->email);
    }

    public static function createUserPasswordResetToken($user): string
    {
        return sha1($user->id.' '.$user->email);
    }
}
