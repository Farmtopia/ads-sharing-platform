<?php

namespace App\Helpers;

class FieldHelper
{
    public static function number_of_cylinders()
    {
        return [
            [
                'label' => '2',
                'value'=> 1
            ],
            [
                'label' => '3',
                'value'=> 2
            ],
            [
                'label' => '4',
                'value'=> 3
            ],
            [
                'label' => '6',
                'value'=> 4
            ],
            [
                'label' => '8',
                'value'=> 5
            ],
        ];
    }

    public static function fuel_types()
    {
        return [
            [
                'label' => 'Diesel',
                'value'=> 1
            ],
            [
                'label' => 'Gasoline',
                'value'=> 2
            ],
            [
                'label' => 'Bio-diesel',
                'value'=> 3
            ],
            [
                'label' => 'Compressed Natural Gas (CNG)',
                'value'=> 4
            ],
            [
                'label' => 'Liquefied Petroleum Gas (LPG)',
                'value'=> 5
            ],
            [
                'label' => 'Electricity',
                'value'=> 6
            ],
        ];
    }

    public static function power_supply()
    {
        return [
            [
                'label' => 'Power-take-off',
                'value'=> 1
            ],
            [
                'label' => 'Hydraulic pressure',
                'value'=> 2
            ],
            [
                'label' => 'Electrical power',
                'value'=> 3
            ],
            [
                'label' => 'Fuel consumption',
                'value'=> 4
            ],
            [
                'label' => 'Direct mechanical power',
                'value'=> 5
            ],
            [
                'label' => 'Compressed air',
                'value'=> 6
            ],
        ];
    }

    public static function attachment_types()
    {
        return [
            [
                'label' => 'Fixed',
                'value'=> 1
            ],
            [
                'label' => 'Machine-mounted',
                'value'=> 2
            ]
        ];
    }

    public static function measurement_parameters()
    {
        return [
            [
                'label' => 'Electrical conductivity',
                'value'=> 1
            ],[
                'label' => 'pH',
                'value'=> 2
            ],[
                'label' => 'Air Humidity',
                'value'=> 3
            ],[
                'label' => 'CO2',
                'value'=> 4
            ],[
                'label' => 'Wind Direction',
                'value'=> 5
            ],[
                'label' => 'Turbidity',
                'value'=> 6
            ],[
                'label' => 'Air Temperature',
                'value'=> 7
            ],[
                'label' => 'Motion',
                'value'=> 8
            ],[
                'label' => 'Colour',
                'value'=> 9
            ],[
                'label' => 'Soil Temperature',
                'value'=> 10
            ],[
                'label' => 'Wind Speed',
                'value'=> 11
            ],[
                'label' => 'Light',
                'value'=> 12
            ],[
                'label' => 'Solar Radiation',
                'value'=> 13
            ],[
                'label' => 'Leaf wetness',
                'value'=> 14
            ],[
                'label' => 'Soil Moisture',
                'value'=> 15
            ],[
                'label' => 'Pressure',
                'value'=> 16
            ],[
                'label' => 'Rainfall',
                'value'=> 17
            ],
        ];
    }

    public static function correction_services()
    {
        return [
            [
                'label' => 'RTK',
                'value'=> 1
            ],
            [
                'label' => 'DGPS',
                'value'=> 2
            ],
            [
                'label' => 'WAAS',
                'value'=> 3
            ],
            [
                'label' => 'WBAS',
                'value'=> 4
            ],
            [
                'label' => 'PKK',
                'value'=> 5
            ],
            [
                'label' => 'PPP',
                'value'=> 6
            ]
        ];
    }

    public static function licenses()
    {
        return [
            [
                'label' => 'Open',
                'value'=> 1
            ],
            [
                'label' => 'Licensed',
                'value'=> 2
            ],
        ];
    }

    public static function findByValue(array $array, int|null $value): array|null
    {
        if ($value === null) {
            return null;
        }
        $filtered = array_filter($array, fn($item) => $item['value'] === $value);
        return reset($filtered) ?: null;
    }

    public static function findByValues(array $array, array|null $searchValues): array|null {
        if ($searchValues === null) {
            return null;
        }
        $arr = array_values(array_filter($array, fn($item) => in_array($item['value'], $searchValues)));
        if (count($arr) === 0) {
            return null;
        }
        return $arr;
    }
}
