<?php

namespace App\Helpers;

use App\Enums\SettingTypesEnum;

class SettingHelper
{
    public static function get(string $settingType):string
    {
        return \App\Models\Setting::where('key', $settingType)->first()->value;
    }

    public static function head_scripts():string
    {
        return self::get(SettingTypesEnum::HEAD_SCRIPTS);
    }

    public static function footer_scripts():string
    {
        return self::get(SettingTypesEnum::FOOTER_SCRIPTS);
    }

    public static function privacy_policy():string
    {
        return self::get(SettingTypesEnum::PRIVACY_POLICY);
    }
    public static function terms_and_conditions():string
    {
        return self::get(SettingTypesEnum::TERMS_AND_CONDITIONS);
    }

}
