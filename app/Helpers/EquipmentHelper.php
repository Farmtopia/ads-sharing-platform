<?php

namespace App\Helpers;

class EquipmentHelper
{
    public static function equipmentOptions($items)
    {
        return $items->map(function ($item) {
            return [
                'value' => $item->id,
                'label' => $item->name,
            ];
        });
    }
}
