<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class GeocodingService
{
    public function getCoordinates(string $addressNumber, string $addressRoad, string $city, string $postalCode, string $country): array
    {
        try {
            $searchQuery = implode(', ', [
                $addressRoad . ' ' . $addressNumber,
                $city,
                $postalCode
            ]);

            $response = Http::withHeaders([
                'User-Agent' => config('app.name') . ' Geocoding Service'
            ])->get('https://nominatim.openstreetmap.org/search', [
                'q' => $searchQuery,
                'format' => 'geocodejson'
            ]);

            if ($response->successful()) {
                $data = $response->json();
                if (!empty($data['features'])) {
                    // Find the closest match by postal code
                    $closestMatch = null;
                    $smallestDiff = PHP_FLOAT_MAX;

                    foreach ($data['features'] as $feature) {
                        // Extract postal code from the label
                        if (preg_match('/\b(\d{5}|\d{3}\s?\d{2})\b/', $feature['properties']['geocoding']['label'], $matches)) {
                            $featurePostalCode = str_replace(' ', '', $matches[1]);
                            $requestedPostalCode = str_replace(' ', '', $postalCode);

                            // Calculate difference
                            $diff = abs(intval($featurePostalCode) - intval($requestedPostalCode));

                            if ($diff < $smallestDiff) {
                                $smallestDiff = $diff;
                                $closestMatch = $feature;
                            }
                        }
                    }

                    if ($closestMatch) {
                        return [
                            'lon' => (float) $closestMatch['geometry']['coordinates'][0],
                            'lat' => (float) $closestMatch['geometry']['coordinates'][1]
                        ];
                    }
                }
            }

            Log::warning('No matching location found', [
                'query' => $searchQuery,
                'response' => $response->json()
            ]);

            return ['lat' => null, 'lon' => null];

        } catch (\Exception $e) {
            Log::error('Geocoding error', [
                'message' => $e->getMessage(),
                'trace' => $e->getTraceAsString()
            ]);
            return ['lat' => null, 'lon' => null];
        }
    }
}
