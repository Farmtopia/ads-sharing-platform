<?php

namespace App\Http\Middleware;

use Closure;

class SetLocale
{
    public function handle($request, Closure $next)
    {
        $locale = session('locale', config('app.locale')); // Default to app locale if session not set
        app()->setLocale($locale);

        return $next($request);
    }
}
