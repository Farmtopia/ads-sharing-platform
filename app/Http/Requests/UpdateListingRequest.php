<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
class UpdateListingRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    protected function prepareForValidation()
    {
        // Convert string boolean values to actual booleans
        $this->merge([
            'has_labour' => filter_var($this->has_labour, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
            'has_delivery_service' => filter_var($this->has_delivery_service, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
            'include_setup' => filter_var($this->include_setup, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
            'has_pick_up_point' => filter_var($this->has_pick_up_point, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE),
        ]);

        // Convert removed_materials to array if it's a string
        if ($this->has('removed_materials') && is_string($this->input('removed_materials'))) {
            $this->merge([
                'removed_materials' => json_decode($this->input('removed_materials'), true)
            ]);
        }
    }

    public function rules(): array
    {
        return [
            'equipment_subcategory_id' =>       ['required', 'exists:equipment_subcategories,id'],
            'manufacturer_id' =>                ['nullable', 'exists:manufacturers,id'],
            'equipment_model_id' =>             ['nullable', 'exists:equipment_models,id'],
            'equipment_model_name' =>           ['required', 'string'],
            'listing_description' =>            ['required', 'string'],
            'min_price' =>                      ['required', 'numeric', 'min:0'],
            'max_price' =>                      ['required', 'numeric', 'gt:min_price'],
            'has_labour' =>                     ['required', 'boolean'],
            'labour_min_price' =>               ['required_if:has_labour,true', 'nullable', 'numeric', 'min:0'],
            'labour_max_price' =>               ['required_if:has_labour,true', 'nullable', 'numeric', 'min:0'],
            'labour_scope' =>                   ['nullable', 'string'],
            'labour_experience' =>              ['nullable', 'string'],
            'labour_special_requirements' =>    ['nullable', 'string'],
            'has_delivery_service' =>           ['nullable', 'boolean'],
            'max_delivery_distance' =>          ['nullable', 'numeric', 'min:0'],
            'max_delivery_distance_unit' =>     ['nullable', 'string'],
            'delivery_fee' =>                   ['nullable', 'numeric', 'min:0'],
            'delivery_fee_unit' =>              ['nullable', 'string'],
            'delivery_timeframe_description' => ['nullable', 'string'],
            'include_setup' =>                  ['nullable', 'boolean'],
            'has_pick_up_point' =>              ['nullable', 'boolean'],
            'pick_up_country_id' =>             ['required_if:has_pick_up_point,true', 'nullable', 'exists:countries,id'],
            'pick_up_region' =>                 ['required_if:has_pick_up_point,true', 'nullable', 'string'],
            'pick_up_address_road' =>           ['required_if:has_pick_up_point,true', 'nullable', 'string'],
            'pick_up_address_number' =>         ['required_if:has_pick_up_point,true', 'nullable', 'string'],
            'pick_up_zip_code' =>               ['required_if:has_pick_up_point,true', 'nullable', 'string'],
            'pickup_timeframe_description' =>   ['nullable', 'string'],
            'max_distance_booking' =>           ['nullable', 'numeric', 'min:0'],
            'max_distance_booking_unit' =>      ['nullable', 'string'],
            'is_license_required' =>            ['nullable', 'boolean'],
            'license_type' =>                   ['nullable', 'string'],
            'renter_responsibilities' =>        ['nullable', 'string'],
            'special_requirements' =>           ['nullable', 'string'],
            'weight' =>                         ['nullable', 'numeric'],
            'power' =>                          ['nullable', 'numeric'],
            'working_width' =>                  ['nullable', 'numeric'],
            'number_of_cylinders'=>             ['nullable', 'integer'],
            'displacement' =>                   ['nullable', 'integer'],
            'fuel_type' =>                      ['nullable', 'integer'],
            'power_supply' =>                   ['nullable', 'integer'],
            'number_of_furrows' =>              ['nullable', 'integer'],
            'is_fixed_or_mounted' =>            ['nullable', 'integer'],
            'measurement_parameters' =>         ['nullable', 'array'],
            'flight_time' =>                    ['nullable', 'integer'],
            'max_range' =>                      ['nullable', 'integer'],
            'max_altitude' =>                   ['nullable', 'integer'],
            'sensors_accompanied' =>            ['nullable', 'string'],
            'accuracy' =>                       ['nullable', 'integer'],
            'correction_services' =>            ['nullable', 'array'],
            'title'=>                           ['nullable', 'string'],
            'description' =>                    ['nullable', 'string'],
            'author' =>                         ['nullable', 'string'],
            'license' =>                        ['nullable', 'integer'],
            'publication_date' =>               ['nullable', 'string'],
            'external_data_source' =>           ['nullable', 'string'],
            'media' =>                          ['nullable', 'array'],
            'media.*' =>                        ['file', 'mimes:jpeg,png,jpg,mp4', 'max:10240'],
            'support_materials' =>              ['nullable', 'array'],
            'support_materials.*' =>            ['file', 'mimes:jpeg,png,jpg,pdf,doc,docx,mp4', 'max:10240'],
            'removed_materials' =>              ['nullable', 'array'],
        ];
    }
}
