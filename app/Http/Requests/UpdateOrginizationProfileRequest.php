<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateOrginizationProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'description' => ['string', 'max:255'],
            'cover_photo' => ['file', 'mimes:jpeg,jpg,png,webp', 'max:5192'],
            'email' => ['string', 'email', 'max:255', 'unique:organizations,email'],
            'phone' => ['string'],
            'facebook' => ['string'],
            'twitter' => ['string'],
            'instagram' => ['string'],
            'linkedin' => ['string'],
            'website' => ['string'],
            'country_id' => ['required', 'integer', 'exists:countries,id'],
            'region' => ['string'],
            'zip_code' => ['string'],
            'address_road' => ['string'],
            'address_number' => ['string'],
        ];
    }
}
