<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserProfileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'firstname' => ['required', 'string'],
            'lastname' => ['required', 'string'],
            'phone' => ['string'],
            'profession' => ['string'],
            'country_id' => ['required', 'exists:countries,id', 'numeric'],
            'region' => ['string'],
            'zip_code' => ['required', 'string'],
            'address_road' => ['required','string'],
            'address_number' => ['required','string'],
            'facebook' => ['string'],
            'twitter' => ['string'],
            'instagram' => ['string'],
            'linkedin' => ['string'],
            'profile_photo' => ['file', 'mimes:jpeg,jpg,png,webp', 'max:5192'],
        ];
    }
}
