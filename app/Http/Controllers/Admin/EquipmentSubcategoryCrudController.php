<?php

namespace App\Http\Controllers\Admin;


use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class EquipmentSubcategoryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EquipmentSubcategoryCrudController extends CrudController implements \App\Interfaces\BackpackAdminControllerInterface
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;
    use \App\Traits\BackpackControllerTrait;

    /** CHANGE THIS TO YOUR MODEL FIELDS HELPER CLASS!!! */
    const FIELDS_CLASS=\App\Fields\EquipmentSubcategoryFields::class;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\EquipmentSubcategory::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/equipment-subcategory');
        CRUD::setEntityNameStrings('equipment subcategory', 'equipment subcategories');
    }

    /**
     * Configure the Create new entity operation
     *
     * @return void
     */
    protected function createViewConfig():void
    {
    }

    /**
     * Configure the Update existing entity operation
     *
     * @return void
     */
    protected function updateViewConfig():void
    {
    }
}
