<?php

namespace App\Http\Controllers\API;

use App\Enums\MaterialGroupEnum;
use App\Helpers\ListingHelper;
use App\Helpers\MaterialHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreListingRequest;
use App\Http\Requests\UpdateListingRequest;
use App\Models\Country;
use App\Models\EquipmentCategory;
use App\Models\EquipmentSubcategory;
use App\Models\Listing;
use App\Models\TechnicalCharacteristics;
use App\Services\GeocodingService;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Listings
 *
 * APIs for listings
 */
class ListingController extends Controller
{
    /**
     * Get organization listings with filters
     *
     * Returns all listings for the authenticated user's organization by applying filters
     *
     * @authenticated
     * @header Authorization Bearer <token>
     *
     * @queryParam from_date date Indicate the search date from where the listing should be available. Example: 16-01-2025
     * @queryParam to_date date Indicate the search date to where the listing should be available. Example: 23-03-2025
     * @queryParam available_only boolean Used to retrieve only available listings into the specified date range. Example: true
     * @queryParam type_id int The equipment type id. Example: 1
     * @queryParam category_id int The equipment category id. Example:2
     * @queryParam sub_category_id int The sub-category id of the equipment. Example: 4
     * @queryParam min_price double The minimum price for listings. Example: 10.4
     * @queryParam max_price double The maximum price for listings. Example: 308.0
     *
     * @response 200 {"success": true, "message": "Retrieved organization listings."}
     * @response 404 {"success": false, "message": "User\'s organization not found."}
     */
    public function getOrganizationListings(Request $request)
    {
        // Get filters from query params
        $startDate = $request->query('from_date');
        $endDate = $request->query('to_date');
        $availableOnly = $request->query('available_only');
        $type = $request->query('type_id'); // Example filter
        $category = $request->query('category_id');
        $subCategory = $request->query('sub_category_id');
        $minPrice = $request->query('min_price');
        $maxPrice = $request->query('max_price');

        // Get user from request
        $user = $request->user();
        $organization = $user->organizations()->first();

        // Check if the user belongs to a valid organization
        if (!$organization) {
            return response()->json([
                'success' => false,
                'message' => 'User\'s organization not found.'
            ], Response::HTTP_NOT_FOUND);
        }

        // Find user ids for this organization
        $orgUserIds = $organization->users()->pluck('id')->toArray();

        // Staring query
        $query = Listing::query();
        $query->whereIn('user_id', $orgUserIds)->get();

        // Query by type, category and subcategory
        if ($subCategory === null || $subCategory < 1) {
            $subCategoryIds = EquipmentSubcategory::query();
            if ($type) {
                $categories = EquipmentCategory::where('equipment_type_id', $type)->get()->pluck('id')->toArray();
                $subCategoryIds->whereIn('equipment_category_id', $categories);
            }
            if ($category) {
                $subCategoryIds->where('equipment_category_id', $category);
            }
            $subCategoryIds = $subCategoryIds->get()->pluck('id')->toArray();

            if (count($subCategoryIds) > 0) {
                $query->whereIn('equipment_subcategory_id', $subCategoryIds);
            }
        } else {
            $query->whereIn('equipment_subcategory_id', $subCategory);
        }
        // Query by min_price
        if ($minPrice) {
            $query->where('min_price', '>=', $minPrice);
        }
        // Query by max_price
        if ($maxPrice) {
            $query->where('max_price', '<=', $maxPrice);
        }
        // Query by availability
        if ($startDate && $endDate && $availableOnly) {
            $query->whereDoesntHave('listing_bookings', function ($bookingQuery) use ($startDate, $endDate) {
                $bookingQuery->where(function ($query) use ($startDate, $endDate) {
                    $query->whereBetween('start_date', [$startDate, $endDate])
                        ->orWhereBetween('end_date', [$startDate, $endDate])
                        ->orWhere(function ($query) use ($startDate, $endDate) {
                            $query->where('start_date', '<=', $startDate)
                                ->where('end_date', '>=', $endDate);
                        });
                });
            });
        }

        $listings = $query->get();


//        $listings = Listing::with(['materials', 'user', 'country'])->whereIn('user_id', $organizationUsersIds)->get();
//
//        $formattedListings = $listings->map(function ($listing) {
//            $location = json_decode($listing->location, true);
//            $listingArray = $listing->toArray();
//
//            // Add user details
//            $user = $listing->user;
//            $listingArray['user'] = [
//                'id' => $user->id,
//                'firstname' => $user->firstname,
//                'lastname' => $user->lastname,
//                'zip_code' => $user->zip_code,
//                'address_road' => $user->address_road,
//                'address_number' => $user->address_number,
//            ];
//
//            // Format materials
//            $listingArray['materials'] = $listing->materials->map(function($material) {
//                return [
//                    'id' => $material->id,
//                    'filename' => $material->filename,
//                    'original_filename' => $material->original_filename,
//                    'type' => $material->type,
//                    'material_group' => $material->group,
//                ];
//            })->all();
//
//            // Add lon/lat from location
//            $listingArray['lon'] = $location['lon'] ?? null;
//            $listingArray['lat'] = $location['lat'] ?? null;
//
//            // Replace country_id with country name
//            $listingArray['country'] = $listing->country?->name;
//            unset($listingArray['pick_up_country_id'], $listingArray['location']);
//
//            return $listingArray;
//        });

        return response()->json([
            'success' => true,
            'message' => 'Retrieved organization listings.',
//            'organization' => [
//                'location' => $organization->location,
//                'address_road' => $organization->address_road,
//                'address_number' => $organization->address_number,
//            ],
            'listings' => ListingHelper::listingCollectionResponse($listings)
        ], Response::HTTP_OK);
    }

    /**
     * Get listing by Id
     *
     * Returns a listing with all its details if user belongs to the same organization
     *
     * @authenticated
     * @header Authorization Bearer <token>
     *
     * @urlParam listingId int required The Id of the listing. Example: 1
     * @response 200 {"success": true, "message": "Retrieved listing successfully"}
     * @response 404 {"success": false, "message": "User's organization not found."}
     * @response 404 {"success": false, "message": "Listing not found."}
     * @response 403 {"success": false, "message": "Not authorized for this listing."}
     */
    public function getById(Request $request, int $listingId)
    {
        // Get user and user's organization
        $user = $request->user();
        $organization = $user->organizations()->first();

        // Check if organization exists
        if (!$organization) {
            return response()->json([
                'success' => false,
                'message' => 'User\'s organization not found.'
            ], Response::HTTP_NOT_FOUND);
        }

        // Get organization listings
        $organizationListings = $organization->listings()->pluck('id');

        // Check if listing is included into organization's listings
        if (!in_array($listingId, $organizationListings->toArray())) {
            return response()->json([
                'success' => false,
                'message' => 'Not authorized for this listing.'
            ], Response::HTTP_FORBIDDEN);
        }

        // Retrieve listing
        $listing = Listing::find($listingId);

        // Check if listing exists
        if (!$listing) {
            return response()->json([
                'success' => false,
                'message' => 'Listing not found.'
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'success' => true,
            'message' => 'Retrieved listing successfully.',
            'listing' => ListingHelper::listingResponse($listing)
        ], Response::HTTP_OK);
    }

    /**
     * Create new listing
     *
     * Creates a new listing for the authenticated user's organization
     *
     * @authenticated
     * @header Authorization Bearer <token>
     * @header Accept application/json
     *
     * @bodyParam equipment_subcategory_id integer required The subcategory ID of the equipment. Example: 1
     * @bodyParam manufacturer_id integer nullable The manufacturer ID. Example: 1
     * @bodyParam equipment_model_id integer nullable The model name of the equipment. Example: 1
     * @bodyParam equipment_model_name string required The model name of the equipment. Example: 6115M
     * @bodyParam listing_description string required The description of the listing. Example: Kubota tractor 4222.Perfect tractor for medium-sized farms.
     * @bodyParam min_price decimal required The minimum price. Example: 100
     * @bodyParam max_price decimal required The maximum price. Example: 200
     * @bodyParam has_labour boolean required Whether labour is included. Example: true
     * @bodyParam labour_min_price number nullable The labour min price. Example: 15.5
     * @bodyParam labour_max_price number nullable The labour max price. Example: 25.5
     * @bodyParam labour_scope string nullable The scope of labour service. Example: Full operation of the tractor including plowing.
     * @bodyParam labour_experience string nullable Labour experience details. Example: 10+ years of experience
     * @bodyParam labour_special_requirements string nullable Special requirements for labour. Example: Only during daylight hours
     * @bodyParam has_delivery_service boolean required Whether delivery is available. Example: true
     * @bodyParam max_delivery_distance decimal The maximum delivery distance. Example: 50
     * @bodyParam max_delivery_distance_unit string The unit for delivery distance. Example: km
     * @bodyParam delivery_fee decimal nullable The delivery fee. Example: 25
     * @bodyParam delivery_fee_unit string nullable The unit for delivery fee. Example: EUR
     * @bodyParam delivery_timeframe_description string nullable Description of delivery timeframe. Example: Delivery Mon-Fri, 8 AM - 6 PM
     * @bodyParam include_setup boolean nullable Whether setup is included. Example: true
     * @bodyParam has_pick_up_point boolean required Whether pickup is available. Example: true
     * @bodyParam pick_up_country_id integer required if has_pick_up_point is true The country ID for pickup. Example: 1
     * @bodyParam pick_up_region string required if has_pick_up_point is true The region for pickup. Example: Attica
     * @bodyParam pick_up_address_road string required if has_pick_up_point is true The street for pickup. Example: Argous
     * @bodyParam pick_up_address_number string required if has_pick_up_point is true The street number for pickup. Example: 139
     * @bodyParam pick_up_zip_code string required if has_pick_up_point is true The ZIP code for pickup. Example: 10441
     * @bodyParam pickup_timeframe_description string nullable Pickup availability schedule. Example: Monday to Friday: 9 AM - 5 PM
     * @bodyParam max_distance_booking numeric The maximum distance/range for a booking. Example: 25
     * @bodyParam max_distance_booking_unit string The maximum booking distance unit. Example: km
     * @bodyParam is_license_required boolean nullable Whether license is required. Example: true
     * @bodyParam license_type string Type of license required. Example: Agricultural Equipment License
     * @bodyParam renter_responsibilities string nullable Renter's responsibilities. Example: Daily maintenance checks
     * @bodyParam special_requirements string nullable Special requirements. Example: Proof of insurance required
     * @bodyParam weight number nullable Weight in kg. Example: 850.5
     * @bodyParam power number nullable Power rating. Example: 120
     * @bodyParam working_width number nullable Working width. Example: 2.5
     * @bodyParam number_of_cylinders integer nullable Engine number of cylinders (value from list). Example: 2
     * @bodyParam displacement number nullable Engine displacement. Example: 4500
     * @bodyParam fuel_type integer nullable Fuel type for the equipment (value from list). Example: 3
     * @bodyParam power_supply integer nullable Power supply type for the equipment (value from list). Example: 3
     * @bodyParam number_of_furrows integer nullable Number of furrows. Example: 3
     * @bodyParam is_fixed_or_mounted integer nullable Equipment is fixed or mounted (value from list). Example: 1
     * @bodyParam measurement_parameters integer[] nullable Measurement parameters type (value from list). Example: 1
     * @bodyParam flight_time integer nullable Flight time of a drone in minutes. Example: 160
     * @bodyParam max_range integer nullable Flight range of a drone in meters. Example: 2400
     * @bodyParam max_altitude integer nullable Flight altitude of a drone in meters. Example: 1600
     * @bodyParam sensors_accompanied string nullable Sensors accompanied this drone. Example: Humidity and temperature sensors
     * @bodyParam accuracy integer nullable Equipment accuracy in meters. Example: 500
     * @bodyParam correction_services integer[] nullable Correction services provided with this technology. Example: [1, 3]
     * @bodyParam title string nullable Title of digital asset. Example: Digital asset
     * @bodyParam description string nullable Description of digital asset. Example: Digital asset description
     * @bodyParam author string nullable The author/owner of the digital asset. Example: AUA Devs
     * @bodyParam license integer nullable The license option of the digital asset. Example: 2
     * @bodyParam publication_date string nullable The publication date of the digital asset. Example: 13-03-2025
     * @bodyParam external_data_source string nullable External data source the digital asset (link, dropbox, cloud). Example: www.farmtopia-cloud.com/files/docs/doc1.pdf
     * @bodyParam media[] file[] files to upload (images, videos, etc). No-example
     * @bodyParam support_materials[] file[] files to upload (images, pdfs, etc). No-example
     *
     * @response 201 {"success": true, "message": "Listing created successfully."}
     * @response 422 {"success": false, "message": "The min price field is required."}
     * @response 500 {"success": false, "message": "Could not create listing."}
     */
    public function create(StoreListingRequest $request)
    {
        try {
            DB::beginTransaction();

            $data = $request->validated();

            // Extract technical characteristics data
            $technicalData = [
                'weight' => $data['weight'] ?? null,
                'power' => $data['power'] ?? null,
                'working_width' => $data['working_width'] ?? null,
                'number_of_cylinders'=>$data['number_of_cylinders'] ?? null,
                'displacement' => $data['displacement'] ?? null,
                'fuel_type' => $data['fuel_type'] ?? null,
                'power_supply' => $data['power_supply'] ?? null,
                'number_of_furrows' => $data['number_of_furrows'] ?? null,
                'is_fixed_or_mounted' => $data['is_fixed_or_mount'] ?? null,
                'measurement_parameters' => $data['measurement_parameters'] ? json_encode($data['measurement_parameters']) : null,
                'flight_time' => $data['flight_time'] ?? null,
                'max_range' => $data['max_range'] ?? null,
                'max_altitude' => $data['max_altitude'] ?? null,
                'sensors_accompanied' => $data['sensors_accompanied'] ?? null,
                'accuracy' => $data['accuracy'] ?? null,
                'correction_services' => $data['correction_services'] ? json_encode($data['correction_services']) : null,
                'title'=>$data['title'] ?? null,
                'description' => $data['description'] ?? null,
                'author' => $data['author'] ?? null,
                'license' => $data['license'] ?? null,
                'publication_date' => $data['publication_date'] ?? null,
                'external_data_source' => $data['external_data_source'] ?? null,
            ];

            // Remove technical fields from main data
            unset(
                $data['weight'], $data['power'], $data['working_width'], $data['number_of_cylinders'], $data['displacement'],
                $data['fuel_type'], $data['power_supply'], $data['number_of_furrows'], $data['is_fixed_or_mounted'],
                $data['measurement_parameters'], $data['flight_time'], $data['max_range'], $data['max_altitude'],
                $data['sensors_accompanied'], $data['accuracy'], $data['correction_services'], $data['title'],
                $data['description'], $data['author'], $data['license'], $data['publication_date'], $data['external_data_source']
            );

            // Set Listing location
            if ($data['has_pick_up_point']) {
                $geocodingService = new GeocodingService();
                $coordinates = $geocodingService->getCoordinates(
                    $data['pick_up_address_number'],
                    $data['pick_up_address_road'],
                    $data['pick_up_region'],
                    $data['pick_up_zip_code'],
                    Country::find($data['pick_up_country_id'])->name
                );

                $data['location'] = json_encode($coordinates);
            } else {
                $user = $request->user();
                $geocodingService = new GeocodingService();
                $coordinates = $geocodingService->getCoordinates(
                    $user->address_number,
                    $user->address_road,
                    $user->region,
                    $user->zip_code,
                    $user->country->name
                );

                $data['location'] = json_encode($coordinates);
            }


            // Create listing
            $listing = Listing::create([
                'user_id' => $request->user()->id,
                ...$data
            ]);

            // Create technical characteristics
            $technicalCharacteristics = TechnicalCharacteristics::create([
                'listing_id' => $listing->id,
                ...$technicalData
            ]);

            // Handle media materials
            if ($request->hasFile('media')) {
                $files = $request->file('media');
                foreach ($files as $file) {
                    MaterialHelper::handleMaterialUpload($file, $listing, MaterialGroupEnum::MEDIA);
                }
            }

            // Handle support materials
            if ($request->hasFile('support_materials')) {
                $files = $request->file('support_materials');
                foreach ($files as $file) {
                    MaterialHelper::handleMaterialUpload($file, $listing, MaterialGroupEnum::SUPPORT_MATERIAL);
                }
            }

            DB::commit();

            // Load relationships for response
            $listing->load(['materials']);

            // Decode the JSON location into an array for the response
            $responseData = $listing->toArray();
            if ($responseData['location']) {
                $responseData['location'] = json_decode($responseData['location'], true);
            }
            $responseData['materials'] = $listing->materials->map(function($material) {
                return [
                    'id' => $material->id,
                    'filename' => $material->filename,
                    'original_filename' => $material->original_filename,
                    'type' => $material->type,
                    'material_group' => $material->group,
                ];
            })->all();
            $responseData['technicalCharacteristics'] = $technicalCharacteristics;

            return response()->json([
                'success' => true,
                'message' => 'Listing created successfully.',
                'listing' => $responseData
            ], Response::HTTP_CREATED);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => 'Could not create listing.',
                'error' => $e->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Update listing
     *
     * Updates an existing listing
     *
     * @authenticated
     * @header Authorization Bearer <token>
     * @header Accept application/json
     * @header Content-type multipart/form-data
     *
     * @urlParam listingId integer required The ID of the listing. Example: 1
     * @bodyParam _method string required Set this to "PUT" for method spoofing. Example: PUT
     * @bodyParam equipment_subcategory_id integer required The subcategory ID of the equipment. Example: 1
     * @bodyParam manufacturer_id integer nullable The manufacturer ID. Example: 1
     * @bodyParam equipment_model_id integer nullable The model name of the equipment. Example: 1
     * @bodyParam equipment_model_name string required The model name of the equipment. Example: 6115M
     * @bodyParam listing_description string required The description of the listing. Example: Kubota tractor 4222.Perfect tractor for medium-sized farms.
     * @bodyParam min_price decimal required The minimum price. Example: 100
     * @bodyParam max_price decimal required The maximum price. Example: 200
     * @bodyParam has_labour boolean required Whether labour is included. Example: true
     * @bodyParam labour_min_price number nullable The labour min price. Example: 15.5
     * @bodyParam labour_max_price number nullable The labour max price. Example: 25.5
     * @bodyParam labour_scope string nullable The scope of labour service. Example: Full operation of the tractor including plowing.
     * @bodyParam labour_experience string nullable Labour experience details. Example: 10+ years of experience
     * @bodyParam labour_special_requirements string nullable Special requirements for labour. Example: Only during daylight hours
     * @bodyParam has_delivery_service boolean required Whether delivery is available. Example: true
     * @bodyParam max_delivery_distance decimal The maximum delivery distance. Example: 50
     * @bodyParam max_delivery_distance_unit string The unit for delivery distance. Example: km
     * @bodyParam delivery_fee decimal nullable The delivery fee. Example: 25
     * @bodyParam delivery_fee_unit string nullable The unit for delivery fee. Example: EUR
     * @bodyParam delivery_timeframe_description string nullable Description of delivery timeframe. Example: Delivery Mon-Fri, 8 AM - 6 PM
     * @bodyParam include_setup boolean nullable Whether setup is included. Example: true
     * @bodyParam has_pick_up_point boolean required Whether pickup is available. Example: true
     * @bodyParam pick_up_country_id integer required if has_pick_up_point is true The country ID for pickup. Example: 1
     * @bodyParam pick_up_region string required if has_pick_up_point is true The region for pickup. Example: Attica
     * @bodyParam pick_up_address_road string required if has_pick_up_point is true The street for pickup. Example: Argous
     * @bodyParam pick_up_address_number string required if has_pick_up_point is true The street number for pickup. Example: 139
     * @bodyParam pick_up_zip_code string required if has_pick_up_point is true The ZIP code for pickup. Example: 10441
     * @bodyParam pickup_timeframe_description string nullable Pickup availability schedule. Example: Monday to Friday: 9 AM - 5 PM
     * @bodyParam max_distance_booking numeric The maximum distance/range for a booking. Example: 25
     * @bodyParam max_distance_booking_unit string The maximum booking distance unit. Example: km
     * @bodyParam is_license_required boolean nullable Whether license is required. Example: true
     * @bodyParam license_type string Type of license required. Example: Agricultural Equipment License
     * @bodyParam renter_responsibilities string nullable Renter's responsibilities. Example: Daily maintenance checks
     * @bodyParam special_requirements string nullable Special requirements. Example: Proof of insurance required
     * @bodyParam weight number nullable Weight in kg. Example: 850.5
     * @bodyParam power number nullable Power rating. Example: 120
     * @bodyParam working_width number nullable Working width. Example: 2.5
     * @bodyParam number_of_cylinders integer nullable Engine number of cylinders (value from list). Example: 2
     * @bodyParam displacement number nullable Engine displacement. Example: 4500
     * @bodyParam fuel_type integer nullable Fuel type for the equipment (value from list). Example: 3
     * @bodyParam power_supply integer nullable Power supply type for the equipment (value from list). Example: 3
     * @bodyParam number_of_furrows integer nullable Number of furrows. Example: 3
     * @bodyParam is_fixed_or_mounted integer nullable Equipment is fixed or mounted (value from list). Example: 1
     * @bodyParam measurement_parameters integer[] nullable Measurement parameters type (value from list). Example: 1
     * @bodyParam flight_time integer nullable Flight time of a drone in minutes. Example: 160
     * @bodyParam max_range integer nullable Flight range of a drone in meters. Example: 2400
     * @bodyParam max_altitude integer nullable Flight altitude of a drone in meters. Example: 1600
     * @bodyParam sensors_accompanied string nullable Sensors accompanied this drone. Example: Humidity and temperature sensors
     * @bodyParam accuracy integer nullable Equipment accuracy in meters. Example: 500
     * @bodyParam correction_services integer[] nullable Correction services provided with this technology. Example: [1, 3]
     * @bodyParam title string nullable Title of digital asset. Example: Digital asset
     * @bodyParam description string nullable Description of digital asset. Example: Digital asset description
     * @bodyParam author string nullable The author/owner of the digital asset. Example: AUA Devs
     * @bodyParam license integer nullable The license option of the digital asset. Example: 2
     * @bodyParam publication_date string nullable The publication date of the digital asset. Example: 13-03-2025
     * @bodyParam external_data_source string nullable External data source the digital asset (link, dropbox, cloud). Example: www.farmtopia-cloud.com/files/docs/doc1.pdf
     * @bodyParam media[] file[] files to upload (images, videos, etc). No-example
     * @bodyParam support_materials[] file[] files to upload (images, pdfs, etc). No-example
     * @bodyParam removed_materials[] integer[] IDs of materials to remove. Example: [1,2]
     *
     * @response 200 {"success": true, "message": "Listing updated successfully."}
     * @response 403 {"success": false, "message": "Unauthorized to update listing."}
     * @response 404 {"success": false, "message": "Listing not found."}
     * @response 422 {"success": false, "message": "The min price must be less than max price."}
     * @response 500 {"success": false, "message": "Could not update listing."}
     */
    public function update(UpdateListingRequest $request, int $listingId)
    {
        try {
            DB::beginTransaction();

            // Get the user
            $user = $request->user();

            // Get user's listings
            $userListings = $user->listings->pluck('id')->toArray();

            // Check if user has access to listing
            if (!in_array($listingId, $userListings)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Unauthorized to update listing.'
                ], Response::HTTP_UNAUTHORIZED);
            }

            // Check if listing exists
            $listing = Listing::find($listingId);
            if (!$listing) {
                return response()->json([
                    'success' => false,
                    'message' => 'Listing not found.',
                ], Response::HTTP_NOT_FOUND);
            }

            $data = $request->validated();

            // Check if any address fields are being updated
            $addressFieldsUpdated = isset($data['pick_up_address_number']) ||
                isset($data['pick_up_address_road']) ||
                isset($data['pick_up_region']) ||
                isset($data['pick_up_zip_code']) ||
                isset($data['pick_up_country_id']);

            if ($addressFieldsUpdated) {
                $geocodingService = new GeocodingService();
                $coordinates = $geocodingService->getCoordinates(
                    $data['pick_up_address_number'] ?? $listing->pick_up_address_number,
                    $data['pick_up_address_road'] ?? $listing->pick_up_address_road,
                    $data['pick_up_region'] ?? $listing->pick_up_region,
                    $data['pick_up_zip_code'] ?? $listing->pick_up_zip_code,
                    Country::find($data['pick_up_country_id'] ?? $listing->pick_up_country_id)->name
                );

                $data['location'] = $coordinates;
            }

            // Handle material removals
            if (isset($data['removed_materials'])) {
                $materialsToRemove = $listing->materials()
                    ->whereIn('id', $data['removed_materials'])
                    ->get();

                foreach ($materialsToRemove as $material) {
                    Storage::disk('material')->delete($material->filename);
                    $material->delete();
                }
                unset($data['removed_materials']);
            }

            // Handle new support materials
            if ($request->hasFile('support_materials')) {
                $files = $request->file('support_materials');
                foreach ($files as $file) {
                    MaterialHelper::handleMaterialUpload($file, $listing, MaterialGroupEnum::SUPPORT_MATERIAL);
                }
            }

            // Handle new media materials
            if ($request->hasFile('media_materials')) {
                $files = $request->file('media_materials');
                foreach ($files as $file) {
                    MaterialHelper::handleMaterialUpload($file, $listing, MaterialGroupEnum::MEDIA);
                }
            }

            // Extract technical characteristics data
            $technicalData = [
                'weight' => $data['weight'] ?? $listing->weight,
                'power' => $data['power'] ?? $listing->power,
                'working_width' => $data['working_width'] ?? $listing->working_width,
                'number_of_cylinders'=>$data['number_of_cylinders'] ?? $listing->number_of_cylinders,
                'displacement' => $data['displacement'] ?? $listing->displacement,
                'fuel_type' => $data['fuel_type'] ?? $listing->fuel_type,
                'power_supply' => $data['power_supply'] ?? $listing->power_supply,
                'number_of_furrows' => $data['number_of_furrows'] ?? $listing->number_of_furrows,
                'is_fixed_or_mounted' => $data['is_fixed_or_mount'] ?? $listing->is_fixed_or_mount,
                'measurement_parameters' => $data['measurement_parameters'] ? json_encode($data['measurement_parameters']) : $listing->measurement_parameters,
                'flight_time' => $data['flight_time'] ?? $listing->flight_time,
                'max_range' => $data['max_range'] ?? $listing->max_range,
                'max_altitude' => $data['max_altitude'] ?? $listing->max_altitude,
                'sensors_accompanied' => $data['sensors_accompanied'] ?? $listing->sensors_accompanied,
                'accuracy' => $data['accuracy'] ?? $listing->accuracy,
                'correction_services' => $data['correction_services'] ? json_encode($data['correction_services']) : $listing->correction_services,
                'title'=>$data['title'] ?? $listing->title,
                'description' => $data['description'] ?? $listing->description,
                'author' => $data['author'] ?? $listing->author,
                'license' => $data['license'] ?? $listing->license,
                'publication_date' => $data['publication_date'] ?? $listing->publication_date,
                'external_data_source' => $data['external_data_source'] ?? $listing->external_data_source,
            ];

            $listing->technical_characteristics->update($technicalData);

            unset(
                $data['weight'], $data['power'], $data['working_width'], $data['number_of_cylinders'], $data['displacement'],
                $data['fuel_type'], $data['power_supply'], $data['number_of_furrows'], $data['is_fixed_or_mounted'],
                $data['measurement_parameters'], $data['flight_time'], $data['max_range'], $data['max_altitude'],
                $data['sensors_accompanied'], $data['accuracy'], $data['correction_services'], $data['title'],
                $data['description'], $data['author'], $data['license'], $data['publication_date'], $data['external_data_source']
            );

            $listing->update($data);

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Listing updated successfully.',
                'listing' => ListingHelper::listingResponse($listing)
            ], Response::HTTP_OK);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => 'Could not update listing.',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get User listings
     *
     * Retrieves all the listings of the request user
     *
     * @authenticated
     * @header Authorization Bearer <token>
     * @header Accept application/json
     * @response 200 {"success": true, "message": "User's listings retrieved successfully."}
     * @response 404 {"success": false, "message": "No listings found."}
     */
    public function getUserListings(Request $request)
    {
        // Retrieve user from request
        $user = $request->user();

        // Get his listings
        $listings = $user->listings()->with('technical_characteristics')->get();

        // If Listing Collection is empty return NOT_FOUND
        if (count($listings) < 1) {
            return response()->json([
                'success' => false,
                'message' => 'No listings found.',
            ], Response::HTTP_NOT_FOUND);
        }

        // Send Mapped successful response
        return response()->json([
            'success' => true,
            'message' => 'User\'s listings retrieved successfully.',
            'listings' => ListingHelper::listingCollectionResponse($listings),
        ]);
    }
}
