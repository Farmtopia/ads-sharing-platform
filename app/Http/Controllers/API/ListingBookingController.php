<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreListingBookingRequest;
use App\Models\ListingBooking;
use App\Models\Listing;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Listing Bookings
 *
 * APIs for managing listing bookings
 */
class ListingBookingController extends Controller
{
    /**
     * Create booking
     *
     * Creates a new booking for a listing
     *
     * @authenticated
     * @header Authorization Bearer <token>
     * @header Accept application/json
     * @header Content-type application/json
     *
     * @urlParam listingId integer required The ID of the listing to book. Example: 1
     * @bodyParam start_date date required Start date of booking. Example: 2025-03-01
     * @bodyParam end_date date required End date of booking. Example: 2025-03-05
     *
     * @response 201 {"success": true, "message": "Booking created successfully."}
     * @response 404 {"success": false, "message": "Listing not found."}
     * @response 422 {"success": false, "message": "The selected dates are not available."}
     * @response 500 {"success": false, "message": "Could not create booking."}
     */
    public function create(StoreListingBookingRequest $request, int $listingId)
    {
        try {
            $listing = Listing::find($listingId);

            if (!$listing) {
                return response()->json([
                    'success' => false,
                    'message' => 'Listing not found.',
                ], Response::HTTP_NOT_FOUND);
            }

            $data = $request->validated();
            $user = $request->user();

            // Check if dates are available
            $existingBookings = ListingBooking::where('listing_id', $listingId)
                ->where(function($query) use ($data) {
                    $query->whereBetween('start_date', [$data['start_date'], $data['end_date']])
                        ->orWhereBetween('end_date', [$data['start_date'], $data['end_date']])
                        ->orWhere(function($q) use ($data) {
                            $q->where('start_date', '<=', $data['start_date'])
                                ->where('end_date', '>=', $data['end_date']);
                        });
                })->exists();

            if ($existingBookings) {
                return response()->json([
                    'success' => false,
                    'message' => 'The selected dates are not available.',
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            $booking = ListingBooking::create([
                'listing_id' => $listingId,
                'booker_id' => $user->id,
                'start_date' => $data['start_date'],
                'end_date' => $data['end_date']
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Booking created successfully.',
                'booking' => $booking
            ], Response::HTTP_CREATED);

        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Could not create booking.',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
