<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Country;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Countries
 *
 * APIs for countries
 */
class CountryController extends Controller
{
    /**
     * Get all countries
     *
     * Call to return all countries
     *
     * @response 200 {"success": true, "message": "Retrieved all countries."}
     */
    public function all(Request $request)
    {
        $countries = Country::all();

        return response()->json([
            'success' => true,
            'message' => 'Retrieved all countries.',
            'countries' => $countries->map(fn($country) => $country->only('id', 'name', 'code', 'dial_prefix')),
        ], Response::HTTP_OK);
    }

    /**
     * Get country by Id
     *
     * Returns a country by Id
     *
     * @urlParam countryId int required The Id of the country. Example: 1
     * @response 200 {"success": true, "message": "Retrieved Country."}
     * @response 404 {"success": false, "message": "Country not found."}
     */
    public function getById(Request $request, int $countryId)
    {

        $country = Country::find($countryId);

        if (!$country) {
            return response()->json([
                'success' => false,
                'message' => 'Country not found.',
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'success' => true,
            'message' => 'Retrieved Country.',
            'country' => $country->only(['id', 'name', 'code', 'dial_prefix'])
        ], Response::HTTP_OK);
    }
}
