<?php

namespace App\Http\Controllers\API;

use App\Enums\OrganizationAccessEnum;
use App\Helpers\OrganizationHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateOrginizationProfileRequest;
use App\Models\Organization;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Organizations
 *
 * APIs for organizations
 */
class OrganizationController extends Controller
{
    /**
     *
     * Get user's organization
     *
     * Retrieve the organization in which the user is registered in
     *
     * @authenticated
     * @header Authorization Bearer <token>
     *
     * @response 200 { "success": true, "message": "Organization retrieved successfully." }
     * @response 500 { "success": false, "message": "Could not retrieve organization data." }
     */
    public function getOrganization(Request $request)
    {
        $user = $request->user();
        if (count($user->organizations) < 1) {
            return response()->json([
                'success' => false,
                'message' => 'Organization not found'
            ], Response::HTTP_NOT_FOUND);
        }
        $organization = $user->organizations->first();

        return response()->json([
            'success' => true,
            'message' => 'Organization retrieved successfully.',
            'organization' => OrganizationHelper::organizationResponse($organization),
            'access_level' => $organization->pivot->access_level
        ], Response::HTTP_OK);
    }

    /**
     * Update Organization profile
     *
     * Update an organizations profile by organization id (only org admin).
     *
     * @authenticated
     * @header Authorization Bearer <token>
     *
     * @urlParam organizationId int The id of the organization. Example: 1
     * @header Authorization Bearer <token>
     * @bodyParam name string required The name of the organization. Example: Συνεταιρισμός Χανίων
     * @bodyParam description string required The description of the organization. Example: The best organization ever
     * @bodyParam email string The email address of the organization. Example: synchaniwn@gmail.com
     * @bodyParam phone string The phone number of the organization. Example: +306977777777
     * @bodyParam country_id int required The country id of the organization. Example: 1
     * @bodyParam region string The region/city of the organization. Example: Αθήνα
     * @bodyParam zip_code string The zip code of the organization. Example: 14145
     * @bodyParam address_road string The address name of the organization. Example: Άργους
     * @bodyParam address_number string The address number of the organization. Example: 139
     * @bodyParam facebook string Facebook account link of the organization. Example: facebook.com/bill_andrik
     * @bodyParam instagram string Instagram account link of the organization. Example: instagram.com/bill_andrik
     * @bodyParam twitter string Twitter account link of the organization. Example: twitter.com/bill_andrik
     * @bodyParam linkedin string LinkedIn account link of the organization. Example: linkedin.com/bill_andrik
     * @bodyParam website string Website link for the organization (if present). Example: www.synchaniwn.gr
     * @bodyParam cover_photo file The cover photo of the organization
     * @response 200 { "success": true, "message": "Organization updated successfully." }
     * @response 403 { "success": false, "message": "You don\'t have permission to update organization." }
     * @response 404 { "success": false, "message": "Organization not found." }
     * @response 500 { "success": false, "message": "Could not update organization profile." }
     */
    public function updateOrganizationProfile(UpdateOrginizationProfileRequest $request, int $organizationId)
    {
        $user = $request->user();
        $organization = Organization::find($organizationId);
        if (!$organization) {
            return response()->json([
                'success' => false,
                'message' => 'Organization not found.'
            ], Response::HTTP_NOT_FOUND);
        }

        // Check if user is admin for this organization
        if (!in_array($user->id, $organization->admins()->pluck('id')->toArray()))
        {
            return response()->json([
                'success' => false,
                'message' => 'You don\'t have permission to update organization.',
            ], Response::HTTP_FORBIDDEN);
        }

        $updateData = $request->validated();
        try {
            DB::beginTransaction();

            $organization->name = $updateData['name'];
            $organization->description = $updateData['description'];
            $organization->email = $updateData['email'];
            $organization->phone = $updateData['phone'];
            $organization->country_id = $updateData['country_id'];
            $organization->region = $updateData['region'];
            $organization->zip_code = $updateData['zip_code'];
            $organization->address_road = $updateData['address_road'];
            $organization->address_number = $updateData['address_number'];
            $facebook = $updateData['facebook'];
            $instagram = $updateData['instagram'];
            $twitter = $updateData['twitter'];
            $linkedin = $updateData['linkedin'];

            $organization->social = json_encode([
                'facebook' => $facebook,
                'twitter' => $twitter,
                'instagram' => $instagram,
                'linkedin' => $linkedin,
            ]);
            $organization->website = $updateData['website'];

            if ($request->cover_photo && $request->cover_photo->isValid()) {
                $fileName = time().'-'.$request->cover_photo->getClientOriginalName();
                if ($organization->cover_photo) {
                    Storage::disk('public')->delete('cover_photos/'.$organization->cover_photo);
                }
                // Store new cover photo
                $request->cover_photo->storePubliclyAs('',$fileName,'cover_photos');
                $organization->cover_photo = $fileName;
            }
            $organization->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Organization updated successfully.',
                'organization' => OrganizationHelper::organizationResponse($organization)
            ], Response::HTTP_OK);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => 'Could not update organization profile.',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }
}
