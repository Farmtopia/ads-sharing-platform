<?php

namespace App\Http\Controllers\API;

use App\Helpers\EquipmentHelper;
use App\Http\Controllers\Controller;
use App\Models\EquipmentCategory;
use App\Models\EquipmentSubcategory;
use App\Models\EquipmentType;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Equipment
 *
 * APIs for equipment
 */
class EquipmentController extends Controller
{
    /**
     * Get equipment types
     *
     * Call to return equipment type options
     *
     * @response 200 { "success": true, "message": "Equipment retrieved successfully." }
     * @response 404 { "success": false, "message": "No equipment types found." }
     */
    public function getTypes(Request $request)
    {
        $types = EquipmentType::all();
        if (count($types) < 1) {
            return response()->json([
                'success' => false,
                'message' => 'No equipment types found.',
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'success' => true,
            'message' => 'Equipment types retrieved successfully.',
            'types' => EquipmentHelper::equipmentOptions($types),
        ]);
    }

    /**
     * Get equipment categories
     *
     * Call to return equipment category options
     *
     * @urlParam typeId int required The type id - required to return Type's children categories. Example: 1
     * @response 200 { "success": true, "message": "Equipment categories retrieved successfully." }
     * @response 404 { "success": false, "message": "No equipment categories found." }
     */
    public function getCategories(Request $request, int $typeId)
    {
        $categories = EquipmentCategory::where('equipment_type_id', $typeId)->get();
        if (count($categories) < 1) {
            return response()->json([
                'success' => false,
                'message' => 'No equipment categories found.',
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'success' => true,
            'message' => 'Equipment categories retrieved successfully.',
            'types' => EquipmentHelper::equipmentOptions($categories),
        ]);
    }

    /**
     * Get equipment sub-categories
     *
     * Call to return equipment sub-category options
     *
     * @urlParam categoryId int required The category id - required to return Category's children sub-categories. Example: 1
     * @response 200 { "success": true, "message": "Equipment subcategories retrieved successfully." }
     * @response 404 { "success": false, "message": "No equipment subcategories found." }
     */
    public function getSubCategories(Request $request, int $typeId)
    {
        $subCategories = EquipmentSubCategory::where('equipment_category_id', $typeId)->get();
        if (count($subCategories) < 1) {
            return response()->json([
                'success' => false,
                'message' => 'No equipment subcategories found.',
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'success' => true,
            'message' => 'Equipment subcategories retrieved successfully.',
            'types' => EquipmentHelper::equipmentOptions($subCategories),
        ]);
    }
}
