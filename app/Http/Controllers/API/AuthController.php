<?php

namespace App\Http\Controllers\API;

use App\Enums\OrganizationAccessEnum;
use App\Enums\UserRolesEnum;
use App\Helpers\AuthHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\InvitationRequest;
use App\Http\Requests\ResetPasswordLinkRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Requests\StoreOrganizationMemberRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UserVerifyEmailRequest;
use App\Mail\NotifyAdminForUserRegistration;
use App\Mail\OrganizationMemberInvitationMail;
use App\Mail\PasswordResetLinkEmail;
use App\Mail\PasswordResetSuccessEmail;
use App\Mail\UserVerificationSuccessEmail;
use App\Models\Invitation;
use App\Models\Organization;
use App\Models\PasswordResetToken;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;


/**
 * @group Auth
 *
 * APIs for Authentication/ Authorization
 */
class AuthController extends Controller
{
    /**
     * Register a User
     *
     * Register a new User as admin of an organization.
     *
     * @bodyParam firstname string required The first name of the user. Example: Bill
     * @bodyParam lastname string required The last name of the user. Example: Andrik
     * @bodyParam email string required The email address of the user. Example: bill_andrik@example.com
     * @bodyParam password string required The password for the user account. Example: pass1234
     * @bodyParam confirm_password string required Password confirmation. Example: pass1234
     * @bodyParam organization_name string required The name of the organization. Example: Super org
     * @bodyParam country_id int required The country id of the user: Example: 1
     * @bodyParam region string required The region/city of the user: Example: Αθήνα
     * @bodyParam zip_code string required The zip code of the user: Example: 14145
     * @bodyParam address_road string required The address name of the user: Example: Άργους
     * @bodyParam address_number string required The address number of the user: Example: 139
     * @bodyParam region string required The region/city of the user: Example: Athens
     * @response 200 {"success": true, "message": "User registered successfully."}
     * @response 422 {"success": false, "message": "Could not register user."}
     * @response 500 {"success": false, "message": "Could not register user."}
     */
    public function register(StoreUserRequest $request)
    {
        $data = $request->validated();

        try {
            DB::beginTransaction();
            $user = AuthHelper::storeUser($data);

            if (!$user) {
                return response()->json([
                    'success' => false,
                    'message' => 'Could not register user.',
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            // Set as normal user as Application Level Role
            $user->assignRole(UserRolesEnum::ADMIN->value);

            // Create organization entry and give the user
            // Admin access as Organization Level Role
            $organization = Organization::create([
                'name' => $data['organization_name'],
                'country_id' => $data['country_id'],
                'region' => $data['region'],
                'zip_code' => $data['zip_code'],
                'address_road' => $data['address_road'],
                'address_number' => $data['address_number'],
            ]);

            $organization->users()->attach($user->id, ['access_level' => OrganizationAccessEnum::ORG_ADMIN->value]);

            // Notify Admin users for a new user registration
            $adminUsers = User::role(UserRolesEnum::ADMIN->value)->get();
            foreach ($adminUsers as $adminUser) {
                // Make this Queueable job
                Mail::to($adminUser)->send(new NotifyAdminForUserRegistration($user));
            }
            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'User registered successfully.',
            ], Response::HTTP_OK);

        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => 'Could not register user.',
                'error' => $e->getMessage(),
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * Login a User
     *
     * @bodyParam email string required Must be a valid email address. Example: bill_andrik@example.com
     * @bodyParam  password string required Example: pass1234
     * @response 400 {"success": false, "message": "Wrong username or password."}
     * @response 200 {"success": true, "message": "User logged in successfully.", "token": "<token>"}
     */
    public function login(LoginRequest $request)
    {
        $data = $request->validated();

        $user = AuthHelper::authenticateUser($data);

        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'Wrong username or password.',
            ], Response::HTTP_BAD_REQUEST);
        }

        return response()->json([
            'success' => true,
            'message' => 'User logged in successfully.',
            'token' => $user->createToken('api-token')->plainTextToken,
        ], Response::HTTP_OK);
    }

    /**
     * Verify User's email
     */
    public function verifyUserEmail(UserVerifyEmailRequest $request)
    {
        $data = $request->validated();

        $user = User::find($data['id']);
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'Invalid verification link.',
            ], Response::HTTP_UNAUTHORIZED);
        }

        if ($user->hasVerifiedEmail())
        {
            return response()->json([
                'success' => false,
                'message' => 'Email already verified.',
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        if ($user && $request->hash === AuthHelper::createUserVerificationHash($user)) {
            $user->markEmailAsVerified();
            Mail::to($user)->send(new UserVerificationSuccessEmail($user));
            return response()->json([
                'success' => true,
                'message' => 'Email verified successfully.',
            ], Response::HTTP_OK);
        }

        return response()->json([
            'success' => false,
            'message' => 'Invalid verification link.',
        ], Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Send Verification email to User
     */
    public function sendVerificationEmail(User $user)
    {
        if (!$user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ], Response::HTTP_NOT_FOUND);
        }

        if ($user->hasVerifiedEmail()) {
            return response()->json([
                'success' => false,
                'message' => 'Email already verified.'
            ], Response::HTTP_OK);
        }

        Mail::to($user)->send(new \App\Mail\UserVerificationEmail($user));

        return response()->json([
            'success' => true,
            'message' => 'Verification email sent'
        ], Response::HTTP_OK);
    }

    /**
     * Request reset password link
     */
    public function sendResetPasswordEmail(ForgotPasswordRequest $request)
    {
        try {
            $data = $request->validated();

            $user = User::where('email', $data['email'])->first();
            if (!$user) {
                return response()->json([
                    'success' => false,
                    'message' => 'Could not send reset password email.',
                ], Response::HTTP_BAD_REQUEST);
            }

            PasswordResetToken::where('email', $user->email)->delete();
            Mail::to($user->email)->send(new PasswordResetLinkEmail($user));

            return response()->json([
                'success' => true,
                'message' => 'Reset password sent successfully.'
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Could not send reset password email.',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Verify password reset link
     */
    public function verifyResetLink(ResetPasswordLinkRequest $request)
    {
        $data = $request->validated();
        try {
            $email = Crypt::decryptString($data['email_encrypted']);

            $user = User::where('email', $email)->first();
            if (!$user) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid reset link.'
                ], Response::HTTP_UNAUTHORIZED);
            }

            $token = PasswordResetToken::where('email', $email)
                ->where('token', $request->token)
                ->where('created_at', '>', Carbon::now()->subMinutes(Config::get('auth.passwords.users.expire')))
                ->first();
            if (!$token || $token->token !== AuthHelper::createUserPasswordResetToken($user)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid reset link.'
                ], Response::HTTP_UNAUTHORIZED);
            }

            return response()->json([
                'success' => true,
                'message' => 'Reset password link verified.',
            ], Response::HTTP_OK);
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Could not verify reset link.',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Reset Password request
     */
    public function resetPassword(ResetPasswordRequest $request)
    {
        $data = $request->validated();

        try {
            $email = Crypt::decryptString($data['email_encrypted']);

            if ($email !== $data['email']) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid reset link.',
                ], Response::HTTP_UNAUTHORIZED);
            }

            $user = User::where('email', $email)->first();
            if (!$user) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid reset link.'
                ], Response::HTTP_UNAUTHORIZED);
            }

            $token = PasswordResetToken::where('email', $email)
                ->where('token', $data['token'])
                ->where('created_at', '>', Carbon::now()->subMinutes(Config::get('auth.passwords.users.expire')))
                ->first();
            if (!$token || $token->token !== AuthHelper::createUserPasswordResetToken($user)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid reset link.'
                ], Response::HTTP_UNAUTHORIZED);
            }

            DB::beginTransaction();
            $user->update([
                'password' => bcrypt($data['password']),
            ]);

            $token->delete();
            $user->tokens()->delete();

            DB::commit();
            Mail::to($user->email)->send(new PasswordResetSuccessEmail($user));


            return response()->json([
                'success' => true,
                'message' => 'Password reset successfully.',
            ], Response::HTTP_OK);
        } catch(\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Could not reset password.',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Invite an Organization member
     *
     * Invite a user to register as organization member by email
     *
     * @authenticated
     * @header Authorization Bearer <token>
     * @bodyParam email string required The email of the member to invite
     */
    public function inviteOrganizationMember(InvitationRequest $request)
    {
        $data = $request->validated();
        try {
            $organization = $request->user()->organization->first();
            if (!$organization) {
                return response()->json([
                    'success' => false,
                    'message' => 'Organization not found.'
                ], Response::HTTP_NOT_FOUND);
            }

            $adminIds = $organization->admins()->pluck('id');
            if (!$adminIds->contains($request->user()->id)) {
                return response()->json([
                    'success' => false,
                    'message' => 'Unauthorized for this action.'
                ], Response::HTTP_UNAUTHORIZED);
            }
            $token = sha1(Str::random(64));
            Invitation::create([
                'organization_id' => $organization->id,
                'user_id' => null,
                'token' => $token,
                'email' => $data['email'],
            ]);

            Mail::to($data['email'])->send(new OrganizationMemberInvitationMail($data['email'], $token));

            return response()->json([
                'success' => true,
                'message' => 'Invitation sent.',
            ], Response::HTTP_OK);
        } catch(\Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Could not send invitation email.',
                'error' => $e->getMessage(), // Include the actual error message
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    // TODO: create a link verification call

    /**
     * Register Organization Member
     *
     * Call to register an organization member by personal link (token)
     *
     * @header Accept application/json
     * @header Content-type application/json
     * @urlParam token string required The token created from the invitation sent to user. Example: Ff3ere34r3fdwf45
     * @bodyParam firstname string required The first name of the user. Example: Bill
     * @bodyParam lastname string required The last name of the user. Example: Andrik
     * @bodyParam email string required The email address of the user. Example: bill_andrik2@example.com
     * @bodyParam password string required The password for the user account. Example: pass1234
     * @bodyParam confirm_password string required Password confirmation. Example: pass1234
     * @bodyParam organization_id integer required The Id of the organization. Example: 1
     * @bodyParam country_id int required The country id of the user: Example: 1
     * @bodyParam region string required The region/city of the user: Example: Αθήνα`
     * @bodyParam zip_code string required The zip code of the user: Example: 14145
     * @bodyParam address_road string required The address name of the user: Example: Άργους
     * @bodyParam address_number string required The address number of the user: Example: 139
     * @bodyParam region string required The region/city of the user: Example: Athens
     * @response 200 {"success": true, "message": "User registered successfully."}
     * @response 400 {"success": false, "message": "User already exists."}
     * @response 401 {"success": false, "message": "Invalid registration link."}
     * @response 404 {"success": false, "message": "Organization not found."}
     * @response 422 {"success": false, "message": "Could not register user."}
     * @response 500 {"success": false, "message": "Could not register user."}
     */
    public function registerOrganizationMember(StoreOrganizationMemberRequest $request, string $token)
    {
        $data = $request->validated();
        try {
            DB::beginTransaction();
            $invitation = Invitation::where('email', $data['email'])->first();
            if (!$invitation || $invitation->token !== $token) {
                return response()->json([
                    'success' => false,
                    'message' => 'Invalid registration link.',
                ], Response::HTTP_UNAUTHORIZED);
            }

            if ($invitation->user_id !== null) {
                return response()->json([
                    'success' => false,
                    'message' => 'User already exists.',
                ], Response::HTTP_BAD_REQUEST);
            }

            $user = AuthHelper::storeUser($data);

            if (!$user) {
                return response()->json([
                    'success' => false,
                    'message' => 'Could not register user.',
                ], Response::HTTP_UNPROCESSABLE_ENTITY);
            }

            // Set as normal user as Application Level Role
            $user->assignRole(UserRolesEnum::USER->value);

            $organization = Organization::find($invitation->organization_id);

            if (!$organization) {
                return response()->json([
                    'success' => false,
                    'message' => 'Organization not found.'
                ], Response::HTTP_NOT_FOUND);
            }

            $organization->users()->attach($user->id, [
                'access_level' => OrganizationAccessEnum::ORG_MEMBER->value,
                'created_at' => now(),
                'updated_at' => now(),
            ]);

            $invitation->user_id = $user->id;
            $invitation->save();
            DB::commit();

            // TODO: Maybe send an Email

            return response()->json([
                'success' => true,
                'message' => 'User registered successfully.',
            ], Response::HTTP_OK);

        } catch (Exception $e) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => 'Could not invite.',
                'error' => $e->getMessage()
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }

    /**
     * Post user logout
     *
     * Logs out a user.
     *
     * @authenticated
     * @header Authorization Bearer <token>
     * @response 200 { "success": true, "message": "Logged out successfully." }
     * @response 500 { "success": false, "message": "Could not logout." }
     */
    public function logout(Request $request)
    {
        try {
            $request->user()->tokens()->delete();
            return response()->json([
                'success' => true,
                'message' => 'Logged out successfully'
            ], Response::HTTP_OK);
        } catch (Exception $e) {
            return response()->json([
                'success' => false,
                'message' => 'Could not logout.'
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

}
