<?php

namespace App\Http\Controllers\API;

use App\Helpers\UserHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserProfileRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group Users
 *
 * APIs for users
 */
class UserController extends Controller
{
    /**
     * Get Logged in User's data
     *
     * Retrieve data for the authenticated user
     *
     * @authenticated
     * @header Authorization Bearer <token>
     */
    public function getAuthenticatedUser(Request $request)
    {
        return UserHelper::userResponse($request->user());
    }

    /**
     * Update User's profile data
     *
     * Update the profile data of the authenticated user
     *
     * @authenticated
     * @header Authorization Bearer <token>
     * @bodyParam firstname string required The first name of the user. Example: Bill
     * @bodyParam lastname string required The last name of the user. Example: Andrik
     * @bodyParam phone string The phone number of the user. Example: +306977777777
     * @bodyParam profession string The profession of the user. Example: Software Developer
     * @bodyParam country_id int required The country id of the user. Example: 1
     * @bodyParam region string required The region/city of the user. Example: Αθήνα
     * @bodyParam zip_code string required The zip code of the user. Example: 14145
     * @bodyParam address_road string required The address name of the user. Example: Άργους
     * @bodyParam address_number string required The address number of the user. Example: 139
     * @bodyParam facebook string Facebook account link of the user. Example: facebook.com/bill_andrik
     * @bodyParam instagram string Instagram account link of the user. Example: instagram.com/bill_andrik
     * @bodyParam twitter string Twitter account link of the user. Example: twitter.com/bill_andrik
     * @bodyParam linkedin string LinkedIn account link of the user. Example: linkedin.com/bill_andrik
     * @bodyParam profile_photo file The profile image file
     * @response 200 {"success": true, "message": "Profile updated successfully."}
     * @response 500 {"success": false, "message": "Could not update user\'s profile."}
     */
    public function updateUserProfile(UpdateUserProfileRequest $request)
    {
        $user = $request->user();
        $updateData = $request->validated();

        try {
            DB::beginTransaction();

            $user->firstname = $updateData['firstname'];
            $user->lastname = $updateData['lastname'];
            $user->phone = $updateData['phone'] ?? null;
            $user->profession = $updateData['profession'] ?? null;
            $user->country_id = $updateData['country_id'];
            $user->region = $updateData['region'] ?? null;
            $user->zip_code = $updateData['zip_code'] ?? null;
            $user->address_road = $updateData['address_road'] ?? null;
            $user->address_number = $updateData['address_number'] ?? null;
            $facebook = $updateData['facebook'] ?? null;
            $twitter = $updateData['twitter'] ?? null;
            $instagram = $updateData['instagram'] ?? null;
            $linkedin = $updateData['linkedin'] ?? null;
            $user->social = json_encode([
                'facebook' => $facebook,
                'twitter' => $twitter,
                'instagram' => $instagram,
                'linkedin' => $linkedin,
            ]);

            if ($request->profile_photo && $request->profile_photo->isValid()) {
                $fileName = time() . '_' . $request->profile_photo->getClientOriginalName();
                if ($user->profile_photo) {
                    Storage::disk('public')->delete('profile_photos/'.$user->profile_photo);
                }

                // Store new cover photo
                $request->profile_photo->storePubliclyAs('', $fileName, 'profile_photos');
                $user->profile_photo = $fileName;
            }
            $user->save();

            DB::commit();

            return response()->json([
                'success' => true,
                'message' => 'Profile updated successfully.',
                'user' => UserHelper::userResponse($user)
            ], Response::HTTP_OK);

        } catch (\Exception $exception) {
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => 'Could not update user\'s profile.',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }

    }
}
