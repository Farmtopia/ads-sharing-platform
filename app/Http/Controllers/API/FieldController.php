<?php

namespace App\Http\Controllers\API;

use App\Helpers\FieldHelper;
use App\Http\Controllers\Controller;
use App\Models\EquipmentSubcategory;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @group FormFields
 *
 * APIs for form fields
 */
class FieldController extends Controller
{
    /**
     * Get form fields by subCategoryId
     *
     * Call to return all form fields for specific equipment sub-category
     *
     * @urlParam subCategoryId int required The subcatergory id - required for getting the fields needed. Example: 1
     * @response 200 { "success": true, "message": "Retrieved form fields." }
     * @response 404 { "success": false, "message": "Subcategory not found." }
     */
    public function getFormFields(Request $request, int $subCategoryId)
    {
        $subCategory = EquipmentSubcategory::find($subCategoryId);
        if (!$subCategory) {
            return response()->json([
                'success' => false,
                'message' => 'Subcategory not found.'
            ], Response::HTTP_NOT_FOUND);
        }

        return response()->json([
            'success' => true,
            'message' => 'Retrieved fields successfully.',
            'fields' => $subCategory->fields->map(function ($field) {
                return [
                    'id' => $field->id,
                    'name' => $field->name,
                    'label' => $field->label,
                    'type' => $field->type,
                    'options' => $field->options,
                    'unit' => $field->unit
                ];
            }),
            'collections' => [
                'number_of_cylinders' => FieldHelper::number_of_cylinders(),
                'fuel_types' => FieldHelper::fuel_types(),
                'power_supply' => FieldHelper::power_supply(),
                'attachment_types' => FieldHelper::attachment_types(),
                'measurement_parameters' => FieldHelper::measurement_parameters(),
                'correction_services' => FieldHelper::correction_services(),
                'licenses' => FieldHelper::licenses(),
            ]
        ], Response::HTTP_OK);
    }
}
