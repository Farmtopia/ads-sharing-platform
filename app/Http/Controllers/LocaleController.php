<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocaleController extends Controller
{
    public function setLocale(Request $request)
    {
        $locale = $request->input('locale');

        if (in_array($locale, ['en'])) { // Add more languages here
            session(['locale' => $locale]);
            app()->setLocale($locale);
        }

        return response()->json(['success' => true, 'locale' => app()->getLocale()]);
    }
}
