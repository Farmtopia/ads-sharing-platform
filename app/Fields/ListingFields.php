<?php

namespace App\Fields;

use App\Interfaces\BackpackFieldsInterface;
use App\Traits\BackpackFieldsTrait;
use Illuminate\Support\Collection;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanel as CRUD;

class ListingFields implements BackpackFieldsInterface
{
    use BackpackFieldsTrait;

    /**
     * Validation rules for this model
     * @var array
     */
    public array $VALIDATION_RULES = [];

    public function filters(CRUD $crud):void
    {
        //you should code your filters here
    }

    public function fields(): Collection
    {
        return  collect([

        ]);
    }
}
