<?php

namespace App\Fields;

use App\Enums\UserRolesEnum;
use App\Interfaces\BackpackFieldsInterface;
use App\Traits\BackpackFieldsTrait;
use Illuminate\Support\Collection;

class UserFields implements BackpackFieldsInterface
{
    use BackpackFieldsTrait;

    /**
     * Validation rules for this model
     * @var array
     */
    public array $VALIDATION_RULES = [
        'name'=>'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'nullable|min:6|confirmed',
    ];

    public function fields(): Collection
    {
        return  collect([
            [
                'name'  => 'user_info_heading',
                'type'  => 'custom_html',
                'value' => '<h2>User information</h2><hr/>',
                'create'=>true,
            ], [
                'name'=>'firstname',
                'type'=>'text',
                'label'=>'First name',
                'create'=>true,
                'list'=>true,
            ], [
                'name'=>'lastname',
                'type'=>'text',
                'label'=>'Last name',
                'create'=>true,
                'list'=>true,
            ],[
                'name'  => 'email',
                'label' => 'Email Address',
                'type'  => 'email',
                'list'=>true,
                'create'=>true,
            ],[
                'name'  => 'separator',
                'type'  => 'custom_html',
                'value' => '<h2>Password setup</h2><hr/>',
                'create'=>true,
            ],[
                'name'  => 'password',
                'label' => 'Password',
                'type'  => 'password',
                'attributes'=>[
                    'placeholder'=>'New password',
                    'autocomplete'=>'new-password',
                ],
                'create'=>true,
            ],[
                'name'  => 'password_confirmation',
                'label' => 'Repeat password',
                'type'  => 'password',
                'attributes'=>[
                    'placeholder'=>'New password',
                    'autocomplete'=>'new-password',
                ],
                'create'=>true,
            ],
        ]);
    }
}
