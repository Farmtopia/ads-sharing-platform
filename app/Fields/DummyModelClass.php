<?php

namespace App\Fields;

use App\Interfaces\BackpackFieldsInterface;
use App\Traits\BackpackFieldsTrait;
use Illuminate\Support\Collection;

class DummyModelClass implements BackpackFieldsInterface
{
    use BackpackFieldsTrait;

    /**
     * Validation rules for this model
     * @var array
     */
    public array $VALIDATION_RULES = [];

    public function fields(): Collection
    {
        return  collect([

        ]);
    }
}
