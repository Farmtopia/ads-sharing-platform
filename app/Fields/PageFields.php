<?php

namespace App\Fields;

use App\Enums\PageStatusEnum;
use App\Interfaces\BackpackFieldsInterface;
use App\Traits\BackpackFieldsTrait;
use Illuminate\Support\Collection;

class PageFields implements BackpackFieldsInterface
{
    use BackpackFieldsTrait;

    /**
     * Validation rules for this model
     * @var array
     */
    public array $VALIDATION_RULES = [
        'title' => 'required|string|max:255',
    ];

    public function fields(): Collection
    {
        return  collect([
            [
                'name' => 'title',
                'label' => 'Title',
                'type' => 'text',
                'create'=>true,
                'list'=>true,
            ],[
                'name' => 'slug',
                'label' => 'Slug',
                'type' => 'slug',
                'target'=>'title',
                'create'=>true,
                'list'=>true,
            ],[
                'name' => 'body',
                'label' => 'Body',
                'type' => 'ckeditor',
                'create'=>true,
                'list'=>false,
            ],[
                'name' => 'status',
                'label' => 'Status',
                'type' => 'select_from_array',
                'options' => PageStatusEnum::getLabels(),
                'create'=>true,
                'list'=>true,
                ]
        ]);
    }
}
