<?php

namespace App\Fields;

use App\Interfaces\BackpackFieldsInterface;
use App\Traits\BackpackFieldsTrait;
use Illuminate\Support\Collection;

class SettingFields implements BackpackFieldsInterface
{
    use BackpackFieldsTrait;

    /**
     * Validation rules for this model
     * @var array
     */
    public array $VALIDATION_RULES = [
        'key'=>'required|string|max:255',
        'label'=>'required|string|max:255',
    ];


    public function fields(): Collection
    {
        return collect([
            [
                'type'=>'text',
                'name'=>'label',
                'label'=>'Label',
                'create'=>true,
                'list'=>true,
            ],[
                'type'=>'slug',
                'name'=>'key',
                'target'=>'label',
                'label'=>'Key',
                'create'=>true,
                'list'=>true,
            ],[
                'type'=>'textarea',
                'name'=>'value',
                'label'=>'Value',
                'create'=>true,
            ]
        ]);
    }
}
