<?php

namespace App\Interfaces;
use Illuminate\Support\Collection;

interface BackpackFieldsInterface
{
    public function fields(): Collection;
    public function list_fields():array;
    public function create_fields():array;
}
