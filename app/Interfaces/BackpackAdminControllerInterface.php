<?php

namespace App\Interfaces;

interface BackpackAdminControllerInterface
{
    const FIELDS_CLASS=null;
    public function __construct();
    function setupListOperation() : void;
    function setupCreateOperation() : void;
    function setupUpdateOperation() : void;
}
