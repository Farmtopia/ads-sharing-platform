<?php

namespace App\Enums;

enum SettingTypesEnum: string
{
    const HEAD_SCRIPTS = 'head-scripts';
    const FOOTER_SCRIPTS = 'footer-scripts';
    const PRIVACY_POLICY = 'privacy-policy';
    const TERMS_AND_CONDITIONS = 'terms-and-conditions';

    public static function labels():array
    {
        return [
            self::HEAD_SCRIPTS=>__('Head Scripts'),
            self::FOOTER_SCRIPTS=>__('Footer Scripts'),
            self::PRIVACY_POLICY=>__('Privacy Policy'),
            self::TERMS_AND_CONDITIONS=>__('Terms and Conditions'),
        ];
    }
}
