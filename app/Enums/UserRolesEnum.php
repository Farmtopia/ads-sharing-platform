<?php

namespace App\Enums;

enum UserRolesEnum: string
{
    case ADMIN = 'admin';
    case USER = 'user';

    public static function selectOptionsArray():array
    {
        return [
            self::ADMIN->value => 'Admin',
            self::USER->value => 'User',
        ];
    }

}
