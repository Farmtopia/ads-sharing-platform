<?php

namespace App\Enums;

enum FieldTypeEnum: string
{
    case SELECT = 'select';
    case MULTISELECT = 'multiselect';
    case TEXT = 'text';
    case NUMBER = 'number';
    case DATE = 'date';
}
