<?php

namespace App\Enums;

enum UserRegistrationStatus: string
{
    case ACCEPTED = 'accepted';
    case REJECTED = 'rejected';
    case PENDING = 'pending';

    public static function selectOptionsArray(): array
    {
        return [
            self::ACCEPTED->value => 'Accepted',
            self::REJECTED->value => 'Rejected',
            self::PENDING->value => 'Pending',
        ];
    }
}
