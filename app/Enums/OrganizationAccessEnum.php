<?php


namespace App\Enums;

enum OrganizationAccessEnum: string
{
    case ORG_ADMIN = 'org_admin';
    case ORG_MEMBER = 'org_member';

    public static function selectOptionsArray(): array
    {
        return [
            self::ORG_ADMIN->value => 'Admin',
            self::ORG_MEMBER->value => 'Member',
        ];
    }

}
