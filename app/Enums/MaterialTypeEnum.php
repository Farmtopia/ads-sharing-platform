<?php

namespace App\Enums;

enum MaterialTypeEnum: string
{
    case IMAGE = 'image';
    case VIDEO = 'video';
    case DOCUMENT = 'document';
}
