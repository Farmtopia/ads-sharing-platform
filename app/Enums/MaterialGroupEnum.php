<?php

namespace App\Enums;

enum MaterialGroupEnum : string
{
    case MEDIA = 'media';
    case SUPPORT_MATERIAL = 'support_material';

}
