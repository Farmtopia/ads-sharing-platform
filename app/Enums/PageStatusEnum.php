<?php

namespace App\Enums;

enum PageStatusEnum: int
{
    case DRAFT = 0;
    case PUBLISHED = 1;
    case ARCHIVED = 2;

    public static function getLabels(): array
    {
        return [
            self::DRAFT->value => __('Draft'),
            self::PUBLISHED->value => __('Published'),
            self::ARCHIVED->value => __('Archived')
        ];
    }
}
