<?php

namespace App\Providers;

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\ServiceProvider;

class ScribeServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        //
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        // Run the scribe:generate command whenever the app boots
        if (app()->environment('local', 'development')) {
            Artisan::call('scribe:generate');
        }
    }
}
