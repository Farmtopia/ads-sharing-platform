<?php

namespace App\Config;

class AppConfig
{
    public static function frontURL():string
    {
        return config('app.frontend.url');
    }
}
