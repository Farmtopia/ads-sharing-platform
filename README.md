# Farmtopia ADS Sharing Platform

## Project Overview

The **Farmtopia ADS Sharing Platform** is a digital matchmaking solution designed to enhance collaboration among farmers by facilitating the sharing of agricultural equipment and digital solutions. Developed under **Task 1.3** of the **Farmtopia project**, the platform enables farmers to **offer and request equipment and software tools**, promoting a cost-effective and cooperative approach to digital agriculture.

### Background and Inspiration

The platform draws inspiration from the **CUMA collaborative model** and the **mycumalink platform**, adapting these concepts to foster a **sharing economy** for agricultural digital solutions (ADS) and equipment. The **ADS Sharing Platform** is being implemented in **two EU countries (excluding France)**, with potential for further **expansion**.

### Key Components

- **Marketplace**  
A **Software-as-a-Service (SaaS)** solution allowing farmers to **list and share equipment** within their farmer associations or regional communities. Listings can be filtered and searched, helping users find the equipment they need.

- **Knowledge Base (Planned Release - Month 36)**  
A repository of **software modules, decision support systems (DSS)**, and other **digital agricultural tools**. This feature will support **knowledge sharing** and foster **innovation** within the farming community.

---

## Platform Functionality

The ADS Sharing Platform offers the following functionality:

- **Organisational Accounts**:  
  Administrators can create organisational accounts and **invite members** to join their organisation.
- **User Profiles**:  
  Each user has a **profile** where they can manage their personal and contact information.
- **Listings Management**:  
  Users can **create, edit, and manage listings** for equipment and software tools available for sharing.
- **Marketplace Access**:  
  Members of the same organisation can **browse, filter, and search listings** within their community.
- **Equipment Requests**:  
  Users can **send requests** to rent or borrow equipment directly through the platform.

---

## Technology Stack

The platform is developed using:

- **Laravel** (Frontend and Backend Framework)
- **React** (Frontend)
- **MySQL/MariaDB** (Database)

---

## Deployment Instructions

To deploy the platform, follow these steps:

1. Clone the repository from GitLab:
    ```bash
    git clone git@gitlab.com:Farmtopia/ads-sharing-platform.git
    ```
2. Navigate to the project directory:
    ```bash
    cd ads-sharing-platform
    ```
3. Install PHP dependencies:
    ```bash
    composer install
    ```
4. Install JavaScript dependencies:
    ```bash
    npm install
    ```
5. Build the frontend assets:
    ```bash
    npm run build
    ```
6. Create a new database for the platform.

7. Copy the environment configuration file and update it:
    ```bash
    cp .env.example .env
    ```
   Edit `.env` to update **database connection details**, including:
   - DB_HOST
   - DB_PORT
   - DB_DATABASE
   - DB_USERNAME
   - DB_PASSWORD

8. Generate the application key:
    ```bash
    php artisan key:generate
    ```
9. Run the database migrations and seed initial data:
    ```bash
    php artisan migrate:fresh --seed
    ```

---

---

## License

Licensed under the **European Union Public Licence v. 1.2 (EUPL)**  
EUPL © the European Union 2007, 2016

This European Union Public Licence (the "EUPL") applies to this Work. Any use of the Work, other than as authorised under this Licence is prohibited (to the extent such use is covered by a right of the copyright holder of the Work).

The Work is provided under the terms of this Licence when the Licensor has placed the following notice immediately following the copyright notice for the Work:

    Licensed under the EUPL

---

## Contact

For more information, please contact the Farmtopia development team.

---

## Future Plans

- **Knowledge Base Integration** (Planned for Month 36)