<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('firstname');
            $table->string('lastname');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->string('profession')->nullable();
            $table->string('zip_code');
            $table->string('address_road');
            $table->string('address_number');
            $table->string('region')->nullable();
            $table->foreignIdFor(\App\Models\Country::class, 'country_id');
            $table->string('phone')->nullable();
            $table->json('social')->nullable();
            $table->string('profile_photo')->nullable();
            $table->string('status')->default(\App\Enums\UserRegistrationStatus::PENDING->value);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
