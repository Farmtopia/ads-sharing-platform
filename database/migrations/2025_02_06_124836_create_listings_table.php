<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('listings', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\EquipmentSubcategory::class, 'equipment_subcategory_id');
            $table->foreignIdFor(\App\Models\Manufacturer::class , 'manufacturer_id')->nullable();
            $table->foreignIdFor(\App\Models\EquipmentModel::class , 'equipment_model_id')->nullable();
            $table->string('equipment_model_name');
            $table->string('listing_description')->nullable();
            $table->unsignedDecimal('min_price');
            $table->unsignedDecimal('max_price');
            $table->boolean('has_labour')->default(false);
            $table->unsignedDecimal('labour_min_price')->nullable();
            $table->unsignedDecimal('labour_max_price')->nullable();
            $table->longText('labour_scope')->nullable(); // discuss
            $table->longText('labour_experience')->nullable(); // discuss
            $table->longText('labour_special_requirements')->nullable(); // discuss
            $table->boolean('has_delivery_service')->default(false);
            $table->unsignedDecimal('max_delivery_distance')->nullable();
            $table->string('max_delivery_distance_unit')->nullable();
            $table->unsignedDecimal('delivery_fee')->nullable();
            $table->string('delivery_fee_unit')->nullable();
            $table->longText('delivery_timeframe_description')->nullable();
            $table->boolean('include_setup')->default(false);
            $table->boolean('has_pick_up_point')->default(false);
            $table->foreignIdFor(\App\Models\Country::class, 'pick_up_country_id')->nullable();
            $table->string('pick_up_region')->nullable();
            $table->string('pick_up_address_road')->nullable();
            $table->string('pick_up_address_number')->nullable();
            $table->string('pick_up_zip_code')->nullable();
            $table->longText('pickup_timeframe_description')->nullable();
            $table->string('location')->nullable();
            $table->unsignedDecimal('max_distance_booking')->nullable();
            $table->string('max_distance_booking_unit')->nullable();
            $table->boolean('is_license_required')->default(false);
            $table->text('license_type')->nullable();
            $table->longText('renter_responsibilities')->nullable();
            $table->longText('special_requirements')->nullable();
            $table->foreignIdFor(\App\Models\User::class , 'user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('listings');
    }
};
