<?php

use App\Models\EquipmentCategory;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('equipment_subcategory_fields', function (Blueprint $table) {
            $table->foreignIdFor(\App\Models\EquipmentSubcategory::class,'equipment_subcategory_id');
            $table->foreignIdFor(\App\Models\Field::class,'field_id');
            $table->boolean('required')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('equipment_subcategory_fields');
    }
};
