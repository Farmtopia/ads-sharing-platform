<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('technical_characteristics', function (Blueprint $table) {
            $table->id();
            $table->unsignedDecimal('weight')->nullable();
            $table->unsignedDecimal('power')->nullable();
            $table->unsignedDecimal('working_width')->nullable();
            $table->unsignedInteger('number_of_cylinders')->nullable();
            $table->unsignedDecimal('displacement')->nullable();
            $table->unsignedInteger('fuel_type')->nullable();
            $table->unsignedInteger('power_supply')->nullable();
            $table->unsignedInteger('number_of_furrows')->nullable();
            $table->unsignedInteger('is_fixed_or_mounted')->nullable();
            $table->string('measurement_parameters')->nullable();
            $table->unsignedInteger('flight_time')->nullable();
            $table->unsignedInteger('max_range')->nullable();
            $table->unsignedInteger('max_altitude')->nullable();
            $table->longText('sensors_accompanied')->nullable();
            $table->unsignedInteger('accuracy')->nullable();
            $table->string('correction_services')->nullable();
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->string('author')->nullable();
            $table->unsignedInteger('license')->nullable();
            $table->string('publication_date')->nullable();
            $table->string('external_data_source')->nullable();
            $table->foreignIdFor(\App\Models\Listing::class, 'listing_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('technical_characteristics');
    }
};
