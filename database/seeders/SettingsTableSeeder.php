<?php

namespace Database\Seeders;

use App\Enums\SettingTypesEnum;
use App\Models\Setting;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        foreach(SettingTypesEnum::labels() as $key => $label) {
            Setting::create([
                'key' => $key,
                'label' => $label,
                'value' => ''
            ]);
        }
    }
}
