<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Enums\UserRegistrationStatus;
use App\Enums\UserRolesEnum;
use App\Models\Country;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // Create Admin User


         $this->call(SettingsTableSeeder::class);
         $this->call(RoleTableSeeder::class);
         $this->call(CountryTableSeeder::class);

        $user = \App\Models\User::factory()->create([
            'firstname' => 'Super',
            'lastname' => 'Admin',
            'email' => 'vasileios.andrik@gmail.com',
            'password'=>'top_secret!',
            'zip_code' => '10441',
            'address_road' => 'Argous',
            'address_number' => '139',
            'phone' => '+306977777789',
            'country_id' => Country::first()->id,
            'region' => 'Athens',
            'status' => UserRegistrationStatus::ACCEPTED->value
        ]);

         // Assign role to Admin
         $user->assignRole(UserRolesEnum::ADMIN->value);

         $this->call(EquipmentTypeSeeder::class);
         $this->call(EquipmentCategorySeeder::class);
         $this->call(EquipmentSubcategorySeeder::class);
         $this->call(FieldSeeder::class);
         $this->call(EquipmentSubcategoryFieldsSeeder::class);
         $this->call(ManufacturerSeeder::class);
//          $this->call(TestSeeder::class);
    }
}
