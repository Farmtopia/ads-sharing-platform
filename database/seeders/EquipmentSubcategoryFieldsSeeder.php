<?php

namespace Database\Seeders;

use App\Models\EquipmentSubcategory;
use App\Models\Field;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EquipmentSubcategoryFieldsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // 2WD Tractor
        $twd_tractor_Id = EquipmentSubcategory::where('name', '2WD Tractor')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $twd_tractor_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $twd_tractor_Id,
            'field_id' => Field::where('name', 'power')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $twd_tractor_Id,
            'field_id' => Field::where('name', 'displacement')->first()->id,
            'required' => false
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $twd_tractor_Id,
            'field_id' => Field::where('name', 'fuel_type')->first()->id,
            'required' => false
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $twd_tractor_Id,
            'field_id' => Field::where('name', 'number_of_cylinders')->first()->id,
            'required' => false
        ]);

        // 4WD tractors
        $fwd_tractor_Id = EquipmentSubcategory::where('name', '4WD Tractor')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $fwd_tractor_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $fwd_tractor_Id,
            'field_id' => Field::where('name', 'power')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $fwd_tractor_Id,
            'field_id' => Field::where('name', 'displacement')->first()->id,
            'required' => false
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $fwd_tractor_Id,
            'field_id' => Field::where('name', 'fuel_type')->first()->id,
            'required' => false
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $fwd_tractor_Id,
            'field_id' => Field::where('name', 'number_of_cylinders')->first()->id,
            'required' => false
        ]);

        // Combine Harvester
        $combine_harvester_Id = EquipmentSubcategory::where('name', 'Combine harvester')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $combine_harvester_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $combine_harvester_Id,
            'field_id' => Field::where('name', 'power')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $combine_harvester_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $combine_harvester_Id,
            'field_id' => Field::where('name', 'displacement')->first()->id,
            'required' => false
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $combine_harvester_Id,
            'field_id' => Field::where('name', 'fuel_type')->first()->id,
            'required' => false
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $combine_harvester_Id,
            'field_id' => Field::where('name', 'number_of_cylinders')->first()->id,
            'required' => false
        ]);

        // Track Tractor
        $track_tractor_Id = EquipmentSubcategory::where('name', 'Track tractor')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $track_tractor_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $track_tractor_Id,
            'field_id' => Field::where('name', 'power')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $track_tractor_Id,
            'field_id' => Field::where('name', 'displacement')->first()->id,
            'required' => false
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $track_tractor_Id,
            'field_id' => Field::where('name', 'fuel_type')->first()->id,
            'required' => false
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $track_tractor_Id,
            'field_id' => Field::where('name', 'number_of_cylinders')->first()->id,
            'required' => false
        ]);

        // Self-Propelled field sprayer
        $sp_sprayer_Id = EquipmentSubcategory::where('name', 'Self-Propelled field sprayer')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sp_sprayer_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sp_sprayer_Id,
            'field_id' => Field::where('name', 'power')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sp_sprayer_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sp_sprayer_Id,
            'field_id' => Field::where('name', 'displacement')->first()->id,
            'required' => false
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sp_sprayer_Id,
            'field_id' => Field::where('name', 'fuel_type')->first()->id,
            'required' => false
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sp_sprayer_Id,
            'field_id' => Field::where('name', 'number_of_cylinders')->first()->id,
            'required' => false
        ]);

        // IMPLEMENTS

        // Cultivator
        $cultivator_Id = EquipmentSubcategory::where('name', 'Cultivator')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $cultivator_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $cultivator_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $cultivator_Id,
            'field_id' => Field::where('name', 'power_supply')->first()->id,
            'required' => true
        ]);

        // Disc-zing Harrow
        $disc_zing_harrow_Id = EquipmentSubcategory::where('name', 'Disc-zinc harrow')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $disc_zing_harrow_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $disc_zing_harrow_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $disc_zing_harrow_Id,
            'field_id' => Field::where('name', 'power_supply')->first()->id,
            'required' => true
        ]);

        // Mounted field sprayer
        $mounted_field_sprayer_Id = EquipmentSubcategory::where('name', 'Mounted field sprayer')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $mounted_field_sprayer_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $mounted_field_sprayer_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $mounted_field_sprayer_Id,
            'field_id' => Field::where('name', 'power_supply')->first()->id,
            'required' => true
        ]);

        // Plough
        $plough_Id = EquipmentSubcategory::where('name', 'Plough')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $plough_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $plough_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $plough_Id,
            'field_id' => Field::where('name', 'power_supply')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $plough_Id,
            'field_id' => Field::where('name', 'number_of_furrows')->first()->id,
            'required' => true
        ]);

        // Power harrow
        $power_harrow_Id = EquipmentSubcategory::where('name', 'Power harrow')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $power_harrow_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $power_harrow_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $power_harrow_Id,
            'field_id' => Field::where('name', 'power_supply')->first()->id,
            'required' => true
        ]);

        // Power harrow
        $rotary_rake_Id = EquipmentSubcategory::where('name', 'Rotary rake')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $rotary_rake_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $rotary_rake_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $rotary_rake_Id,
            'field_id' => Field::where('name', 'power_supply')->first()->id,
            'required' => true
        ]);

        // Round baler
        $round_baler_Id = EquipmentSubcategory::where('name', 'Round baler')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $round_baler_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $round_baler_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $round_baler_Id,
            'field_id' => Field::where('name', 'power_supply')->first()->id,
            'required' => true
        ]);

        // Sowing machine
        $sowing_machine_Id = EquipmentSubcategory::where('name', 'Sowing machine')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sowing_machine_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sowing_machine_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sowing_machine_Id,
            'field_id' => Field::where('name', 'power_supply')->first()->id,
            'required' => true
        ]);

        // Square baler
        $square_baler_Id = EquipmentSubcategory::where('name', 'Square baler')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $square_baler_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $square_baler_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $square_baler_Id,
            'field_id' => Field::where('name', 'power_supply')->first()->id,
            'required' => true
        ]);

        // Trailed field sprayer
        $trailer_field_sprayer_Id = EquipmentSubcategory::where('name', 'Trailed field sprayer')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $trailer_field_sprayer_Id,
            'field_id' => Field::where('name', 'weight')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $trailer_field_sprayer_Id,
            'field_id' => Field::where('name', 'working_width')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $trailer_field_sprayer_Id,
            'field_id' => Field::where('name', 'power_supply')->first()->id,
            'required' => true
        ]);

        // SENSORS

        // Weather Station
        $weather_station_Id = EquipmentSubcategory::where('name', 'Weather Station')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $weather_station_Id,
            'field_id' => Field::where('name', 'is_fixed_or_mounted')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $weather_station_Id,
            'field_id' => Field::where('name', 'measurement_parameters')->first()->id,
            'required' => true
        ]);

        // Mechanical sensor
        $mech_station_Id = EquipmentSubcategory::where('name', 'Mechanical sensor')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $mech_station_Id,
            'field_id' => Field::where('name', 'is_fixed_or_mounted')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $mech_station_Id,
            'field_id' => Field::where('name', 'measurement_parameters')->first()->id,
            'required' => true
        ]);

        // Optical Sensor
        $opt_sensor_Id = EquipmentSubcategory::where('name', 'Optical Sensor')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $opt_sensor_Id,
            'field_id' => Field::where('name', 'is_fixed_or_mounted')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $opt_sensor_Id,
            'field_id' => Field::where('name', 'measurement_parameters')->first()->id,
            'required' => true
        ]);

        // Airflow Sensor
        $air_sensor_Id = EquipmentSubcategory::where('name', 'Airflow Sensor')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $air_sensor_Id,
            'field_id' => Field::where('name', 'is_fixed_or_mounted')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $air_sensor_Id,
            'field_id' => Field::where('name', 'measurement_parameters')->first()->id,
            'required' => true
        ]);

        // Location sensor
        $loc_sensor_Id = EquipmentSubcategory::where('name', 'Location sensor')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $loc_sensor_Id,
            'field_id' => Field::where('name', 'is_fixed_or_mounted')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $loc_sensor_Id,
            'field_id' => Field::where('name', 'measurement_parameters')->first()->id,
            'required' => true
        ]);

        // Electro-chemical sensor
        $elec_sensor_Id = EquipmentSubcategory::where('name', 'Electro-chemical sensor')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $elec_sensor_Id,
            'field_id' => Field::where('name', 'is_fixed_or_mounted')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $elec_sensor_Id,
            'field_id' => Field::where('name', 'measurement_parameters')->first()->id,
            'required' => true
        ]);

        // Dielectric Soil moisture sensor
        $soil_sensor_Id = EquipmentSubcategory::where('name', 'Dielectric Soil moisture sensor')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $soil_sensor_Id,
            'field_id' => Field::where('name', 'is_fixed_or_mounted')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $soil_sensor_Id,
            'field_id' => Field::where('name', 'measurement_parameters')->first()->id,
            'required' => true
        ]);

        // Yield monitor
        $yield_sensor_Id = EquipmentSubcategory::where('name', 'Yield monitor')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $yield_sensor_Id,
            'field_id' => Field::where('name', 'is_fixed_or_mounted')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $yield_sensor_Id,
            'field_id' => Field::where('name', 'measurement_parameters')->first()->id,
            'required' => true
        ]);


        // DRONES

        // Crop monitoring
        $crop_monitoring_Id = EquipmentSubcategory::where('name', 'Crop monitoring')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $crop_monitoring_Id,
            'field_id' => Field::where('name', 'flight_time')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $crop_monitoring_Id,
            'field_id' => Field::where('name', 'max_range')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $crop_monitoring_Id,
            'field_id' => Field::where('name', 'max_altitude')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $crop_monitoring_Id,
            'field_id' => Field::where('name', 'sensors_accompanied')->first()->id,
            'required' => true
        ]);

        // Livestock monitoring
        $ls_monitoring_Id = EquipmentSubcategory::where('name', 'Livestock monitoring')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $ls_monitoring_Id,
            'field_id' => Field::where('name', 'flight_time')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $ls_monitoring_Id,
            'field_id' => Field::where('name', 'max_range')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $ls_monitoring_Id,
            'field_id' => Field::where('name', 'max_altitude')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $ls_monitoring_Id,
            'field_id' => Field::where('name', 'sensors_accompanied')->first()->id,
            'required' => true
        ]);

        // Drone spraying
        $drone_spraying_Id = EquipmentSubcategory::where('name', 'Drone spraying')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $drone_spraying_Id,
            'field_id' => Field::where('name', 'flight_time')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $drone_spraying_Id,
            'field_id' => Field::where('name', 'max_range')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $drone_spraying_Id,
            'field_id' => Field::where('name', 'max_altitude')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $drone_spraying_Id,
            'field_id' => Field::where('name', 'sensors_accompanied')->first()->id,
            'required' => true
        ]);

        // GNSS

        // Handheld GNSS
        $hand_gnss_Id = EquipmentSubcategory::where('name', 'Handheld GNSS')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $hand_gnss_Id,
            'field_id' => Field::where('name', 'accuracy')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $hand_gnss_Id,
            'field_id' => Field::where('name', 'correction_services')->first()->id,
            'required' => true
        ]);

        // Machine mounted GNSS
        $mach_mounted_Id = EquipmentSubcategory::where('name', 'Machine mounted GNSS')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $mach_mounted_Id,
            'field_id' => Field::where('name', 'accuracy')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $mach_mounted_Id,
            'field_id' => Field::where('name', 'correction_services')->first()->id,
            'required' => true
        ]);

        // Base station
        $base_station_Id = EquipmentSubcategory::where('name', 'Base station')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $base_station_Id,
            'field_id' => Field::where('name', 'accuracy')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $base_station_Id,
            'field_id' => Field::where('name', 'correction_services')->first()->id,
            'required' => true
        ]);

        // DIGITAL ASSETS

        // DSS
        $dss_Id = EquipmentSubcategory::where('name', 'DSS')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dss_Id,
            'field_id' => Field::where('name', 'title')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dss_Id,
            'field_id' => Field::where('name', 'description')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dss_Id,
            'field_id' => Field::where('name', 'author')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dss_Id,
            'field_id' => Field::where('name', 'license')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dss_Id,
            'field_id' => Field::where('name', 'publication_date')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dss_Id,
            'field_id' => Field::where('name', 'external_data_source')->first()->id,
            'required' => true
        ]);

        // API
        $api_Id = EquipmentSubcategory::where('name', 'API')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $api_Id,
            'field_id' => Field::where('name', 'title')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $api_Id,
            'field_id' => Field::where('name', 'description')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $api_Id,
            'field_id' => Field::where('name', 'author')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $api_Id,
            'field_id' => Field::where('name', 'license')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $api_Id,
            'field_id' => Field::where('name', 'publication_date')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $api_Id,
            'field_id' => Field::where('name', 'external_data_source')->first()->id,
            'required' => true
        ]);

        // Dataset
        $dataset_Id = EquipmentSubcategory::where('name', 'Dataset')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dataset_Id,
            'field_id' => Field::where('name', 'title')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dataset_Id,
            'field_id' => Field::where('name', 'description')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dataset_Id,
            'field_id' => Field::where('name', 'author')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dataset_Id,
            'field_id' => Field::where('name', 'license')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dataset_Id,
            'field_id' => Field::where('name', 'publication_date')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $dataset_Id,
            'field_id' => Field::where('name', 'external_data_source')->first()->id,
            'required' => true
        ]);

        // SW module
        $sw_Id = EquipmentSubcategory::where('name', 'SW module')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sw_Id,
            'field_id' => Field::where('name', 'title')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sw_Id,
            'field_id' => Field::where('name', 'description')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sw_Id,
            'field_id' => Field::where('name', 'author')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sw_Id,
            'field_id' => Field::where('name', 'license')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sw_Id,
            'field_id' => Field::where('name', 'publication_date')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $sw_Id,
            'field_id' => Field::where('name', 'external_data_source')->first()->id,
            'required' => true
        ]);

        // Documentation
        $doc_Id = EquipmentSubcategory::where('name', 'Documentation')->first()->id;
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $doc_Id,
            'field_id' => Field::where('name', 'title')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $doc_Id,
            'field_id' => Field::where('name', 'description')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $doc_Id,
            'field_id' => Field::where('name', 'author')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $doc_Id,
            'field_id' => Field::where('name', 'license')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $doc_Id,
            'field_id' => Field::where('name', 'publication_date')->first()->id,
            'required' => true
        ]);
        DB::table('equipment_subcategory_fields')->insert([
            'equipment_subcategory_id' => $doc_Id,
            'field_id' => Field::where('name', 'external_data_source')->first()->id,
            'required' => true
        ]);
    }
}
