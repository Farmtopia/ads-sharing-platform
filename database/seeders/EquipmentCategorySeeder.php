<?php

namespace Database\Seeders;

use App\Models\EquipmentCategory;
use App\Models\EquipmentType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EquipmentCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        EquipmentCategory::create([
            'name' => 'Tractor',
            'equipment_type_id' => EquipmentType::where('name', 'Traditional Equipment')->first()->id,
        ]);
        EquipmentCategory::create([
            'name' => 'Implement',
            'equipment_type_id' => EquipmentType::where('name', 'Traditional Equipment')->first()->id,
        ]);
        EquipmentCategory::create([
            'name' => 'Drone',
            'equipment_type_id' => EquipmentType::where('name', 'Digital Equipment')->first()->id,
        ]);
        EquipmentCategory::create([
            'name' => 'Sensor',
            'equipment_type_id' => EquipmentType::where('name', 'Digital Equipment')->first()->id,
        ]);
        EquipmentCategory::create([
            'name' => 'GNSS',
            'equipment_type_id' => EquipmentType::where('name', 'Digital Equipment')->first()->id,
        ]);
        EquipmentCategory::create([
            'name' => 'Digital Asset',
            'equipment_type_id' => EquipmentType::where('name', 'Digital Equipment')->first()->id,
        ]);
    }
}
