<?php

namespace Database\Seeders;

use App\Models\EquipmentCategory;
use App\Models\EquipmentSubcategory;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EquipmentSubcategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        EquipmentSubcategory::create([
            'name' => '2WD Tractor',
            'equipment_category_id' => EquipmentCategory::where('name', 'Tractor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => '4WD Tractor',
            'equipment_category_id' => EquipmentCategory::where('name', 'Tractor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Combine harvester',
            'equipment_category_id' => EquipmentCategory::where('name', 'Tractor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Track tractor',
            'equipment_category_id' => EquipmentCategory::where('name', 'Tractor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Self-Propelled field sprayer',
            'equipment_category_id' => EquipmentCategory::where('name', 'Tractor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Cultivator',
            'equipment_category_id' => EquipmentCategory::where('name', 'Implement')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Disc-zinc harrow',
            'equipment_category_id' => EquipmentCategory::where('name', 'Implement')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Mounted field sprayer',
            'equipment_category_id' => EquipmentCategory::where('name', 'Implement')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Plough',
            'equipment_category_id' => EquipmentCategory::where('name', 'Implement')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Power harrow',
            'equipment_category_id' => EquipmentCategory::where('name', 'Implement')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Rotary rake',
            'equipment_category_id' => EquipmentCategory::where('name', 'Implement')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Round baler',
            'equipment_category_id' => EquipmentCategory::where('name', 'Implement')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Sowing machine',
            'equipment_category_id' => EquipmentCategory::where('name', 'Implement')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Square baler',
            'equipment_category_id' => EquipmentCategory::where('name', 'Implement')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Trailed field sprayer',
            'equipment_category_id' => EquipmentCategory::where('name', 'Implement')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Weather Station',
            'equipment_category_id' => EquipmentCategory::where('name', 'Sensor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Mechanical sensor',
            'equipment_category_id' => EquipmentCategory::where('name', 'Sensor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Optical Sensor',
            'equipment_category_id' => EquipmentCategory::where('name', 'Sensor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Airflow Sensor',
            'equipment_category_id' => EquipmentCategory::where('name', 'Sensor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Location sensor',
            'equipment_category_id' => EquipmentCategory::where('name', 'Sensor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Electro-chemical sensor',
            'equipment_category_id' => EquipmentCategory::where('name', 'Sensor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Dielectric Soil moisture sensor',
            'equipment_category_id' => EquipmentCategory::where('name', 'Sensor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Yield monitor',
            'equipment_category_id' => EquipmentCategory::where('name', 'Sensor')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Crop monitoring',
            'equipment_category_id' => EquipmentCategory::where('name', 'Drone')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Livestock monitoring',
            'equipment_category_id' => EquipmentCategory::where('name', 'Drone')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Drone spraying',
            'equipment_category_id' => EquipmentCategory::where('name', 'Drone')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Handheld GNSS',
            'equipment_category_id' => EquipmentCategory::where('name', 'GNSS')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Machine mounted GNSS',
            'equipment_category_id' => EquipmentCategory::where('name', 'GNSS')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Base station',
            'equipment_category_id' => EquipmentCategory::where('name', 'GNSS')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'DSS',
            'equipment_category_id' => EquipmentCategory::where('name', 'Digital Asset')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'API',
            'equipment_category_id' => EquipmentCategory::where('name', 'Digital Asset')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Dataset',
            'equipment_category_id' => EquipmentCategory::where('name', 'Digital Asset')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'SW module',
            'equipment_category_id' => EquipmentCategory::where('name', 'Digital Asset')->first()->id,
        ]);

        EquipmentSubcategory::create([
            'name' => 'Documentation',
            'equipment_category_id' => EquipmentCategory::where('name', 'Digital Asset')->first()->id,
        ]);
    }
}
