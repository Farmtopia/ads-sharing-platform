<?php

namespace Database\Seeders;

use App\Models\Organization;
use App\Models\User;
use App\Models\Listing;
use App\Models\TechnicalCharacteristics;
use App\Models\ListingBooking;
use App\Models\EquipmentModel;
use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    public function run(): void
    {
        $organization = Organization::create([
            'name' => 'Test Organization',
            'description' => 'A test organization',
            'email' => 'org@test.com',
            'phone' => '+1234567890',
            'country_id' => 1,
            'region' => 'Attica',
            'address_road' => 'Argous',
            'address_number' => '139',
            'zip_code' => '10441'
        ]);

        // First user - Owner of listings
        $user = User::create([
            'firstname' => 'John',
            'lastname' => 'Doe',
            'email' => 'john@test.com',
            'password' => 'secret',
            'country_id' => 1,
            'region' => 'Attica',
            'address_road' => 'Achilleos',
            'address_number' => '115',
            'zip_code' => '10441',
            'status' => 'accepted',
            'profile_photo' => null
        ]);

        // Second user - Will be the booker
        $booker = User::create([
            'firstname' => 'Maria',
            'lastname' => 'Papadopoulou',
            'email' => 'maria@test.com',
            'password' => 'secret',
            'country_id' => 1,
            'region' => 'Attica',
            'address_road' => 'Argous',
            'address_number' => '141',
            'zip_code' => '10441',
            'status' => 'accepted',
            'profile_photo' => null
        ]);

        // Attach both users to organization
        $user->organizations()->attach($organization->id, ['access_level' => 'org_admin']);
        $booker->organizations()->attach($organization->id, ['access_level' => 'org_member']);

        // Create equipment models first
        $equipmentModel1 = EquipmentModel::create([
            'name' => '6115M',
            'manufacturer_id' => 1,
            'equipment_subcategory_id' => 1,
            'technical_characteristics_id' => 1 // Will update after creation
        ]);

        $equipmentModel2 = EquipmentModel::create([
            'name' => 'Axion 850',
            'manufacturer_id' => 2,
            'equipment_subcategory_id' => 2,
            'technical_characteristics_id' => 2 // Will update after creation
        ]);

        $equipmentModel3 = EquipmentModel::create([
            'name' => '7R 350',
            'manufacturer_id' => 1,
            'equipment_subcategory_id' => 3,
            'technical_characteristics_id' => 3 // Will update after creation
        ]);

        // First listing
        $listing1 = Listing::create([
            'user_id' => $user->id,
            'equipment_subcategory_id' => 1,
            'manufacturer_id' => 1,
            'equipment_model_id' => $equipmentModel1->id,
            'equipment_model_name' => '6115M',
            'listing_description' => 'Perfect tractor for medium-sized farms. Well maintained with all service records available. Includes GPS guidance system and climate controlled cabin.',
            'min_price' => 100,
            'max_price' => 200,
            'has_labour' => true,
            'labour_min_price' => 50,
            'labour_max_price' => 100,
            'labour_scope' => 'Full operation of the tractor including plowing, seeding, and harvesting.',
            'labour_experience' => '10+ years of experience with similar equipment. Certified operator.',
            'labour_special_requirements' => 'Operation available only during daylight hours. 24-hour notice required.',
            'has_delivery_service' => true,
            'max_delivery_distance' => 50,
            'max_delivery_distance_unit' => 'km',
            'delivery_fee' => 25,
            'delivery_fee_unit' => 'EUR',
            'delivery_timeframe_description' => 'Delivery available Monday to Friday, 8 AM to 6 PM. Same day delivery for orders before noon.',
            'include_setup' => true,
            'has_pick_up_point' => true,
            'pick_up_country_id' => 1,
            'pick_up_region' => 'Attica',
            'pick_up_address_road' => 'Argous',
            'pick_up_address_number' => '139',
            'pick_up_zip_code' => '10441',
            'pickup_timeframe_description' => 'Monday to Friday: 9 AM - 5 PM, Saturday: 10 AM - 2 PM',
            'location' => json_encode([
                'lat' => 37.9858,
                'lon' => 23.7174
            ]),
            'is_license_required' => true,
            'license_type' => 'Agricultural Equipment Operation License',
            'renter_responsibilities' => 'Daily maintenance checks, fuel refill, cleaning after use',
            'special_requirements' => 'Proof of insurance required. Training session mandatory before first use.'
        ]);

        // Create technical characteristics for first listing
        $techCharacteristics1 = TechnicalCharacteristics::create([
            'weight' => 850.5,
            'power' => 120.0,
            'working_width' => 2.5,
            'number_of_cylinders' => 6,
            'displacement' => 4500,
            'fuel_type' => 1, // Assuming 1 = Diesel
            'power_supply' => 1, // Assuming 1 = Engine
            'number_of_furrows' => 3,
            'is_fixed_or_mounted' => 1, // Assuming 1 = Mounted
            'listing_id' => $listing1->id
        ]);

        // Update equipment model with correct technical characteristics ID
        $equipmentModel1->technical_characteristics_id = $techCharacteristics1->id;
        $equipmentModel1->save();

        // Second listing
        $listing2 = Listing::create([
            'user_id' => $user->id,
            'equipment_subcategory_id' => 2,
            'manufacturer_id' => 2,
            'equipment_model_id' => $equipmentModel2->id,
            'equipment_model_name' => 'Axion 850',
            'listing_description' => 'High-performance tractor ideal for large agricultural operations. Features advanced GPS system.',
            'min_price' => 150,
            'max_price' => 300,
            'has_pick_up_point' => true,
            'pick_up_country_id' => 1,
            'pick_up_region' => 'Attica',
            'pick_up_address_road' => 'Keramikou',
            'pick_up_address_number' => '53',
            'pick_up_zip_code' => '10436',
            'location' => json_encode([
                'lat' => 37.9847,
                'lon' => 23.7219
            ])
        ]);

        // Create technical characteristics for second listing
        $techCharacteristics2 = TechnicalCharacteristics::create([
            'weight' => 950.0,
            'power' => 180.0,
            'working_width' => 3.0,
            'number_of_cylinders' => 8,
            'displacement' => 6500,
            'fuel_type' => 1,
            'power_supply' => 1,
            'listing_id' => $listing2->id
        ]);

        // Update equipment model
        $equipmentModel2->technical_characteristics_id = $techCharacteristics2->id;
        $equipmentModel2->save();

        // Third listing
        $listing3 = Listing::create([
            'user_id' => $user->id,
            'equipment_subcategory_id' => 3,
            'manufacturer_id' => 1,
            'equipment_model_id' => $equipmentModel3->id,
            'equipment_model_name' => '7R 350',
            'listing_description' => 'Latest model with eco-friendly features. Perfect for contractors and large farms.',
            'min_price' => 200,
            'max_price' => 400,
            'has_pick_up_point' => true,
            'pick_up_country_id' => 1,
            'pick_up_region' => 'Attica',
            'pick_up_address_road' => 'Pireos',
            'pick_up_address_number' => '76',
            'pick_up_zip_code' => '10435',
            'location' => json_encode([
                'lat' => 37.9778,
                'lon' => 23.7168
            ])
        ]);

        // Create technical characteristics for third listing
        $techCharacteristics3 = TechnicalCharacteristics::create([
            'weight' => 1050.0,
            'power' => 350.0,
            'working_width' => 3.5,
            'number_of_cylinders' => 6,
            'displacement' => 8500,
            'fuel_type' => 1,
            'power_supply' => 1,
            'listing_id' => $listing3->id
        ]);

        // Update equipment model
        $equipmentModel3->technical_characteristics_id = $techCharacteristics3->id;
        $equipmentModel3->save();

        // Add bookings for first listing
        ListingBooking::create([
            'listing_id' => $listing1->id,
            'booker_id' => $booker->id,
            'start_date' => '2025-03-01',
            'end_date' => '2025-03-05'
        ]);

        ListingBooking::create([
            'listing_id' => $listing1->id,
            'booker_id' => $booker->id,
            'start_date' => '2025-03-15',
            'end_date' => '2025-03-20'
        ]);
    }
}
