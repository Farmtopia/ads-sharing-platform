<?php

namespace Database\Seeders;

use App\Enums\FieldTypeEnum;
use App\Models\Field;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Field::create([
            'name' => 'weight',
            'label' => 'Weight',
            'type' => FieldTypeEnum::NUMBER->value,
            'options' => null,
            'unit' => 'kg',
        ]);

        Field::create([
            'name' => 'power',
            'label' => 'Engine Power',
            'type' => FieldTypeEnum::NUMBER->value,
            'options' => null,
            'unit' => 'kW'
        ]);

        Field::create([
            'name' => 'working_width',
            'label' => 'Working Width',
            'type' => FieldTypeEnum::NUMBER->value,
            'options' => null,
            'unit' => 'm'
        ]);

        Field::create([
            'name' => 'number_of_cylinders',
            'label' => 'Number of Cylinders',
            'type' => FieldTypeEnum::SELECT->value,
            'options' => 'number_of_cylinders',
            'unit' => null
        ]);

        Field::create([
            'name' => 'displacement',
            'label' => 'Displacement',
            'type' => FieldTypeEnum::NUMBER->value,
            'options' => null,
            'unit' => 'cc'
        ]);

        Field::create([
            'name' => 'fuel_type',
            'label' => 'Fuel Type',
            'type' => FieldTypeEnum::SELECT->value,
            'options' => "fuel_types",
            'unit' => null
        ]);

        // implements
        Field::create([
            'name' => 'power_supply',
            'label' => 'Power Supply',
            'type' => FieldTypeEnum::SELECT->value,
            'options' => 'power_supply',
            'unit' => null
        ]);

        Field::create([
            'name' => 'number_of_furrows',
            'label' => 'No. of Furrows',
            'type' => FieldTypeEnum::NUMBER->value,
            'options' => null,
            'unit' => null
        ]);

        Field::create([
            'name' => 'is_fixed_or_mounted',
            'label' => 'Fixed or machine-mounted',
            'type' => FieldTypeEnum::SELECT->value,
            'options' => 'attachment_types',
            'unit' => null
        ]);

        Field::create([
            'name' => 'measurement_parameters',
            'label' => 'Measurement Parameters',
            'type' => FieldTypeEnum::MULTISELECT->value,
            'options' => 'measurement_parameters',
            'unit' => null
        ]);

        Field::create([
            'name' => 'flight_time',
            'label' => 'Flight Time',
            'type' => FieldTypeEnum::NUMBER->value,
            'options' => null,
            'unit' => 'min'
        ]);

        Field::create([
            'name' => 'max_range',
            'label' => 'Maximum Range',
            'type' => FieldTypeEnum::NUMBER->value,
            'options' => null,
            'unit' => 'km'
        ]);

        Field::create([
            'name' => 'max_altitude',
            'label' => 'Maximum altitude',
            'type' => FieldTypeEnum::NUMBER->value,
            'options' => null,
            'unit' => 'm'
        ]);

        Field::create([
            'name' => 'sensors_accompanied',
            'label' => 'Sensors Accompanied',
            'type' => FieldTypeEnum::TEXT->value,
            'options' => null,
            'unit' => null
        ]);

        Field::create([
            'name' => 'accuracy',
            'label' => 'Accuracy',
            'type' => FieldTypeEnum::NUMBER->value,
            'options' => null,
            'unit' => 'm'
        ]);

        Field::create([
            'name' => 'correction_services',
            'label' => 'Correction Services',
            'type' => FieldTypeEnum::MULTISELECT->value,
            'options' => 'correction_services',
            'unit' => null
        ]);

         Field::create([
             'name' => 'title',
             'label' => 'Title',
             'type' => FieldTypeEnum::TEXT->value,
             'options' => null,
             'unit' => null
         ]);

         Field::create([
             'name' => 'description',
             'label' => 'Description',
             'type' => FieldTypeEnum::TEXT->value,
             'options' => null,
             'unit' => null
         ]);

         Field::create([
             'name' => 'author',
             'label' => 'Author/Owner',
             'type' => FieldTypeEnum::TEXT->value,
             'options' => null,
             'unit' => null
         ]);

         Field::create([
             'name' => 'license',
             'label' => 'Licensing Schema',
             'type' => FieldTypeEnum::SELECT->value,
             'options' => 'licenses',
             'unit' => null
         ]);

         Field::create([
             'name' => 'publication_date',
             'label' => 'Publication Date',
             'type'=> FieldTypeEnum::DATE->value,
             'options' => null,
             'unit' => null
         ]);

         Field::create([
             'name'=>'external_data_source',
             'label' => 'External Data Source',
             'type'=> FieldTypeEnum::TEXT->value,
             'options' => null,
             'unit' => null
         ]);
    }
}
