<?php

namespace Database\Seeders;

use App\Models\EquipmentType;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class EquipmentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        EquipmentType::create([
            'name' => 'Traditional Equipment'
        ]);

        EquipmentType::create([
            'name' => 'Digital Equipment'
        ]);
    }
}
