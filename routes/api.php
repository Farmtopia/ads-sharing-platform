<?php

use App\Http\Controllers\API\AuthController;
use App\Http\Controllers\API\CountryController;
use App\Http\Controllers\API\EquipmentController;
use App\Http\Controllers\API\ListingBookingController;
use App\Http\Controllers\API\ListingController;
use App\Http\Controllers\API\OrganizationController;
use App\Http\Controllers\API\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->group(function () {
    Route::prefix('users')->group(function () {
        Route::get('authenticated/user', [UserController::class, 'getAuthenticatedUser']);
        Route::put('profile/user/update', [UserController::class, 'updateUserProfile']);
    });

    Route::prefix('organizations')->group(function () {
        Route::get('organizations/organization/by-member', [OrganizationController::class, 'getOrganization']);
        Route::put('organizations/{organizationId}/update', [OrganizationController::class, 'updateOrganizationProfile']);
    });

    Route::prefix('listings')->group(function () {
        Route::get('all', [ListingController::class, 'getOrganizationListings']);
        Route::get('user/all', [ListingController::class, 'getUserListings']);
        Route::get('listing/{listingId}', [ListingController::class, 'getById']);
        Route::post('listing/create', [ListingController::class, 'create']);
        Route::put('listing/{listingId}/update', [ListingController::class, 'update']);
    });

    Route::prefix('listing-bookings')->group(function () {
        Route::post('listing/{listingId}/bookings', [ListingBookingController::class, 'create']);
    });
});

Route::prefix('auth')->group(function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class,'register']);
    Route::get('verify/{user}', [AuthController::class,'sendVerificationEmail']);
    Route::put('verify', [AuthController::class, 'verifyUserEmail']);
    Route::post('forgot-password', [AuthController::class, 'sendResetPasswordEmail']);
    Route::post('reset-password/verify', [AuthController::class, 'verifyResetLink']);
    Route::post('reset-password', [AuthController::class, 'resetPassword']);
    Route::post('invite/user', [AuthController::class, 'inviteOrganizationMember'])->middleware('auth:sanctum');
    Route::post('register/organization/member/{token}', [AuthController::class, 'registerOrganizationMember']);
    Route::post('logout', [AuthController::class, 'logout'])->middleware('auth:sanctum');
});

Route::prefix('countries')->group(function () {
    Route::get('/', [CountryController::class, 'all']);
    Route::get('/{countryId}', [CountryController::class, 'getById']);
});

Route::prefix('equipment')->group(function () {
    Route::get('types', [EquipmentController::class, 'getTypes']);
    Route::get('categories/type/{typeId}', [EquipmentController::class, 'getCategories']);
    Route::get('subcategories/category/{categoryId}', [EquipmentController::class, 'getSubCategories']);
});

Route::prefix('form_fields')->group(function () {
    Route::get('{subCategoryId}', [\App\Http\Controllers\API\FieldController::class, 'getFormFields']);
});

Route::get('/translations/{locale}', function ($locale) {
    $path = resource_path("lang/$locale");

    if (!is_dir($path)) {
        return response()->json(['error' => 'Language not supported.'], 404);
    }

    $translations = [];
    foreach (glob("$path/*.php") as $file) {
        $key = basename($file, '.php');
        $translations[$key] = include $file;
    }

    return response()->json($translations);
});
