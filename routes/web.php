<?php

use App\Http\Controllers\ProfileController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::post('login', [LoginController::class, 'login']);
Route::post('logout', [LogoutController::class, 'logout']);

Route::get('/docs', function () {
    return response()->file(public_path('docs/index.html'));
});

Route::get('/', function () {
    return view('welcome');
})->name('home');

Route::get('/listingview/{listing_id?}', function () {
    return view('listing');
})->where('listing_id', '(.*)')->middleware(['auth', 'verified'])->name('listingview');

Route::get('/signup', function (Illuminate\Http\Request $request) {
    $action = $request->query('action', 'signup'); // Default to 'signup' if no action is provided
    return view('signuplogin', ['action' => $action]);
})->name('signuplogin');

Route::get('/member-signup', function (Illuminate\Http\Request $request) {
    return view('membersignup');
})->name('member-signup');

Route::get('/reset-password', function (Illuminate\Http\Request $request) {
    $token = $request->query('token');
    $email = $request->query('email');
    return view('resetpassword', ['token' => $token, 'email' => $email]);
})->name('reset-password');

Route::get('/marketplace', function (Illuminate\Http\Request $request) {
    return view('marketplace');
})->middleware(['auth', 'verified'])->name('marketplace');

Route::get('/profile', function (Illuminate\Http\Request $request) {
    return view('myprofile');
})->middleware(['auth', 'verified'])->name('profile');

Route::get('/newlisting/{type}', function (Illuminate\Http\Request $request, $type) {
    return view('newlisting', ['type' => $type]);
})->middleware(['auth', 'verified'])->name('newlisting');

Route::get('/editlisting/{listing_id}', function (Illuminate\Http\Request $request, $listing_id) {
    return view('newlisting', ['listing_id' => $listing_id]);
})->middleware(['auth', 'verified'])->name('editlisting');

Route::post('/set-locale', [App\Http\Controllers\LocaleController::class, 'setLocale'])->name('set-locale');

//Route::get('/dashboard', function () {
//    return view('dashboard');
//})->middleware(['auth', 'verified'])->name('dashboard');
//
//Route::middleware('auth')->group(function () {
//    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
//    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
//    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
//});

//require __DIR__.'/auth.php';
