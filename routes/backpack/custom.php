<?php

use Illuminate\Support\Facades\Route;

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace' => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('user', 'UserCrudController');
    Route::crud('page', 'PageCrudController');
    Route::crud('setting', 'SettingCrudController');
    Route::crud('equipment-type', 'EquipmentTypeCrudController');
    Route::crud('equipment-category', 'EquipmentCategoryCrudController');
    Route::crud('equipment-subcategory', 'EquipmentSubcategoryCrudController');
    Route::crud('field', 'FieldCrudController');
    Route::crud('manufacturer', 'ManufacturerCrudController');
    Route::crud('listing', 'ListingCrudController');
    Route::crud('listing-booking', 'ListingBookingCrudController');
    Route::crud('technical-characteristics', 'TechnicalCharacteristicsCrudController');
    Route::crud('material', 'MaterialCrudController');
    Route::crud('invitation', 'InvitationCrudController');
    Route::crud('equipment-model', 'EquipmentModelCrudController');
}); // this should be the absolute last line of this file